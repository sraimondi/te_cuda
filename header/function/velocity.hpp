/*
 * velocity.hpp
 *
 *  Created on: Mar 13, 2015
 *      Author: simon
 */

#ifndef VELOCITY_HPP_
#define VELOCITY_HPP_

#include "types.hpp"

/* Velocity field function */
__host__ __device__ vec2d v_field2D(const vec2d& v, const double t);

__host__ __device__ vec3d v_field3D(const vec3d& v, const double t);

__host__ __device__ vec2d v_rot2D(const vec2d& v, const double t);

__host__ __device__ vec3d v_rot3DXY(const vec3d& v, const double t);

__host__ __device__ vec3d v_rot3DXZ(const vec3d& v, const double t);

__host__ __device__ vec3d v_rot3DYZ(const vec3d& v, const double t);

#endif /* VELOCITY_HPP_ */
