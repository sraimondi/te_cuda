/*
 * boundary.hpp
 *
 *  Created on: Mar 20, 2015
 *      Author: simon
 */

#ifndef BOUNDARY_HPP_
#define BOUNDARY_HPP_

#include "types.hpp"


/* Boundary conditions function */
__host__ __device__ double boundary2D(const vec2d& v, const double t);
__host__ __device__ double boundary3D(const vec3d& v, const double t);

#endif /* BOUNDARY_HPP_ */
