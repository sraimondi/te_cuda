/*
 * types.hpp
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

#include "vector/vector2D.hpp"
#include "vector/vector3D.hpp"

/* Define single precision types */
typedef Vector2D<float> vec2f;
typedef Vector3D<float> vec3f;

/* Define double precision types */
typedef Vector2D<double> vec2d;
typedef Vector3D<double> vec3d;

/* Structure to hold the adapted sizes of a grid */
typedef struct Dim2D {
	unsigned int NX;
	unsigned int NY;

	__host__ __device__ Dim2D(unsigned int nx, unsigned int ny) {
		NX = nx;
		NY = ny;
	}

	__host__ __device__ Dim2D(const Dim2D& d) {
		NX = d.NX;
		NY = d.NY;
	}
} Dim2D;

typedef struct Dim3D {
	unsigned int NX;
	unsigned int NY;
	unsigned int NZ;

	__host__ __device__ Dim3D(unsigned int nx, unsigned int ny, unsigned int nz) {
		NX = nx;
		NY = ny;
		NZ = nz;
	}

	__host__ __device__ Dim3D(const Dim3D& d) {
		NX = d.NX;
		NY = d.NY;
		NZ = d.NZ;
	}
} Dim3D;

#endif /* TYPES_HPP_ */
