/*
 * mpdata2D.hpp
 *
 *  Created on: Apr 6, 2015
 *      Author: simon
 */

#ifndef MPDATA2D_HPP_
#define MPDATA2D_HPP_

#include "solver/solver2D.hpp"
#include "discretization/mesh/discretizedMesh2DCells.hpp"
#include "discretization/velocities/discretizedVelocities2DCells.hpp"

/* CUDA functions */
__global__ void computeMPDATA2D(DiscretizedValue2D* dest, DiscretizedValue2D* src,
								double* x_pa_velocities, double* y_pa_velocities, double* d_vertex_color_buffer,
								double time, double dt,
								Dim2D dim,
								double dx, double dy,
								double field_max, double field_min);
/* Compute MPDATA at a certain position (j,i) */
__host__ __device__ void computeMPDATA2Dat(	DiscretizedValue2D* dest, DiscretizedValue2D* src,
											double* x_pa_velocities, double* y_pa_velocities, double* d_vertex_color_buffer,
											unsigned int i, unsigned int j,
											unsigned int Nx,
											double field_max, double field_min);
/* Compute boundaies */
__host__ __device__ void computeBoundaryMPDATA2D(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
													unsigned int i, unsigned int j,
													double time, double dt,
													Dim2D dim,
													double dx, double dy, double field_max, double field_min);
/* Compute flux at a certain point */
__host__ __device__ double flux(DiscretizedValue2D& left, DiscretizedValue2D& right, double courant_number);

class SolverMPDATA2D: public Solver2D {
private:
	/* Pointer to the 2D cell mesh on host */
	DiscretizedMesh2DCells& mesh;
	/* Pointer to the class that computes the velocities */
	DiscretizedVelocities2DCells mesh_velocities;
	/* Compute delta_t according to the stability of the method */
	virtual void computeDeltaT();
	/* Compute boundary conditions */
	virtual void computeBoundary();
	/* Store two buffers to compute multiple passes */
	DiscretizedValue2D* d_domain_new;
	DiscretizedValue2D* d_domain_intermediate;
	/* Number of passes */
	unsigned int passes;
	/* Velocities do not change over time so we can compute initial only once */
	const bool const_v;

public:
	/* Constructor */
	SolverMPDATA2D(const SolverMPDATA2D&);
	SolverMPDATA2D(DiscretizedMesh2DCells&, const double, const double = 0.01, unsigned int = 1, const bool const_v = false);
	/* Destructor */
	virtual ~SolverMPDATA2D();
	/* Assign operator */
	SolverMPDATA2D& operator=(const SolverMPDATA2D&);
	/* Solve one step */
	virtual void computeStepCPU();
	/* Solve one step on the GPU */
	virtual void computeStepGPU(double* d_vertex_color = NULL);
};



#endif /* MPDATA2D_HPP_ */
