/*
 * mpdata3D.hpp
 *
 *  Created on: Apr 15, 2015
 *      Author: simon
 */

#ifndef MPDATA3D_HPP_
#define MPDATA3D_HPP_

#include "solver/solver3D.hpp"
#include "discretization/mesh/discretizedMesh3DCells.hpp"
#include "discretization/velocities/discretizedVelocities3DCells.hpp"

/* CUDA functions */
__global__ void computeMPDATA3D(DiscretizedValue3D* dest, DiscretizedValue3D* src,
								double* x_pa_velocities, double* y_pa_velocities, double* z_pa_velocities,
								double* d_vertex_color_buffer, double time, double dt,
								Dim3D dim,
								double dx, double dy, double dz, double field_max, double field_min);
/* Compute MPDATA at a certain position (i, j, k) */
__host__ __device__ void computeMPDATA3Dat(DiscretizedValue3D* dest, DiscretizedValue3D* src,
											double* x_pa_velocities, double* y_pa_velocities, double* z_pa_velocities,
											double* d_vertex_color_buffer, unsigned int i, unsigned int j, unsigned int k,
											unsigned int Nx, unsigned int Ny,
											double field_max, double field_min);
/* Compute boundaries */
__host__ __device__ void computeBoundaryMPDATA3D(	DiscretizedValue3D* dest, DiscretizedValue3D* src, double* d_vertex_color_buffer,
													unsigned int i, unsigned int j, unsigned int k,
													double time, double dt,
													Dim3D dim,
													double dx, double dy, double dz, double field_max, double field_min);
/* Compute flux at a certain point */
__host__ __device__ double flux(DiscretizedValue3D& left, DiscretizedValue3D& right, double courant_number);

class SolverMPDATA3D: public Solver3D {
private:
	/* Pointer to the 3D cell mesh on host */
	DiscretizedMesh3DCells& mesh;
	/* Pointer to the class that computes the velocities */
	DiscretizedVelocities3DCells mesh_velocities;
	/* Compute delta_t according to the stability of the method */
	virtual void computeDeltaT();
	/* Compute boundary conditions */
	virtual void computeBoundary();
	/* Store two buffers to compute multiple passes */
	DiscretizedValue3D* d_domain_new;
	DiscretizedValue3D* d_domain_intermediate;
	/* Number of passes */
	unsigned int passes;
	/* Velocities do not change over time so we can compute initial only once */
	const bool const_v;

public:
	/* Constructor */
	SolverMPDATA3D(const SolverMPDATA3D&);
	SolverMPDATA3D(DiscretizedMesh3DCells&, const double, const double = 0.01, unsigned int = 1, const bool = false);
	/* Destructor */
	~SolverMPDATA3D();
	/* Assign operator */
	SolverMPDATA3D& operator=(const SolverMPDATA3D&);
	/* Solve one step */
	virtual void computeStepCPU();
	/* Solve one step on the GPU */
	virtual void computeStepGPU(double* d_vertex_color = NULL);
};




#endif /* MPDATA3D_HPP_ */
