/*
 * solverFOUScheme.hpp
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#ifndef SOLVERFOUSCHEME_HPP_
#define SOLVERFOUSCHEME_HPP_

#include "solver/solver2D.hpp"
#include "discretization/discretizedValue2D.hpp"

/* CUDA functions */
/* __global__ function to call to compute a step on the device */
__global__ void computeStepDeviceFOUSUnsplit(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
												double time, double dt, Dim2D dim, double dx, double dy,
												double field_max, double field_min);

/* Compute new value at a certain position */
__host__ __device__ void computeStepAtPositionFOUSUnsplit(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
															unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dx, double dy,
															double field_max, double field_min);

/* Compute boundary conditions */
__host__ __device__ void computeBoundaryFOUSUsnplit(DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
													unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dx, double dy,
													double field_max, double field_min);

class DiscretizedMesh2D;

/* Solver class declaration */
class SolverFOUSchemeUnsplit : public Solver2D {
private:
	/* Pointer to the 2D field on host */
	DiscretizedMesh2D* mesh;
	/* Compute delta_t according to the stability of the method */
	virtual void computeDeltaT();
	/* Compute boundary conditions */
	virtual void computeBoundary();
	/* Copy of the the buffer to store previous field status to compute new step */
	DiscretizedValue2D* d_domain_input;
public:
	/* Constructor */
	SolverFOUSchemeUnsplit();
	SolverFOUSchemeUnsplit(DiscretizedMesh2D*, const double, const double = 0.01);
	/* Destructor */
	virtual ~SolverFOUSchemeUnsplit();
	/* Solve one step */
	virtual void computeStepCPU();
	/* Solve one step on the GPU */
	virtual void computeStepGPU(double* d_vertex_color = NULL);
};


#endif /* SOLVERFOUSCHEME_HPP_ */
