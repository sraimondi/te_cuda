/*
 * solverFOUSchemeSplit.hpp
 *
 *  Created on: Mar 9, 2015
 *      Author: simon
 */

#ifndef SOLVERFOUSCHEMESPLIT_HPP_
#define SOLVERFOUSCHEMESPLIT_HPP_

#include "solver/solver2D.hpp"
#include "discretization/discretizedValue2D.hpp"

/* CUDA functions */
/* __global__ function to call to compute a step on the device in X direction*/
__global__ void computeStepDeviceFOUSSplitX(DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
											double time, double dt, Dim2D dim, double dx, double dy,
											double field_max, double field_min);

/* __global__ function to call to compute a step on the device in Y direction*/
__global__ void computeStepDeviceFOUSSplitY(DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
											double time, double dt, Dim2D dim, double dx, double dy,
											double field_max, double field_min);

/* Compute new value at a certain position in X direction */
__host__ __device__ void computeStepAtPositionFOUSSplitX(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
															unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dx,
															double field_max, double field_min);

/* Compute new value at a certain position in Y direction */
__host__ __device__ void computeStepAtPositionFOUSSplitY(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
															unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dy,
															double field_max, double field_min);

/* Compute boundary conditions */
__host__ __device__ void computeBoundaryFOUSSplit(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
													unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dx, double dy,
													double field_max, double field_min);

class DiscretizedMesh2D;

class SolverFOUSchemeSplit: public Solver2D {
private:
	/* Pointer to the 2D field on host */
	DiscretizedMesh2D* mesh;
	/* Compute delta_t according to the stability of the method */
	virtual void computeDeltaT();
	/* Compute boundary conditions */
	virtual void computeBoundary();
	/* Copy of the the buffer to store previous field status to compute new step */
	DiscretizedValue2D* d_domain_input;
	/* Intermediate step buffer */
	DiscretizedValue2D* d_domain_intermediate;
public:
	/* Constructor */
	SolverFOUSchemeSplit();
	SolverFOUSchemeSplit(DiscretizedMesh2D*, const double, const double = 0.01);
	/* Destructor */
	virtual ~SolverFOUSchemeSplit();
	/* Solve one step */
	virtual void computeStepCPU();
	/* Solve one step on the GPU */
	virtual void computeStepGPU(double* d_vertex_color = NULL);
};

#endif /* SOLVERFOUSCHEMESPLIT_HPP_ */
