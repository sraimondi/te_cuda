/*
 * solver.hpp
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#ifndef SOLVER_HPP_
#define SOLVER_HPP_

#include "types.hpp"
#include "discretization/discretizedValue2D.hpp"

/* Define for how much time form starting time we have to check for stability */
#define TIME_STABILITY 5
//#define CHECK_STABILITY

/* CUDA functions */
/* Compute color and set it in the device buffer
 * Parameters
 * @point point to update
 * @d_vertex_color_buffer OpenGL buffer to draw the mesh
 * @index index of the point
 * @max initial maximum of the scalar field
 * @min initial minimum of the scalar field */
__host__ __device__ void updateDeviceBufferColor2D(DiscretizedValue2D& point, double* d_vertex_color_buffer, unsigned int index, double max, double min);


class Solver2D {
protected:
	/* Field values adapted */
	Dim2D dim;
	/* Delta t for the solver */
	double dt;
	/* Current time of the simulation */
	double time;
	/* Compute delta_t according to the stability of the method */
	virtual void computeDeltaT() = 0;
	/* Compute boundary conditions */
	// TODO Extend boundary conditions possibilities
	virtual void computeBoundary() = 0;
public:
	/* Constructor */
	Solver2D();
	Solver2D(const Solver2D&);
	Solver2D(const double, const double, const unsigned int, const unsigned int);
	/* Destructor */
	virtual ~Solver2D();
	/* Assign operator */
	Solver2D& operator=(const Solver2D&);
	/* Solve one step on the CPU */
	virtual void computeStepCPU() = 0;
	/* Solve one step on the GPU */
	virtual void computeStepGPU(double* d_vertex_color = NULL) = 0;
};

#endif /* SOLVER_HPP_ */
