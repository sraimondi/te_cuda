/*
 * solver3D.hpp
 *
 *  Created on: Apr 15, 2015
 *      Author: simon
 */

#ifndef SOLVER3D_HPP_
#define SOLVER3D_HPP_

#include "types.hpp"
#include "discretization/discretizedValue3D.hpp"

/* Define for how much time form starting time we have to check for stability */
#define TIME_STABILITY 5
//#define CHECK_STABILITY

/* CUDA functions */
/* Compute color and set it in the device buffer
 * Parameters
 * @point point to update
 * @d_vertex_color_buffer OpenGL buffer to draw the mesh
 * @index index of the point
 * @max initial maximum of the scalar field
 * @min initial minimum of the scalar field */
__host__ __device__ void updateDeviceBufferColor3D(DiscretizedValue3D& point, double* d_vertex_color_buffer, unsigned int index, double max, double min);


class Solver3D {
protected:
	/* Field values adapted */
	Dim3D dim;
	/* Delta t for the solver */
	double dt;
	/* Current time of the simulation */
	double time;
	/* Compute delta_t according to the stability of the method */
	virtual void computeDeltaT() = 0;
	/* Compute boundary conditions */
	// TODO Extend boundary conditions possibilities
	virtual void computeBoundary() = 0;
public:
	/* Constructor */
	Solver3D();
	Solver3D(const Solver3D&);
	Solver3D(const double, const double, const unsigned int, const unsigned int, const unsigned int);
	/* Destructor */
	virtual ~Solver3D();
	/* Assign operator */
	Solver3D& operator=(const Solver3D&);
	/* Solve one step on the CPU */
	virtual void computeStepCPU() = 0;
	/* Solve one step on the GPU */
	virtual void computeStepGPU(double* d_vertex_color = NULL) = 0;
};


#endif /* SOLVER3D_HPP_ */
