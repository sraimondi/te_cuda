/*
 * utils.hpp
 *
 *  Created on: Mar 11, 2015
 *      Author: simon
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include "stdio.h"

// Define this to turn on error checking
#define CUDA_ERROR_CHECK

#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError() __cudaCheckError( __FILE__, __LINE__ )

/* Check error for CUDA function call */
inline void __cudaSafeCall(cudaError err, const char *file, const int line) {
#ifdef CUDA_ERROR_CHECK
	if (cudaSuccess != err) {
		fprintf(stderr, "cudaSafeCall() failed at %s:%i : %s\n", file, line,
				cudaGetErrorString(err));
		exit(-1);
	}
#endif

	return;
}

/* Check error after kernel call */
inline void __cudaCheckError(const char *file, const int line) {
#ifdef CUDA_ERROR_CHECK
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err) {
		fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n", file, line,
				cudaGetErrorString(err));
		exit(-1);
	}

	/* More careful checking. However, this will affect performance. */
	err = cudaDeviceSynchronize();
	if (cudaSuccess != err) {
		fprintf(stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
				file, line, cudaGetErrorString(err));
		exit(-1);
	}
#endif

	return;
}

/* Ask to choose device with a certain compute capability, returns device index */
inline int cudaChooseCompCapability(const int major, const int minor) {\
	cudaDeviceProp prop;
	int dev;
	/* Initialize property structure */
	memset(&prop, 0, sizeof(cudaDeviceProp));
	/* Set compute capability */
	prop.major = major;
	prop.minor = minor;

	CudaSafeCall(cudaChooseDevice(&dev, &prop));

	return dev;
}

#endif /* UTILS_HPP_ */
