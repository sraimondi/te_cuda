/*
 * graphic.hpp
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#ifndef GRAPHIC_HPP_
#define GRAPHIC_HPP_

#define check_gl_error() _check_gl_error(__FILE__,__LINE__)
#define NORMAL_COLOR

#include "types.hpp"
#include "discretization/discretizedValue2D.hpp"
#include "discretization/discretizedValue3D.hpp"

class Mesh2DGraphic;

/* Create lookAt matrix to modify view */
double* lookAt(const vec3d&, const vec3d&, const vec3d&);

/* Check OpenGL error */
void _check_gl_error(const char*, int);

/* CUDA functions */
/* Compute color for a point */
__host__ __device__ void computePointColor(const DiscretizedValue2D&, const double, const double, float*, float*, float*);
__host__ __device__ void computePointColor(const DiscretizedValue3D&, const double, const double, float*, float*, float*, float*);



#endif /* GRAPHIC_HPP_ */
