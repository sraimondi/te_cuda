/*
 * mesh3DCellsGraphic.hpp
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#ifndef MESH3DCELLSGRAPHIC_HPP_
#define MESH3DCELLSGRAPHIC_HPP_

#include <GL/glew.h>
#include "types.hpp"

class DiscretizedMesh3DCells;

class Mesh3DCellsGraphic {
private:
	/* Store reference to field */
	DiscretizedMesh3DCells& mesh;
	/* Field values adapted */
	Dim3D dim;
	/* Id for the buffers */
	GLuint vertex_color_id;
	GLuint index_id;
	/* Device pointer to the buffer of vertex / colors */
	double* d_vertex_color;
	/* Size of accessible buffer */
	size_t d_vertex_color_size;
	/* Generate buffers for the GPU */
	void generateBuffers();
	/* Map resources for CUDA - OpenGL interoperability */
	void initInteroperabilityBuffer();
public:
	/* Constructor */
	Mesh3DCellsGraphic(DiscretizedMesh3DCells&);
	/* Destructor */
	virtual ~Mesh3DCellsGraphic();
	/* Draw field */
	void draw3DField();
	/* Update vertex and color buffer */
	void updateBuffer();
	/* Map resources */
	void mapResourcesCUDA();
	/* Unmap resources */
	void unmapResourcesCUDA();
	/* Get pointer to CUDA vertex / color buffer */
	double*& getDeviceVertexColorBuffer();
};


#endif /* MESH3DCELLSGRAPHIC_HPP_ */
