/*
 * field2DCellsGraphic.hpp
 *
 *  Created on: Apr 2, 2015
 *      Author: simon
 */

#ifndef FIELD2DCELLSGRAPHIC_HPP_
#define FIELD2DCELLSGRAPHIC_HPP_

#include "mesh2DGraphics.hpp"

class DiscretizedMesh2DCells;

class Mesh2DCellsGraphic : public Mesh2DGraphic {
private:
	/* Store reference to field */
	DiscretizedMesh2DCells& mesh;
	/* Generate buffers for the GPU */
	virtual void generateBuffers();
public:
	/* Constructor */
	Mesh2DCellsGraphic(DiscretizedMesh2DCells&);
	/* Destructor */
	virtual ~Mesh2DCellsGraphic();
	/* Update vertex and color buffer */
	virtual void updateBuffer();
};

#endif /* FIELD2DCELLSGRAPHIC_HPP_ */
