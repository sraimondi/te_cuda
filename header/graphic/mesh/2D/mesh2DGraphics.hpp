/*
 * meshGraphics.hpp
 *
 *  Created on: Apr 7, 2015
 *      Author: simon
 */

#ifndef MESHGRAPHICS_HPP_
#define MESHGRAPHICS_HPP_

#include <GL/glew.h>
#include "types.hpp"

class Mesh2DGraphic {
protected:
	/* Field values adapted */
	Dim2D dim;
	/* Id for the buffers */
	GLuint vertex_color_id;
	GLuint index_id;
	/* CUDA resource for the vertex buffer */
	cudaGraphicsResource* vertex_color_resource;
	/* Device pointer to the buffer of vertex / colors */
	double* d_vertex_color;
	/* Size of accessible buffer */
	size_t d_vertex_color_size;
	/* Generate buffers for the GPU */
	virtual void generateBuffers() = 0;
	/* Map resources for CUDA - OpenGL interoperability */
	void initInteroperabilityBuffer();
public:
	/* Constructor */
	Mesh2DGraphic();
	Mesh2DGraphic(unsigned int, unsigned int);
	/* Destructor */
	virtual ~Mesh2DGraphic();
	/* Draw field */
	void draw2DField();
	/* Update vertex and color buffer */
	virtual void updateBuffer() = 0;
	/* Map resources */
	void mapResourcesCUDA();
	/* Unmap resources */
	void unmapResourcesCUDA();
	/* Get pointer to CUDA vertex / color buffer */
	double*& getDeviceVertexColorBuffer();
};


#endif /* MESHGRAPHICS_HPP_ */
