/*
 * Field2DGraphic.hpp
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#ifndef FIELD2DGRAPHIC_HPP_
#define FIELD2DGRAPHIC_HPP_

#include "mesh2DGraphics.hpp"

class DiscretizedMesh2D;

class Mesh2DCrossGraphic : public Mesh2DGraphic {
private:
	/* Store reference to field */
	DiscretizedMesh2D* mesh;
	/* Generate buffers for the GPU */
	virtual void generateBuffers();
public:
	/* Constructor */
	Mesh2DCrossGraphic();
	Mesh2DCrossGraphic(DiscretizedMesh2D*);
	/* Destructor */
	virtual ~Mesh2DCrossGraphic();
	/* Update vertex and color buffer */
	virtual void updateBuffer();
};

#endif /* FIELD2DGRAPHIC_HPP_ */
