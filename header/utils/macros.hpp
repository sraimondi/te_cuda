/*
 * macros.hpp
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#ifndef MACROS_HPP_
#define MACROS_HPP_

/* Macro definition */
#define OFFSET2D(i, j, Nx) ((j) * (Nx) + i)
#define OFFSET3D(i, j, k, Nx, Ny) ((k) * (Nx) * (Ny) + (j) * (Nx) + i)

#endif /* MACROS_HPP_ */
