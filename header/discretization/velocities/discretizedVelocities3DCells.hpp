/*
 * discretizedVelocities3DCells.hpp
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#ifndef DISCRETIZEDVELOCITIES3DCELLS_HPP_
#define DISCRETIZEDVELOCITIES3DCELLS_HPP_

#include "types.hpp"
#include "discretization/discretizedValue3D.hpp"

/* Define small tollerance for derivatives denominator so we don't get nan */
#define PSI 10e-15

class DiscretizedMesh3DCells;

/* CUDA functions */
__global__ void initVelocitiesXDevice(	double* d_x_velocities, double* d_x_pa_velocities,
										Dim3D dim_v_x,
										double xmin, double ymin, double zmin,
										double dx, double dy, double dz, double time, double dt);

__global__ void initVelocitiesYDevice(	double* d_y_velocities, double* d_y_pa_velocities,
										Dim3D dim_v_y,
										double xmin, double ymin, double zmin,
										double dx, double dy, double dz, double time, double dt);

__global__ void initVelocitiesZDevice(	double* d_z_velocities, double* d_z_pa_velocities,
										Dim3D dim_v_z,
										double xmin, double ymin, double zmin,
										double dx, double dy, double dz, double time, double dt);

/* Compute partial velocities on the GPU */
__global__ void paVelocitiesXDevice(	double* pa_dest, double* pa_src,
										double* d_y_pa_velocities, double* d_z_pa_velocities,
										DiscretizedValue3D* u,
										Dim3D dim_v_x, Dim3D dim_v_y, Dim3D dim_v_z,
										double dx, double dy, double dz, double dt);

__global__ void paVelocitiesYDevice(	double* pa_dest, double* pa_src,
										double* d_x_pa_velocities, double* d_z_pa_velocities,
										DiscretizedValue3D* u,
										Dim3D dim_v_y, Dim3D dim_v_x, Dim3D dim_v_z,
										double dx, double dy, double dz, double dt);

__global__ void paVelocitiesZDevice(	double* pa_dest, double* pa_src,
										double* d_x_pa_velocities, double* d_y_pa_velocities,
										DiscretizedValue3D* u,
										Dim3D dim_v_z, Dim3D dim_v_x, Dim3D dim_v_y,
										double dx, double dy, double dz, double dt);

/* Compute partial velocity at a certain position j i */
__host__ __device__ void computePaVelocitiesXAt(	double* pa_dest, double* pa_src,
													double* pa_y, double* pa_z,
													DiscretizedValue3D* u,
													Dim3D dim_v_x, Dim3D dim_v_y, Dim3D dim_v_z,
													double dx, double dy, double dz, double dt,
													unsigned int i, unsigned int j, unsigned int k);

__host__ __device__ void computePaVelocitiesYAt(	double* pa_dest, double* pa_src,
													double* pa_x, double* pa_z,
													DiscretizedValue3D* u,
													Dim3D dim_v_y, Dim3D dim_v_x, Dim3D dim_v_z,
													double dx, double dy, double dz, double dt,
													unsigned int i, unsigned int j, unsigned int k);

__host__ __device__ void computePaVelocitiesZAt(	double* pa_dest, double* pa_src,
													double* pa_x, double* pa_y,
													DiscretizedValue3D* u,
													Dim3D dim_v_z, Dim3D dim_v_x, Dim3D dim_v_y,
													double dx, double dy, double dz, double dt,
													unsigned int i, unsigned int j, unsigned int k);

/* This class initializes the velocities on both X and Y direction */
class DiscretizedVelocities3DCells {
private:
	/* Store pointer to the field for which we want the velocities on the cells edges */
	DiscretizedMesh3DCells& mesh;

	/* Adapted size of the velocities arrays */
	Dim3D dim_v_x;
	Dim3D dim_v_y;
	Dim3D dim_v_z;

	/* Size of the buffers */
	unsigned int size_x;
	unsigned int size_y;
	unsigned int size_z;
	size_t size_x_t;
	size_t size_y_t;
	size_t size_z_t;

	/* Store pointers to the velocities (host and device) that are the initial velocities */
	/* Store the pointers of the three velocities in X, Y and Z for the host */
	double* h_x_velocities;
	double* h_y_velocities;
	double* h_z_velocities;

	/* Store pointers to the three velocities in X, Y and Z for the device */
	double* d_x_velocities;
	double* d_y_velocities;
	double* d_z_velocities;

	/* Store pointers to the velocities (host and device) that are the partial velocities for multiple passes */
	/* Store the pointers of the three velocities in X, Y and Z for the host */
	double* h_x_pa_velocities;
	double* h_y_pa_velocities;
	double* h_z_pa_velocities;

	/* Store pointers to the three velocities in X, Y and Z for the device */
	double* d_x_pa_velocities;
	double* d_y_pa_velocities;
	double* d_z_pa_velocities;

	/* Compute initial velocities on X */
	void initVelocitiesX(const double, const double);
	/* Compute initial velocities on Y */
	void initVelocitiesY(const double, const double);
	/* Compute initial velocities on Z */
	void initVelocitiesZ(const double, const double);

	/* Compute partial velocities on X */
	double* computePaVelocitiesX(const double);
	/* Compute partial velocities on Y */
	double* computePaVelocitiesY(const double);
	/* Compute partial velocities on Z */
	double* computePaVelocitiesZ(const double);

	/* Update single initial velocities from host to device and vice-versa */
	void updateInitVelocitiesXHost();
	void updateInitVelocitiesYHost();
	void updateInitVelocitiesZHost();

	void updateInitVelocitiesXDevice();
	void updateInitVelocitiesYDevice();
	void updateInitVelocitiesZDevice();

	/* Update single partial velocities from host to device and vice-versa */
	void updatePaVelocitiesXHost();
	void updatePaVelocitiesYHost();
	void updatePaVelocitiesZHost();

	void updatePaVelocitiesXDevice();
	void updatePaVelocitiesYDevice();
	void updatePaVelocitiesZDevice();

public:
	/* Constructor */
	DiscretizedVelocities3DCells(DiscretizedMesh3DCells&);
	DiscretizedVelocities3DCells(const DiscretizedVelocities3DCells&);
	/* Destructor */
	~DiscretizedVelocities3DCells();
	/* Assign operator */
	DiscretizedVelocities3DCells& operator=(const DiscretizedVelocities3DCells&);

	/* Compute initial velocities */
	void initVelocitiesHost(const double, const double);

	/* Compute initial velocities on the device */
	void initVelocitiesDevice(const double, const double);

	/* Reset the initial partial velocities */
	void resetPaVelocitiesHost();

	/* Reset the initial partial velocities on the device */
	void resetPaVelocitiesDevice();

	/* Compute one step of partial velocities on the host */
	void computePaVelocitiesHost(const double);

	/* Compute one step of partial velocities on the host */
	void computePaVelocitiesDevice(const double);

	/* Getters for the pointers */
	double*& getHostInitVelocitiesX();
	double*& getHostInitVelocitiesY();
	double*& getHostInitVelocitiesZ();

	double*& getDeviceInitVelocitiesX();
	double*& getDeviceInitVelocitiesY();
	double*& getDeviceInitVelocitiesZ();

	double*& getHostPaVelocitiesX();
	double*& getHostPaVelocitiesY();
	double*& getHostPaVelocitiesZ();

	double*& getDevicePaVelocitiesX();
	double*& getDevicePaVelocitiesY();
	double*& getDevicePaVelocitiesZ();

	/* Update buffers form host to device and vice-versa */
	void updateInitVelocitiesHost();
	void updateInitVelocitiesDevice();
};




#endif /* DISCRETIZEDVELOCITIES3DCELLS_HPP_ */
