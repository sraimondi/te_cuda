/*
 * discretizedValue3D.hpp
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#ifndef DISCRETIZEDVALUE3D_HPP_
#define DISCRETIZEDVALUE3D_HPP_

#include "types.hpp"

class DiscretizedValue3D {
private:
	/* 2D position */
	vec3d p;
	/* Function value */
	double f_value;
public:
	/* Constructor */
		__host__ __device__ DiscretizedValue3D() : p(), f_value(0.0) {};
		__host__ __device__ DiscretizedValue3D(const double x, const double y, const double z) : p(x, y, z), f_value(0.0) {};
		__host__ __device__ DiscretizedValue3D(const vec3d& v, const float f_v) : p(v), f_value(f_v) {};

		__host__ __device__ virtual ~DiscretizedValue3D() {}

		/* Compute the value of the function */
		__host__ __device__ void computeFunctionValue(double(*func)(const vec3d& p)) {
			f_value = func(p);
		}

		/* Position getter*/
		__host__ __device__ const vec3d& getPosition() const {
			return p;
		}

		/* f_value getter */
		__host__ __device__ double getFunctionValue() const {
			return f_value;
		}

		/* Setter for the f_value */
		__host__ __device__ void setFValue(const double f) {
			f_value = f;
		}

		/* Operator overloading */
		/* Assignment operator */
		__host__ __device__ DiscretizedValue3D& operator=(const DiscretizedValue3D& d) {
			if (this != &d) {
				p = vec3d(d.p);
				f_value = d.f_value;
			}
			return *this;
		}

		__host__ __device__ void operator=(const double f) {
			this->f_value = f;
		}

		__host__ __device__ void operator=(const float f) {
			this->f_value = f;
		}

		/* Sum the value of the other point to this. Use the position of this for the new DiscretizedValue2D */
		__host__ __device__ double operator+(const DiscretizedValue3D& d) {
			return f_value + d.f_value;
		}

		/* Same as previous but with a scalar value */
		__host__ __device__ double operator+(const double& d) {
			return f_value + d;
		}

		/* Subtract the value of the other point to this. Use the position of this for the new DiscretizedValue2D */
		__host__ __device__ double operator-(const DiscretizedValue3D& d) {
			return f_value - d.f_value;
		}

		/* Same as previous but with a scalar value */
		__host__ __device__ double operator-(const double& d) {
			return f_value - d;
		}

		/* Multiply the value of the other point to this. Use the position of this for the new DiscretizedValue2D */
		__host__ __device__ double operator*(const DiscretizedValue3D& d) {
			return f_value * d.f_value;
		}

		/* Same as previous but with a scalar value */
		__host__ __device__ double operator*(const double& d) {
			return f_value * d;
		}

		/* Friend operators */
		__host__ __device__ friend double operator-(const double& v, const DiscretizedValue3D& d) {
			return v - d.f_value;
		}

		__host__ __device__ friend double operator+(const double& v, const DiscretizedValue3D& d) {
			return v + d.f_value;
		}

		__host__ __device__ friend double operator*(const double& v, const DiscretizedValue3D& d) {
			return v * d.f_value;
		}
};

#endif /* DISCRETIZEDVALUE3D_HPP_ */
