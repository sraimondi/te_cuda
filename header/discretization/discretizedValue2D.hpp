/*
 * DiscretizedValue2D.hpp
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#ifndef DISCRETIZEDVALUE2D_HPP_
#define DISCRETIZEDVALUE2D_HPP_

#include "types.hpp"

class DiscretizedValue2D {
private:
	/* 2D position */
	vec2d p;
	/* Function value */
	double f_value;
public:
	/* Constructor */
	__host__ __device__ DiscretizedValue2D() : p(), f_value(0.0) {};
	__host__ __device__ DiscretizedValue2D(const double x, const double y) : p(x, y), f_value(0.0) {};
	__host__ __device__ DiscretizedValue2D(const vec2d& v, const float f_v) : p(v), f_value(f_v) {};

	__host__ __device__ virtual ~DiscretizedValue2D() {}

	/* Compute the value of the function */
	__host__ __device__ void computeFunctionValue(double(*func)(const vec2d& p)) {
		f_value = func(p);
	}

	/* Position getter*/
	__host__ __device__ const vec2d& getPosition() const {
		return p;
	}

	/* f_value getter */
	__host__ __device__ double getFunctionValue() const {
		return f_value;
	}

	/* Setter for the f_value */
	__host__ __device__ void setFValue(const double f) {
		f_value = f;
	}

	/* Operator overloading */
	/* Assignment operator */
	__host__ __device__ DiscretizedValue2D& operator=(const DiscretizedValue2D& d) {
		if (this != &d) {
			p = vec2d(d.p);
			f_value = d.f_value;
		}
		return *this;
	}

	__host__ __device__ void operator=(const double f) {
		this->f_value = f;
	}

	__host__ __device__ void operator=(const float f) {
		this->f_value = f;
	}

	/* Sum the value of the other point to this. Use the position of this for the new DiscretizedValue2D */
	__host__ __device__ double operator+(const DiscretizedValue2D& d) {
		return f_value + d.f_value;
	}

	/* Same as previous but with a scalar value */
	__host__ __device__ double operator+(const double& d) {
		return f_value + d;
	}

	/* Subtract the value of the other point to this. Use the position of this for the new DiscretizedValue2D */
	__host__ __device__ double operator-(const DiscretizedValue2D& d) {
		return f_value - d.f_value;
	}

	/* Same as previous but with a scalar value */
	__host__ __device__ double operator-(const double& d) {
		return f_value - d;
	}

	/* Multiply the value of the other point to this. Use the position of this for the new DiscretizedValue2D */
	__host__ __device__ double operator*(const DiscretizedValue2D& d) {
		return f_value * d.f_value;
	}

	/* Same as previous but with a scalar value */
	__host__ __device__ double operator*(const double& d) {
		return f_value * d;
	}

	/* Friend operators */
	__host__ __device__ friend double operator-(const double& v, const DiscretizedValue2D& d) {
		return v - d.f_value;
	}

	__host__ __device__ friend double operator+(const double& v, const DiscretizedValue2D& d) {
		return v + d.f_value;
	}

	__host__ __device__ friend double operator*(const double& v, const DiscretizedValue2D& d) {
		return v * d.f_value;
	}
};

#endif /* DISCRETIZEDVALUE2D_HPP_ */
