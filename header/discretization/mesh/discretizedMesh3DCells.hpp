/*
 * discretizedMesh3DCells.hpp
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#ifndef DISCRETIZEDMESH3DCELLS_HPP_
#define DISCRETIZEDMESH3DCELLS_HPP_

#include "types.hpp"

class DiscretizedValue3D;

class DiscretizedMesh3DCells {
	/* Discretization boundaries */
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double zmin;
	double zmax;
	/* Number of cells in each axis */
	Dim3D dim;
	/* Step delta in each axis */
	double dx;
	double dy;
	double dz;
	/* Size of the domain, mostly as shortcut for memory allocation */
	size_t domain_size;
	/* Store initial max and min */
	double start_max;
	double start_min;
	/* Domain for the host */
	DiscretizedValue3D* h_domain;
	/* Domain for the GPU */
	DiscretizedValue3D* d_domain;

	/* Initialize domain */
	void initializeDomain();
public:

	/* Constructor */
	DiscretizedMesh3DCells(const double, const double, const double, const double, const double, const double, const unsigned int, const unsigned int, const unsigned int);
	DiscretizedMesh3DCells(const DiscretizedMesh3DCells&);
	/* Destructor */
	virtual ~DiscretizedMesh3DCells();
	/* Assign operator */
	DiscretizedMesh3DCells& operator=(const DiscretizedMesh3DCells&);

	/* Initialize domain with a function */
	void initializeDomainFunction(double(*func)(const vec3d& p));

	/* Compute max of the field */
	double computeMax() const;

	/* Compute min of the field */
	double computeMin() const;

	/* Update host domain */
	void updateHostDomain();

	/* Update device domain */
	void updateDeviceDomain();

	/* Getters */
	/* Reference to pointer to the domains */
	DiscretizedValue3D* getHostDomain();
	DiscretizedValue3D* getDeviceDomain();

	/* Constant reference to pointer to the domains */
	DiscretizedValue3D* getHostDomain() const;
	DiscretizedValue3D* getDeviceDomain() const;

	size_t getDomainSizeT() const;
	unsigned int getDomainSize() const;

	unsigned int getCellsX() const;
	unsigned int getCellsY() const;
	unsigned int getCellsZ() const;

	double getMinX() const;
	double getMaxX() const;

	double getMinY() const;
	double getMaxY() const;

	double getMinZ() const;
	double getMaxZ() const;

	double getDeltaX() const;
	double getDeltaY() const;
	double getDeltaZ() const;

	double getStartMax() const;
	double getStartMin() const;

};

#endif /* DISCRETIZEDMESH3DCELLS_HPP_ */
