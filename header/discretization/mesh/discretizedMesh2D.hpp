/*
 * discretizedField2D.hpp
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#ifndef DISCRETIZEDFIELD2D_HPP_
#define DISCRETIZEDFIELD2D_HPP_

#include "types.hpp"

class DiscretizedValue2D;
class Mesh2DCrossGraphic;

class DiscretizedMesh2D {
private:
	/* Friend class Filed2DGraphic*/
	friend Mesh2DCrossGraphic;
	/* Discretization boundaries */
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	/* Number of nodes - 1 in each axis */
	Dim2D dim;
	/* Step delta in each axis */
	double dx;
	double dy;
	/* Domain for the host */
	DiscretizedValue2D* h_domain;
	/* Domain for the GPU */
	DiscretizedValue2D* d_domain;
	/* Size of the domain, mostly as shortcut for memory allocation */
	size_t domain_size;
	/* Store initial max and min */
	double start_max;
	double start_min;

	/* Initialize domain */
	void initializeDomain();
public:
	/* Constructor */
	DiscretizedMesh2D();
	DiscretizedMesh2D(const double, const double, const double, const double, const unsigned int, const unsigned int);

	virtual ~DiscretizedMesh2D();

	/* Initialize domain with a function */
	void initializeDomainFunction(double(*func)(const vec2d& p));

	/* Compute max of the field */
	double computeMax() const;

	/* Compute min of the field */
	double computeMin() const;

	/* Update host domain */
	void updateHostDomain();

	/* Update device domain */
	void updateDeviceDomain();

	/* Getters */
	/* Reference to pointer to the domains */
	DiscretizedValue2D*& getHostDomain();
	DiscretizedValue2D*& getDeviceDomain();

	/* Constant reference to pointer to the domains */
	DiscretizedValue2D* const & getHostDomain() const;
	DiscretizedValue2D* const & getDeviceDomain() const;

	size_t getDomainSizeT() const;
	unsigned int getDomainSize() const;

	unsigned int getNodeX() const;
	unsigned int getNodeY() const;

	double getDeltaX() const;
	double getDeltaY() const;

	double getStartMax() const;
	double getStartMin() const;
};

#endif /* DISCRETIZEDFIELD2D_HPP_ */
