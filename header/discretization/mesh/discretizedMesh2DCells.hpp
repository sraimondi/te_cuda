/*
 * discretizedField2DCells.hpp
 *
 *  Created on: Apr 2, 2015
 *      Author: simon
 */

#ifndef DISCRETIZEDFIELD2DCELLS_HPP_
#define DISCRETIZEDFIELD2DCELLS_HPP_

#include "types.hpp"

class DiscretizedValue2D;

class DiscretizedMesh2DCells {
	/* Discretization boundaries */
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	/* Number of cells in each axis */
	Dim2D dim;
	/* Step delta in each axis */
	double dx;
	double dy;
	/* Size of the domain, mostly as shortcut for memory allocation */
	size_t domain_size;
	/* Store initial max and min */
	double start_max;
	double start_min;
	/* Domain for the host */
	DiscretizedValue2D* h_domain;
	/* Domain for the GPU */
	DiscretizedValue2D* d_domain;

	/* Initialize domain */
	void initializeDomain();
public:

	/* Constructor */
	DiscretizedMesh2DCells(const double, const double, const double, const double, const unsigned int, const unsigned int);
	DiscretizedMesh2DCells(const DiscretizedMesh2DCells&);
	/* Destructor */
	virtual ~DiscretizedMesh2DCells();

	/* Assign operator */
	DiscretizedMesh2DCells& operator=(const DiscretizedMesh2DCells&);

	/* Initialize domain with a function */
	void initializeDomainFunction(double(*func)(const vec2d& p));

	/* Compute max of the field */
	double computeMax() const;

	/* Compute min of the field */
	double computeMin() const;

	/* Update host domain */
	void updateHostDomain();

	/* Update device domain */
	void updateDeviceDomain();

	/* Getters */
	/* Reference to pointer to the domains */
	DiscretizedValue2D* getHostDomain();
	DiscretizedValue2D* getDeviceDomain();

	/* Constant reference to pointer to the domains */
	DiscretizedValue2D* getHostDomain() const;
	DiscretizedValue2D* getDeviceDomain() const;

	size_t getDomainSizeT() const;
	unsigned int getDomainSize() const;

	unsigned int getCellsX() const;
	unsigned int getCellsY() const;

	double getMinX() const;
	double getMaxX() const;

	double getMinY() const;
	double getMaxY() const;

	double getDeltaX() const;
	double getDeltaY() const;

	double getStartMax() const;
	double getStartMin() const;
};


#endif /* DISCRETIZEDFIELD2DCELLS_HPP_ */
