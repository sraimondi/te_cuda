/*
 * vector3D.hpp
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#ifndef VECTOR3D_HPP_
#define VECTOR3D_HPP_

#include <iostream>
#include <math.h>

template <typename T>
class Vector3D {
private:
	double _x;
	double _y;
	double _z;
public:
	/* Constructor */
	__host__ __device__ Vector3D() : _x(0), _y(0), _z(0) {};
	__host__ __device__ Vector3D(const double x, const double y, const double z) : _x(x), _y(y), _z(z) {};

	__host__ __device__ Vector3D(const Vector3D& v) : _x(v._x), _y(v._y), _z(v._z) {};

	__host__ __device__ Vector3D& operator=(const Vector3D& v) {
		if (this != &v) {
			_x = v._x;
			_y = v._y;
			_z = v._z;
		}
		return *this;
	}

	__host__ __device__ virtual ~Vector3D() {};

	/* Sum vector */
	__host__ __device__ Vector3D sum(const Vector3D& v) const {
		Vector3D new_v = Vector3D(_x + v._x, _y + v._y, _z + v._z);
		return new_v;
	}

	__host__ __device__ Vector3D operator+(const Vector3D& v) const {
		return sum(v);
	}

	/* Subtract vector */
	__host__ __device__ Vector3D sub(const Vector3D& v) const {
		Vector3D new_v = Vector3D(_x - v._x, _y - v._y, _z - v._z);
		return new_v;
	}

	__host__ __device__ Vector3D operator-(const Vector3D& v) const {
		return sub(v);
	}

	/* Scalar product */
	__host__ __device__ Vector3D scalar(const float r) const {
		Vector3D new_v = Vector3D(_x * r, _y * r, _z * r);
		return new_v;
	}

	/* Scalar product */
	__host__ __device__ Vector3D scalar(const double r) const {
		Vector3D new_v = Vector3D(_x * r, _y * r, _z * r);
		return new_v;
	}

	__host__ __device__ Vector3D operator*(const float r) const {
		return scalar(r);
	}

	__host__ __device__ Vector3D operator*(const double r) const {
		return scalar(r);
	}

	__host__ __device__ friend Vector3D operator*(const float r, const Vector3D& v) {
		return v.scalar(r);
	}

	__host__ __device__ friend Vector3D operator*(const double r, const Vector3D& v) {
		return v.scalar(r);
	}

	/* Dot product */
	__host__ __device__ double dot(const Vector3D& v)const {
		return _x * v._x + _y * v._y + _z * v._z;
	}

	__host__ __device__ double operator*(const Vector3D& v) const {
		return dot(v);
	}

	/* Cross product */
	__host__ __device__ Vector3D cross(const Vector3D& v) const {
		Vector3D new_v = Vector3D(	_y * v._z - _z * v._y,
									_z * v._x - _x * v._z,
									_x * v._y - _y * v._x);
		return new_v;
	}

	__host__ __device__ Vector3D operator^(const Vector3D& v) const {
		return cross(v);
	}

	/* Norm of the vector */
	__host__ __device__ double norm() const {
		return sqrt(_x * _x + _y * _y + _z * _z);
	}

	/* Normalize vector */
	__host__ __device__ Vector3D normalize() const {
		if (this->norm() == 0.0) {
			return Vector3D(0, 0, 0);
		} else {
			return *this * (1.0 / this->norm());
		}
	}

	/* Test function to compare vectors */
	__host__ __device__ bool compare(const Vector3D& v) const {
		return _x == v._x && _y == v._y && _z == v._z;
	}

	__host__ __device__ bool operator==(const Vector3D& v) const {
		return compare(v);
	}

	/* Print vector */
	__host__ void print() const {
		std::cout << "x: " << _x << ", y: " << _y <<", z: " << _z << std::endl;
	}

	/* Getters */
	__host__ __device__ const double& getX() const {
		return _x;
	}

	__host__ __device__ const double& getY() const {
		return _y;
	}

	__host__ __device__ const double& getZ() const {
		return _z;
	}

};


#endif /* VECTOR3D_HPP_ */
