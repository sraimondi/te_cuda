/*
 * Vector2D.hpp
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#ifndef VECTOR2D_HPP_
#define VECTOR2D_HPP_

#include <iostream>
#include <math.h>

template <typename T>
class Vector2D {
private:
	T _x;
	T _y;
public:
	/* Constructor */
	__host__ __device__ Vector2D() : _x(static_cast<T>(0)), _y(static_cast<T>(0)) {};
	__host__ __device__ Vector2D(const T& x, const T& y) : _x(x), _y(y) {};

	__host__ __device__ Vector2D(const Vector2D& v) : _x(v._x), _y(v._y) {};
	__host__ __device__ Vector2D& operator=(const Vector2D& v) {
		if (this != &v) {
			_x = v._x;
			_y = v._y;
		}
		return *this;
	}

	__host__ __device__ virtual ~Vector2D() {};

	/* Sum vector */
	__host__ __device__ Vector2D sum(const Vector2D& v) const {
		Vector2D<T> new_v = Vector2D(_x + v._x, _y + v._y);
		return new_v;
	}

	__host__ __device__ Vector2D operator+(const Vector2D& v) const {
		return sum(v);
	}

	/* Subtract vector */
	__host__ __device__ Vector2D sub(const Vector2D& v) const {
		Vector2D<T> new_v = Vector2D(_x - v._x, _y - v._y);
		return new_v;
	}

	__host__ __device__ Vector2D operator-(const Vector2D& v) const {
		return sub(v);
	}

	/* Scalar product */
	__host__ __device__ Vector2D scalar(const float r) const {
		Vector2D<T> new_v = Vector2D(_x * r, _y * r);
		return new_v;
	}

	__host__ __device__ Vector2D scalar(const double r) const {
		Vector2D<T> new_v = Vector2D(_x * r, _y * r);
		return new_v;
	}

	__host__ __device__ Vector2D operator*(const float r) const {
		return scalar(r);
	}

	__host__ __device__ Vector2D operator*(const double r) const {
		return scalar(r);
	}

	__host__ __device__ friend Vector2D operator*(const float r, const Vector2D& v) {
		return v.scalar(r);
	}

	__host__ __device__ friend Vector2D operator*(const double r, const Vector2D& v) {
		return v.scalar(r);
	}

	/* Dot product */
	__host__ __device__ T dot(const Vector2D& v) const {
		return _x * v._x + _y * v._y;
	}

	__host__ __device__ T operator*(const Vector2D& v) const {
		return dot(v);
	}

	/* Cross product */
	__host__ __device__ T cross(const Vector2D& v) const {
		return _x * v._y - _y * v._x;
	}

	__host__ __device__ Vector2D operator^(const Vector2D& v) const {
		return cross(v);
	}

	/* Norm of the vector */
	__host__ __device__ T norm() const {
		return sqrt(_x * _x + _y * _y);
	}

	/* Normalize vector */
	__host__ __device__ Vector2D normalize() const {
		if (this->norm() == 0.0) {
			return Vector2D(static_cast<T>(0), static_cast<T>(0));
		} else {
			return *this * (static_cast<T>(1) / this->norm());
		}
	}

	/* Test function to compare vectors */
	__host__ __device__ bool compare(const Vector2D& v) const {
		return _x == v._x && _y == v._y;
	}

	__host__ __device__ bool operator==(const Vector2D& v) const {
		return compare(v);
	}

	/* Print vector */
	__host__ __device__ void print() const {
		std::cout << "x: " << _x << ", y: " << _y << std::endl;
	}

	/* Getters */
	__host__ __device__ const T& getX() const {
		return _x;
	}

	__host__ __device__ const T& getY() const {
		return _y;
	}

};

#endif /* VECTOR2D_HPP_ */
