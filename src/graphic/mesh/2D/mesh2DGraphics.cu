/*
 * meshGraphics.cu
 *
 *  Created on: Apr 7, 2015
 *      Author: simon
 */

#include "graphic/mesh/2D/mesh2DGraphics.hpp"
#include "cuda_utils/utils.hpp"
#include "cuda.h"
#include "cuda_gl_interop.h"

Mesh2DGraphic::Mesh2DGraphic()
	: dim(0, 0), vertex_color_id(0), index_id(0), vertex_color_resource(NULL), d_vertex_color(NULL), d_vertex_color_size(0) {}

Mesh2DGraphic::Mesh2DGraphic(unsigned int Nx, unsigned int Ny)
	: dim(Nx, Ny), vertex_color_id(0), index_id(0), d_vertex_color(NULL), vertex_color_resource(NULL), d_vertex_color_size(0) {}


Mesh2DGraphic::~Mesh2DGraphic() {
	CudaSafeCall(cudaGraphicsUnregisterResource(vertex_color_resource));
	glDeleteBuffers(1, &vertex_color_id);
	glDeleteBuffers(1, &index_id);
}

void Mesh2DGraphic::initInteroperabilityBuffer() {
	/* Assign graphics resources to vertex / color buffer */
	CudaSafeCall(cudaGraphicsGLRegisterBuffer(&vertex_color_resource, vertex_color_id, cudaGraphicsMapFlagsNone));
}

void Mesh2DGraphic::draw2DField() {
	/* Enable OpenGL client states */
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	/* Bind buffers to draw */
	glBindBuffer(GL_ARRAY_BUFFER, vertex_color_id);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_id);

	/* Set pointer to vertex array */
	glVertexPointer(3, GL_DOUBLE, 6 * sizeof(GLdouble), (GLvoid*)0);
	glColorPointer(3, GL_DOUBLE, 6 * sizeof(GLdouble), (GLvoid*)(3 * sizeof(GLdouble)));

	/* Draw mesh */
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, (dim.NX - 1) * (dim.NY - 1) * 2 * 3, GL_UNSIGNED_INT, (GLvoid*)0);

	/* Unbind buffers to draw */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	/* Disable OpenGL client states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

void Mesh2DGraphic::mapResourcesCUDA() {
	/* Map shared resources */
	CudaSafeCall(cudaGraphicsMapResources(1, &vertex_color_resource, NULL));
	/* Request pointer to the mapped resource */
	CudaSafeCall(cudaGraphicsResourceGetMappedPointer((void**)&d_vertex_color, &d_vertex_color_size, vertex_color_resource));
}

void Mesh2DGraphic::unmapResourcesCUDA() {
	/* Unmap CUDA resource to make sure that all operations are completed */
	CudaSafeCall(cudaGraphicsUnmapResources(1, &vertex_color_resource, NULL));
}

double*& Mesh2DGraphic::getDeviceVertexColorBuffer() {
	return d_vertex_color;
}


