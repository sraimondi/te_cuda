/*
 * Field2DGraphic.cu
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#include "graphic/mesh/2D/mesh2DCrossGraphic.hpp"
#include "graphic/graphic.hpp"
#include "discretization/mesh/discretizedMesh2D.hpp"
#include "discretization/discretizedValue2D.hpp"
#include "cuda_utils/utils.hpp"
#include "cuda.h"
#include "cuda_gl_interop.h"
#include <stdlib.h>


Mesh2DCrossGraphic::Mesh2DCrossGraphic()
	: mesh(NULL), Mesh2DGraphic() {
}

Mesh2DCrossGraphic::Mesh2DCrossGraphic(DiscretizedMesh2D* m)
	: mesh(m), Mesh2DGraphic(m->getNodeX() + 3, m->getNodeY() + 3) {
	generateBuffers();
	initInteroperabilityBuffer();
}

Mesh2DCrossGraphic::~Mesh2DCrossGraphic() {
	CudaSafeCall(cudaGraphicsUnregisterResource(vertex_color_resource));
	glDeleteBuffers(1, &vertex_color_id);
	glDeleteBuffers(1, &index_id);
	mesh = NULL;
}

void Mesh2DCrossGraphic::generateBuffers() {
	/* Create buffer to hold the data of the surface */
	glGenBuffers(1, &vertex_color_id);
	glGenBuffers(1, &index_id);

	unsigned int vertex_number = dim.NX * dim.NY;
	size_t vertex_size = vertex_number * 3 * 2 * sizeof(double);
	/* Create buffer to hold positions and colors */
	double* vertex_data = (double*)malloc(vertex_size);

	/* Fill color buffer */
	float r, g, b;
	/* Loop over domain data */
	/* Loop over the y axis */
	for (unsigned int j = 0; j < dim.NY; ++j) {
		/* Loop over the x axis */
		for (unsigned int i = 0; i < dim.NX; ++i) {
			unsigned int index = j * dim.NX + i;
			/* Get current position */
			DiscretizedValue2D current_point = mesh->h_domain[index];
			/* Compute position color */
			computePointColor(current_point, mesh->getStartMax(), mesh->getStartMin(), &r, &g, &b);
			/* Store position */
			vertex_data[index * 6] = current_point.getPosition().getX();
			vertex_data[index * 6 + 1] = current_point.getPosition().getY();
			vertex_data[index * 6 + 2] = 0.f;
			/* Store color */
			vertex_data[index * 6 + 3] = r;
			vertex_data[index * 6 + 4] = g;
			vertex_data[index * 6 + 5] = b;
		}
	}

	/* Copy data to the GPU */
	glBindBuffer(GL_ARRAY_BUFFER, vertex_color_id);
	glBufferData(GL_ARRAY_BUFFER, vertex_size, vertex_data, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	free(vertex_data);


	/* Create indexes buffer */
	unsigned int indexes_number = (dim.NX - 1) * (dim.NY - 1) * 2 * 3;
	size_t indexes_size = indexes_number * sizeof(unsigned int);
	unsigned int* indexes = (unsigned int*)malloc(indexes_size);
	unsigned int index_counter = 0;
	/* Loop over the y axis */
	for (int j = 0; j < dim.NY - 1; ++j) {
		/* Loop over the x axis */
		for (int i = 0; i < dim.NX - 1; ++i) {
			unsigned int p1_index = j * dim.NX + i;
			unsigned int p2_index = j * dim.NX + i + 1;
			unsigned int p3_index = (j + 1) * dim.NX + i + 1;
			unsigned int p4_index = (j + 1) * dim.NX + i;

			/* First triangle */
			indexes[index_counter++] = p1_index;
			indexes[index_counter++] = p2_index;
			indexes[index_counter++] = p3_index;

			/* Second triangle */
			indexes[index_counter++] = p1_index;
			indexes[index_counter++] = p3_index;
			indexes[index_counter++] = p4_index;
		}
	}

	/* Copy data to the GPU */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexes_size, indexes, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	free(indexes);
}

void Mesh2DCrossGraphic::updateBuffer() {
	unsigned int vertex_number = dim.NX * dim.NY;
	size_t vertex_size = vertex_number * 3 * 2 * sizeof(double);
	/* Create buffer to hold positions and colors */
	double* vertex_data = (double*)malloc(vertex_size);

	/* Fill vertex buffer */
	float r, g, b;
	/* Loop over domain data */
	/* Loop over the y axis */
	for (unsigned int j = 0; j < dim.NY; ++j) {
		/* Loop over the x axis */
		for (unsigned int i = 0; i < dim.NX; ++i) {
			unsigned int index = j * dim.NX + i;
			/* Get current position */
			DiscretizedValue2D current_point = mesh->h_domain[index];
			/* Compute position color */
			computePointColor(current_point, mesh->getStartMax(), mesh->getStartMin(), &r, &g, &b);
			/* Store position */
			vertex_data[index * 6] = current_point.getPosition().getX();
			vertex_data[index * 6 + 1] = current_point.getPosition().getY();
			vertex_data[index * 6 + 2] = 0.f;
			/* Store color */
			vertex_data[index * 6 + 3] = r;
			vertex_data[index * 6 + 4] = g;
			vertex_data[index * 6 + 5] = b;
		}
	}


	/* Copy data to the GPU */
	glBindBuffer(GL_ARRAY_BUFFER, vertex_color_id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_size, vertex_data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	free(vertex_data);
}
