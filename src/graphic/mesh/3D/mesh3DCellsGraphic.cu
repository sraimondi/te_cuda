/*
 * mesh3DCellsGraphic.cu
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#include "graphic/mesh/3D/mesh3DCellsGraphic.hpp"
#include "graphic/graphic.hpp"
#include "discretization/mesh/discretizedMesh3DCells.hpp"
#include "discretization/discretizedValue3D.hpp"
#include "utils/macros.hpp"
#include "cuda_utils/utils.hpp"
#include "cuda.h"
#include "cuda_gl_interop.h"

Mesh3DCellsGraphic::Mesh3DCellsGraphic(DiscretizedMesh3DCells& m)
	: mesh(m), dim(m.getCellsX(), m.getCellsY(), m.getCellsZ()), vertex_color_id(0), index_id(0), d_vertex_color(NULL), /*vertex_color_resource(NULL),*/ d_vertex_color_size(0) {
	generateBuffers();
	initInteroperabilityBuffer();
}

Mesh3DCellsGraphic::~Mesh3DCellsGraphic() {
	CudaSafeCall(cudaFree(d_vertex_color));
	glDeleteBuffers(1, &vertex_color_id);
	glDeleteBuffers(1, &index_id);
}

void Mesh3DCellsGraphic::generateBuffers() {
	/* Create buffer to hold the data of the surface */
	glGenBuffers(1, &vertex_color_id);
	glGenBuffers(1, &index_id);

	unsigned int vertex_number = dim.NX * dim.NY * dim.NZ;
	size_t vertex_size = vertex_number * (3 + 4) * sizeof(double);
	/* Create buffer to hold positions and colors */
	double* vertex_data = (double*)malloc(vertex_size);

	/* Fill color buffer */
	float r, g, b, alpha;
	/* Loop over domain data */
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim.NZ; ++k) {
		/* Loop over the y axis */
		for (unsigned int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (unsigned int i = 0; i < dim.NX; ++i) {
				unsigned int index = OFFSET3D(i, j, k, dim.NX, dim.NY);
				/* Get current position */
				DiscretizedValue3D current_point = mesh.getHostDomain()[index];
				/* Compute position color */
				computePointColor(current_point, mesh.getStartMax(), mesh.getStartMin(), &r, &g, &b, &alpha);
				/* Store position */
				vertex_data[index * 7] = current_point.getPosition().getX();
				vertex_data[index * 7 + 1] = current_point.getPosition().getY();
				vertex_data[index * 7 + 2] = current_point.getPosition().getZ();
				/* Store color */
				vertex_data[index * 7 + 3] = r;
				vertex_data[index * 7 + 4] = g;
				vertex_data[index * 7 + 5] = b;
				vertex_data[index * 7 + 6] = alpha;
			}
		}
	}

	/* Copy data to the GPU */
	glBindBuffer(GL_ARRAY_BUFFER, vertex_color_id);
	glBufferData(GL_ARRAY_BUFFER, vertex_size, vertex_data, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	free(vertex_data);


	/* Create indexes buffer */
	unsigned int indexes_number = dim.NX * dim.NY * dim.NZ;
	size_t indexes_size = indexes_number * sizeof(unsigned int);
	unsigned int* indexes = (unsigned int*)malloc(indexes_size);
	/* Loop over domain data */
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim.NZ; ++k) {
		/* Loop over the y axis */
		for (int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (int i = 0; i < dim.NX; ++i) {
				unsigned int index = OFFSET3D(i, j, k, dim.NX, dim.NY);
				/* Add point index */
				indexes[index] = index;
			}
		}
	}

	/* Copy data to the GPU */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexes_size, indexes, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	free(indexes);
}

void Mesh3DCellsGraphic::initInteroperabilityBuffer() {
	/* Assign graphics resources to vertex / color buffer */
	CudaSafeCall(cudaGLRegisterBufferObject(vertex_color_id));
}

void Mesh3DCellsGraphic::draw3DField() {
	/* Enable OpenGL client states */
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	/* Bind buffers to draw */
	glBindBuffer(GL_ARRAY_BUFFER, vertex_color_id);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_id);

	/* Set pointer to vertex array */
	glVertexPointer(3, GL_DOUBLE, 7 * sizeof(GLdouble), (GLvoid*)0);
	glColorPointer(4, GL_DOUBLE, 7 * sizeof(GLdouble), (GLvoid*)(3 * sizeof(GLdouble)));

	/* Draw mesh */
	glPointSize(3.f);
	glRotated(-90, 1, 0, 0);
	glTranslated(-(mesh.getMaxX() - mesh.getMinX()) / 2.0, -(mesh.getMaxY() - mesh.getMinY()) / 2.0, -(mesh.getMaxZ() - mesh.getMinZ()) / 2.0);
	glDrawElements(GL_POINTS, dim.NX * dim.NY * dim.NZ, GL_UNSIGNED_INT, (GLvoid*)0);
	glPointSize(1.f);

	/* Unbind buffers to draw */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	/* Disable OpenGL client states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

void Mesh3DCellsGraphic::mapResourcesCUDA() {
	/* Map shared resources */
	CudaSafeCall(cudaGLMapBufferObject((void**)&d_vertex_color, vertex_color_id));
}

void Mesh3DCellsGraphic::unmapResourcesCUDA() {
	/* Unmap CUDA resource to make sure that all operations are completed */
	CudaSafeCall(cudaGLUnmapBufferObject(vertex_color_id));
}

double*& Mesh3DCellsGraphic::getDeviceVertexColorBuffer() {
	return d_vertex_color;
}

void Mesh3DCellsGraphic::updateBuffer() {
	unsigned int vertex_number = dim.NX * dim.NY * dim.NZ;
	size_t vertex_size = vertex_number * (3 + 4) * sizeof(double);
	/* Create buffer to hold positions and colors */
	double* vertex_data = (double*)malloc(vertex_size);

	/* Fill color buffer */
	float r, g, b, alpha;
	/* Loop over domain data */
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim.NZ; ++k) {
		/* Loop over the y axis */
		for (unsigned int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (unsigned int i = 0; i < dim.NX; ++i) {
				unsigned int index = OFFSET3D(i, j, k, dim.NX, dim.NY);
				/* Get current position */
				DiscretizedValue3D current_point = mesh.getHostDomain()[index];
				/* Compute position color */
				computePointColor(current_point, mesh.getStartMax(), mesh.getStartMin(), &r, &g, &b, &alpha);
				/* Store position */
				vertex_data[index * 7] = current_point.getPosition().getX();
				vertex_data[index * 7 + 1] = current_point.getPosition().getY();
				vertex_data[index * 7 + 2] = current_point.getPosition().getZ();
				/* Store color */
				vertex_data[index * 7 + 3] = r;
				vertex_data[index * 7 + 4] = g;
				vertex_data[index * 7 + 5] = b;
				vertex_data[index * 7 + 6] = alpha;
			}
		}
	}

	/* Copy data to the GPU */
	glBindBuffer(GL_ARRAY_BUFFER, vertex_color_id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_size, vertex_data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	free(vertex_data);
}
