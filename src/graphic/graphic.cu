/*
 * graphic.cpp
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#include "graphic/graphic.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>


double* lookAt(const vec3d& cam_pos, const vec3d& look_at, const vec3d& up) {
	double* matrix = (double*)malloc(16 * sizeof(double));

	/* Compute look at matrix */
	vec3d f = (look_at - cam_pos).normalize();
	vec3d s = (f ^ up).normalize();
	vec3d u = s ^ f;

	/* First column */
	matrix[0] = s.getX();
	matrix[4] = s.getY();
	matrix[8] = s.getZ();
	matrix[12] = -(s * cam_pos);

	/* Second column */
	matrix[1] = u.getX();
	matrix[5] = u.getY();
	matrix[9] = u.getZ();
	matrix[13] = -(u * cam_pos);

	/* Third column */
	matrix[2] = -f.getX();
	matrix[6] = -f.getY();
	matrix[10] = -f.getZ();
	matrix[14] = f * cam_pos;

	/* Fourth column */
	matrix[3] = 0;
	matrix[7] = 0;
	matrix[11] = 0;
	matrix[15] = 1;

	return matrix;
}


void _check_gl_error(const char *file, int line) {
	GLenum err  = glGetError();

    while(err != GL_NO_ERROR) {
		switch(err) {
        	case GL_INVALID_OPERATION:      fprintf(stdout, "GL_%s - %s at %d\n", "INVALID_OPERATION", file, line);     	break;
            case GL_INVALID_ENUM:           fprintf(stdout, "GL_%s - %s at %d\n", "INVALID_ENUM", file, line);				break;
            case GL_INVALID_VALUE:          fprintf(stdout, "GL_%s - %s at %d\n", "INVALID_VALUE", file, line);          	break;
			case GL_OUT_OF_MEMORY:          fprintf(stdout, "GL_%s - %s at %d\n", "OUT_OF_MEMORY", file, line);         	break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:  fprintf(stdout, "GL_%s - %s at %d\n", "INVALID_FRAMEBUFFER_OPERATION", file, line);  break;
 	   	}
        err = glGetError();
	}
}

/* CUDA function */
__host__ __device__ void computePointColor(const DiscretizedValue2D& v, const double max, const double min, float* r, float* g, float* b) {
#ifdef NORMAL_COLOR
	/* Compute hue according to height */
	float h = 320.f - (320.f * (v.getFunctionValue() - min) / (max - min));
	h /= 60.f;
	int i = (int)h;

	float f = h - i;
	float p = 0;
	float q = 1 - f;
	float t = f;

	switch( i ) {
		case 0:
			*r = 1;
			*g = t;
			*b = p;
			break;
		case 1:
			*r = q;
			*g = 1;
			*b = p;
			break;
		case 2:
			*r = p;
			*g = 1;
			*b = t;
			break;
		case 3:
			*r = p;
			*g = q;
			*b = 1;
			break;
		case 4:
			*r = t;
			*g = p;
			*b = 1;
			break;
		default:		// case 5:
			*r = 1;
			*g = p;
			*b = q;
			break;
	}

#else
	/* Check if point is gone under the minimum */
	if (v.getFunctionValue() < 0.0) {
		*r = 1.0;
		*g = 0.0;
		*b = 0.0;
	} else {
		/* Compute value according to height */
		float grey = (v.getFunctionValue() - min) / (max - min);
		*r = grey;
		*g = grey;
		*b = grey;
	}
#endif


}

__host__ __device__ void computePointColor(const DiscretizedValue3D& v, const double max, const double min, float* r, float* g, float* b, float* alpha) {
#ifdef NORMAL_COLOR
	/* Compute hue according to height */
	float h = 320.f - (320.f * (v.getFunctionValue() - min) / (max - min));
	h /= 60.f;
	int i = (int)h;

	float f = h - i;
	float p = 0;
	float q = 1 - f;
	float t = f;

	switch( i ) {
		case 0:
			*r = 1;
			*g = t;
			*b = p;
			break;
		case 1:
			*r = q;
			*g = 1;
			*b = p;
			break;
		case 2:
			*r = p;
			*g = 1;
			*b = t;
			break;
		case 3:
			*r = p;
			*g = q;
			*b = 1;
			break;
		case 4:
			*r = t;
			*g = p;
			*b = 1;
			break;
		default:		// case 5:
			*r = 1;
			*g = p;
			*b = q;
			break;
	}
	/* Compute alpha */
	*alpha = fmax(0.1, (v.getFunctionValue() - min) / (max - min));

#else
	/* Check if point is gone under the minimum */
	if (v.getFunctionValue() < 0.0) {
		*r = 1.f;
		*g = 0.f;
		*b = 0.f;
		*alpha = 1.f;
	} else {
		/* Compute value according to height */
		float grey = (v.getFunctionValue() - min) / (max - min);
		*r = grey;
		*g = grey;
		*b = grey;
		/* Compute alpha */
		*alpha = fmax(0.1, (v.getFunctionValue() - min) / (max - min));
	}
#endif


}
