/*
 * main.cu
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#include "stdlib.h"
#include "stdio.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "graphic/mesh/2D/mesh2DCrossGraphic.hpp"
#include "graphic/mesh/2D/mesh2DCellsGraphic.hpp"
#include "graphic/mesh/3D/mesh3DCellsGraphic.hpp"
#include "discretization/mesh/discretizedMesh2D.hpp"
#include "discretization/mesh/discretizedMesh2DCells.hpp"
#include "discretization/mesh/discretizedMesh3DCells.hpp"
#include "graphic/graphic.hpp"
#include "types.hpp"
#include "solver/upwind_scheme/solverFOUSchemeUnsplit.hpp"
#include "solver/upwind_scheme/solverFOUSchemeSplit.hpp"
#include "solver/mpdata/mpdata2D.hpp"
#include "solver/mpdata/mpdata3D.hpp"
#include "cuda_utils/utils.hpp"
#include "cuda_gl_interop.h"
#include <math.h>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>

/* define to enable error check */
#define CHECK_ERROR


/* Define function prototypes */
static void error_callback(int error, const char* description);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void resize(GLFWwindow*, bool);

void update_view(GLFWwindow*);

/* Initial field value */
double inital_value_2D(const vec2d&);
double inital_value_3D(const vec3d&);

/* Compute FPS */
void calcFPS(GLFWwindow* window, double time_interval = 1.0, std::string theWindowTitle = "NONE");

/* Read input flags */
void parseFlags(int argc, char* argv[], bool& cpu, bool& three_d, unsigned int& Nx, unsigned int& Ny, unsigned int& Nz);

/* Data for the boundaries */
double xmin, xmax, ymin, ymax, zmin, zmax;

/* Number of step to compute */
static unsigned int steps = 4000;
static unsigned int step = 0;

/* Data for the camera navigation */
static vec2d prev_position;
static vec3d cam_pos;
static float phi, tao, r;

/* Pause simulation */
static bool pause = false;

/* Window title */
std::string windowTitle = "Transport equation ";

int main(int argc, char* argv[]) {

	GLFWwindow* window;
	/* Initialize boundaries */
	xmin = 0;
	xmax = 2;
	ymin = 0;
	ymax = 2;
	zmin = 0;
	zmax = 2;

	/* Pointer to the two solvers */
	Solver2D* solver2d;
	Solver3D* solver3d;

    /* Initialize camera position for 3D */
    r = 4.f;
    phi = 0.f;
    tao = M_PI / 2.f;

	bool cpu = true;
	bool three_d = false;
	unsigned int Nx = 0, Ny = 0, Nz = 0;

	/* Parse input */
	parseFlags(argc, argv, cpu, three_d, Nx, Ny, Nz);


	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_SAMPLES, 4);

	window = glfwCreateWindow(800, 800, windowTitle.c_str(), NULL, NULL);

	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	GLenum err = glewInit();
	if (GLEW_OK != err) {
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	glfwSwapInterval(1);

	glfwSetErrorCallback(error_callback);
	glfwSetKeyCallback(window, keyCallback);

	if (three_d) {
//		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glClearColor(0.0,0.0,0.0,0.0);
	}

	/* Ask for device with compute cap 3.5*/
	/* Device id */
	static int device_id = cudaChooseCompCapability(3, 5);

	/* Set selected device as CUDA and OpenGL device */
	CudaSafeCall(cudaGLSetGLDevice(device_id));


	double x, y;
	glfwGetCursorPos(window, &x, &y);
	prev_position = vec2d(x, y);

	/* Init field */
	DiscretizedMesh2DCells f_c(xmin, xmax, ymin, ymax, Nx, Ny);
	DiscretizedMesh3DCells f_c_3(xmin, xmax, ymin, ymax, zmin, zmax, Nx, Ny, Nz);

	f_c.initializeDomainFunction(&inital_value_2D);
	f_c_3.initializeDomainFunction(&inital_value_3D);

	Mesh2DCellsGraphic f_g_c(f_c);
	Mesh3DCellsGraphic f_g_c_3(f_c_3);

	if (three_d) {
		solver3d = new SolverMPDATA3D(f_c_3, 0, 0.001, 2, true);
	} else {
		solver2d = new SolverMPDATA2D(f_c, 0, 0.001, 3, true);
	}

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		resize(window, three_d);

		if (three_d) {
			update_view(window);
			f_g_c_3.draw3DField();
		} else {
			f_g_c.draw2DField();
		}

		if (!pause) {
			if (step < steps) {
				if (cpu) {
					if (three_d) {
						solver3d->computeStepCPU();
						f_g_c_3.updateBuffer();
					} else {
						solver2d->computeStepCPU();
						f_g_c.updateBuffer();
					}
				} else {
					if (three_d) {
						f_g_c_3.mapResourcesCUDA();
						solver3d->computeStepGPU(f_g_c_3.getDeviceVertexColorBuffer());
						f_g_c_3.unmapResourcesCUDA();
					} else {
						f_g_c.mapResourcesCUDA();
						solver2d->computeStepGPU(f_g_c.getDeviceVertexColorBuffer());
						f_g_c.unmapResourcesCUDA();
					}
				}
				step++;
			} else {
				std::cout << "Simulation done" << std::endl;
			}
		}


		calcFPS(window, 0.1, windowTitle);

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

#ifdef CHECK_ERROR
	/* Compute maximum error on the mesh */
	std::cout << "Checking for maximum error after " << step << " steps." << std::endl;
	double error = 0.0;
	if (three_d) {
		/* Update host domain if we did the simulation on the GPU */
		if (!cpu)
			f_c_3.updateHostDomain();

		/* Loop over the z axis */
		for (int k = 1; k < f_c_3.getCellsZ() - 1; k++) {
			/* Loop over the y axis */
			for (int j = 1; j < f_c_3.getCellsY() - 1; ++j) {
				/* Loop over the x axis */
				for (int i = 1; i < f_c_3.getCellsX() - 1; ++i) {
					DiscretizedValue3D d = f_c_3.getHostDomain()[k * f_c_3.getCellsX() * f_c_3.getCellsY() + j * f_c_3.getCellsX() + i];
					double e = fabs(d.getFunctionValue() - inital_value_3D(d.getPosition()));
					if (e > error)
						error = e;
				}
			}
		}
	} else {
		/* Update host domain if we did the simulation on the GPU */
		if (!cpu)
			f_c.updateHostDomain();
		/* Loop over the domain and check for the error */
		/* Loop over the y axis */
		for (int j = 1; j < f_c.getCellsY() - 1; ++j) {
			/* Loop over the x axis */
			for (int i = 1; i < f_c.getCellsX() - 1; ++i) {
				DiscretizedValue2D d = f_c.getHostDomain()[f_c.getCellsX() * j + i];
				double e = fabs(d.getFunctionValue() - inital_value_2D(d.getPosition()));
				if (e > error)
					error = e;
			}
		}
	}

	std::cout << "Error is: " << error << std::endl;
#endif

	glfwTerminate();

	if (three_d)
		free(solver3d);
	else
		free(solver2d);

	CudaSafeCall(cudaDeviceReset());

	exit(EXIT_SUCCESS);
}

/* Function implementation */
static void error_callback(int error, const char* description) {
	fputs(description, stderr);
}

void resize(GLFWwindow* window, bool three_d) {
	float ratio;
	int width, height;

	glfwGetFramebufferSize(window, &width, &height);
	ratio = width / (float) height;
	glViewport(0, 0, width, height);

	float x_window = fabs(xmax - xmin);
	float y_window = fabs(ymax - ymin);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (three_d) {
		glFrustum(-ratio, ratio, -1.f, 1.f, 0.9f, 10.f);
	} else {
		glOrtho(ratio * xmin - 0.1, ratio * xmax + 0.1, ymin - 0.1, ymax + 0.1, -1, 1);
	}


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

double inital_value_2D(const vec2d& v) {
	double value = 0;
	value = exp(-20.f * ((v.getX() - 1.0) * (v.getX() - 1.0) + (v.getY() - 1.0) * (v.getY() - 1.0)));

	return value;
}

double inital_value_3D(const vec3d& v) {
	double value = 0;
	value = exp(-10.f * ((v.getX() - 1.0) * (v.getX() - 1.0) + (v.getY() - 1.0) * (v.getY() - 1.0) + (v.getZ() - 1.0) * (v.getZ() - 1.0)));

	return value;
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_W && (action == GLFW_PRESS ||action == GLFW_REPEAT))
		r -= 0.5f;
	if (key == GLFW_KEY_S && (action == GLFW_PRESS ||action == GLFW_REPEAT))
		r += 0.5f;
	if (key == GLFW_KEY_P && (action == GLFW_PRESS ||action == GLFW_REPEAT))
		pause = !pause;
}

void calcFPS(GLFWwindow* window, double time_interval, std::string theWindowTitle) {
	// Static values which only get initialised the first time the function runs
	static double t0Value = glfwGetTime(); // Set the initial time to now
	static int fpsFrameCount = 0;        // Set the initial FPS frame count to 0
	static double fps = 0.0;           // Set the initial FPS value to 0.0

	// Get the current time in seconds since the program started (non-static, so executed every time)
	double currentTime = glfwGetTime();

	// Ensure the time interval between FPS checks is sane (low cap = 0.1s, high-cap = 10.0s)
	// Negative numbers are invalid, 10 fps checks per second at most, 1 every 10 secs at least.
	if (time_interval < 0.1) {
		time_interval = 0.1;
	}
	if (time_interval > 10.0) {
		time_interval = 10.0;
	}

	// Calculate and display the FPS every specified time interval
	if ((currentTime - t0Value) > time_interval) {
	// Calculate the FPS as the number of frames divided by the interval in seconds
		fps = (double) fpsFrameCount / (currentTime - t0Value);

		// If the user specified a window title to append the FPS value to...
		if (theWindowTitle != "NONE") {
			// Convert the fps value into a string using an output stringstream
			std::ostringstream stream;
			std::ostringstream s;
			stream << fps;
			std::string fpsString = stream.str();

			// Append the FPS value to the window title details
			theWindowTitle += " | FPS: " + fpsString;

			// Convert the new window title to a c_str and set it
			const char* pszConstString = theWindowTitle.c_str();
			glfwSetWindowTitle(window, pszConstString);
		} else // If the user didn't specify a window to append the FPS to then output the FPS to the console
		{
			std::cout << "FPS: " << fps << std::endl;
		}

		// Reset the FPS frame counter and set the initial time to be now
		fpsFrameCount = 0;
		t0Value = glfwGetTime();
	} else // FPS calculation time interval hasn't elapsed yet? Simply increment the FPS frame counter
	{
		fpsFrameCount++;
	}
}

void parseFlags(int argc, char* argv[], bool& cpu, bool& three_d, unsigned int& Nx, unsigned int& Ny, unsigned int& Nz) {
	/* Expected: -cpu or -gpu <number of x divisions> <number of y divisions> */
	/* Index of current argument parsed */
	unsigned int i = 1;
	bool cpu_read = false;
	bool nx_read = false;
	bool ny_read = false;
	if (argc < 4) {
		std::cout << "Usage is -cpu or -gpu <number of x divisions> <number of y divisions> " << std::endl;
		exit(EXIT_FAILURE);
	} else {
		while (i < argc) {
			/* Read first option, should be either -cpu or -gpu */
			if (strcmp(argv[i], "-cpu") == 0) {
				if (cpu_read) {
					std::cout << "Usage is -cpu or -gpu <number of x divisions> <number of y divisions> " << std::endl;
					exit(EXIT_FAILURE);
				}
				cpu = true;
				cpu_read = true;
			} else if (strcmp(argv[i], "-gpu") == 0) {
				if (cpu_read) {
					std::cout << "Usage is -cpu or -gpu <number of x divisions> <number of y divisions> " << std::endl;
					exit(EXIT_FAILURE);
				}
				cpu = false;
				cpu_read = true;
			} else if (!nx_read) {
				/* Read input numbers */
				Nx = atoi(argv[i]);
				nx_read = true;
			} else if (!ny_read) {
				Ny = atoi(argv[i]);
				ny_read = true;
			} else {
				Nz = atoi(argv[i]);
				three_d = true;
			}
			i++;
		}
	}
}

void update_view(GLFWwindow* window) {
    double x, y;
    float delta_x, delta_y;

    glfwGetCursorPos(window, &x, &y);

    delta_x = prev_position.getX() - x;
    delta_y = prev_position.getY() - y;

    prev_position = vec2d(x, y);

    if (delta_x > 0.f) {
        phi += 0.05f;
    } else if (delta_x < 0) {
        phi -= 0.05f;
    } else if (delta_y > 0.f) {
        tao -= 0.05f;
        if (tao < 0.01f) {
            tao = 0.01f;
        }
    } else if (delta_y < 0.f) {
        tao += 0.05f;
        if (tao > M_PI / 2.f) {
            tao = M_PI / 2.f;
        }
    }

    cam_pos = vec3d(r * sin(tao) * sin(phi), r * cos(tao), r * sin(tao) * cos(phi));

    double* M;
    if (tao == 0) {
        M = lookAt(cam_pos, vec3d(0.f, 0.f, 0.f), vec3d(0.f, 0.f, 1.f));
    } else {
        M = lookAt(cam_pos, vec3d(0.f,0.f,0.f), vec3d(0.f, 1.f, 0.f));
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMultMatrixd(M);

}
