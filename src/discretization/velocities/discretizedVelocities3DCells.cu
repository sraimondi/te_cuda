/*
 * discretizedVelocities3DCells.cu
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#include "discretization/velocities/discretizedVelocities3DCells.hpp"
#include "discretization/mesh/discretizedMesh3DCells.hpp"
#include "cuda_utils/utils.hpp"
#include "function/velocity.hpp"
#include "utils/macros.hpp"
#include <new>
#include <iostream>
#include <assert.h>

/* CUDA function */
__global__ void initVelocitiesXDevice(	double* d_x_velocities, double* d_x_pa_velocities,
										Dim3D dim_v_x,
										double xmin, double ymin, double zmin,
										double dx, double dy, double dz, double time, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim_v_x.NX && tid_y < dim_v_x.NY && tid_z < dim_v_x.NZ) {
		/* Compute position */
		vec3d position(xmin + tid_x * dx, ymin + (tid_y + 1 / 2.0) * dy, zmin + (tid_z + 1 / 2.0) * dz);
		/* Store initial velocity */
		vec3d v = v_field3D(position, time);
		double v_courant = v.getX() * dt / dx;
		d_x_velocities[OFFSET3D(tid_x, tid_y, tid_z, dim_v_x.NX, dim_v_x.NY)] = v_courant;
		/* Store initial partial velocities as the initial one */
		d_x_pa_velocities[OFFSET3D(tid_x, tid_y, tid_z, dim_v_x.NX, dim_v_x.NY)] = v_courant;
	}
}

__global__ void initVelocitiesYDevice(	double* d_y_velocities, double* d_y_pa_velocities,
										Dim3D dim_v_y,
										double xmin, double ymin, double zmin,
										double dx, double dy, double dz, double time, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim_v_y.NX && tid_y < dim_v_y.NY && tid_z < dim_v_y.NZ) {
		/* Compute position */
		vec3d position(xmin + (tid_x + 1 / 2.0) * dx, ymin + tid_y * dy, zmin + (tid_z + 1 / 2.0) * dz);
		/* Store initial velocity */
		vec3d v = v_field3D(position, time);
		double v_courant = v.getY() * dt / dy;
		d_y_velocities[OFFSET3D(tid_x, tid_y, tid_z, dim_v_y.NX, dim_v_y.NY)] = v_courant;
		/* Store initial partial velocities as the initial one */
		d_y_pa_velocities[OFFSET3D(tid_x, tid_y, tid_z, dim_v_y.NX, dim_v_y.NY)] = v_courant;
	}
}

__global__ void initVelocitiesZDevice(	double* d_z_velocities, double* d_z_pa_velocities,
										Dim3D dim_v_z,
										double xmin, double ymin, double zmin,
										double dx, double dy, double dz, double time, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim_v_z.NX && tid_y < dim_v_z.NY && tid_z < dim_v_z.NZ) {
		/* Compute position */
		vec3d position(xmin + (tid_x + 1 / 2.0) * dx, ymin + (tid_y + 1 / 2.0) * dy, zmin + tid_z * dz);
		/* Store initial velocity */
		vec3d v = v_field3D(position, time);
		double v_courant = v.getZ() * dt / dz;
		d_z_velocities[OFFSET3D(tid_x, tid_y, tid_z, dim_v_z.NX, dim_v_z.NY)] = v_courant;
		/* Store initial partial velocities as the initial one */
		d_z_pa_velocities[OFFSET3D(tid_x, tid_y, tid_z, dim_v_z.NX, dim_v_z.NY)] = v_courant;
	}
}

/* Compute partial velocities on the GPU */
__global__ void paVelocitiesXDevice(	double* pa_dest, double* pa_src,
										double* d_y_pa_velocities, double* d_z_pa_velocities,
										DiscretizedValue3D* u,
										Dim3D dim_v_x, Dim3D dim_v_y, Dim3D dim_v_z,
										double dx, double dy, double dz, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim_v_x.NX && tid_y < dim_v_x.NY && tid_z < dim_v_x.NZ) {
		/* Compute new partial velocity */
		computePaVelocitiesXAt(pa_dest, pa_src, d_y_pa_velocities, d_z_pa_velocities, u, dim_v_x, dim_v_y, dim_v_z, dx, dy, dz, dt, tid_x, tid_y, tid_z);
	}
}

__global__ void paVelocitiesYDevice(	double* pa_dest, double* pa_src,
										double* d_x_pa_velocities, double* d_z_pa_velocities,
										DiscretizedValue3D* u,
										Dim3D dim_v_y, Dim3D dim_v_x, Dim3D dim_v_z,
										double dx, double dy, double dz, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim_v_y.NX && tid_y < dim_v_y.NY && tid_z < dim_v_y.NZ) {
		/* Compute new partial velocity */
		computePaVelocitiesYAt(pa_dest, pa_src, d_x_pa_velocities, d_z_pa_velocities, u, dim_v_y, dim_v_x, dim_v_z, dx, dy, dz, dt, tid_x, tid_y, tid_z);
	}
}

__global__ void paVelocitiesZDevice(	double* pa_dest, double* pa_src,
										double* d_x_pa_velocities, double* d_y_pa_velocities,
										DiscretizedValue3D* u,
										Dim3D dim_v_z, Dim3D dim_v_x, Dim3D dim_v_y,
										double dx, double dy, double dz, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim_v_z.NX && tid_y < dim_v_z.NY && tid_z < dim_v_z.NZ) {
		/* Compute new partial velocity */
		computePaVelocitiesZAt(pa_dest, pa_src, d_x_pa_velocities, d_y_pa_velocities, u, dim_v_z, dim_v_x, dim_v_y, dx, dy, dz, dt, tid_x, tid_y, tid_z);
	}
}

/* Compute partial velocity at a certain position i j k */
__host__ __device__ void computePaVelocitiesXAt(	double* pa_dest, double* pa_src,
													double* pa_y, double* pa_z,
													DiscretizedValue3D* u,
													Dim3D dim_v_x, Dim3D dim_v_y, Dim3D dim_v_z,
													double dx, double dy, double dz, double dt,
													unsigned int i, unsigned int j, unsigned int k) {
	/* Compute Courant numbers */
	double V_x = pa_src[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)];

	/* Compute V_y */
	double V_y;
	if (i == 0) {
		V_y = 1 / 2.0 * (pa_y[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i, j + 1, k, dim_v_y.NX, dim_v_y.NY)]);
	} else if (i == dim_v_x.NX - 1) {
		V_y = 1 / 2.0 * (pa_y[OFFSET3D(i - 1, j, k, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i - 1, j + 1, k, dim_v_y.NX, dim_v_y.NY)]);
	} else {
		V_y = 1 / 4.0 * (	pa_y[OFFSET3D(i - 1, j, k, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i - 1, j + 1, k, dim_v_y.NX, dim_v_y.NY)] +
							pa_y[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i, j + 1, k, dim_v_y.NX, dim_v_y.NY)]);
	}

	/* Compute V_z */
	double V_z;
	if (i == 0) {
		V_z = 1 / 2.0 * (pa_z[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i, j, k + 1, dim_v_z.NX, dim_v_z.NY)]);
	} else if (i == dim_v_x.NX - 1) {
		V_z = 1 / 2.0 * (pa_z[OFFSET3D(i - 1, j, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i - 1, j, k + 1, dim_v_z.NX, dim_v_z.NY)]);
	} else {
		V_z = 1 / 4.0 * (pa_z[OFFSET3D(i - 1, j, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i - 1, j, k + 1, dim_v_z.NX, dim_v_z.NY)] +
				pa_z[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i, j, k + 1, dim_v_z.NX, dim_v_z.NY)]);
	}

	/* Compute partial velocity */
	/* Domain size */
	unsigned int NX = dim_v_z.NX + 2;
	unsigned int NY = dim_v_z.NY + 2;
	/* Adapted indices */
	unsigned int j_p = j + 1;
	unsigned int k_p = k + 1;

	/* Compute derivative on x direction */
	double D_x = (u[OFFSET3D(i + 1, j_p, k_p, NX, NY)] - u[OFFSET3D(i, j_p, k_p, NX, NY)]) / (u[OFFSET3D(i, j_p, k_p, NX, NY)] + u[OFFSET3D(i + 1, j_p, k_p, NX, NY)] + PSI);

	/* Compute derivative on y direction */
	double D_y = 	(u[OFFSET3D(i + 1, j_p + 1, k_p, NX, NY)] + u[OFFSET3D(i, j_p + 1, k_p, NX, NY)] - u[OFFSET3D(i, j_p - 1, k_p, NX, NY)] - u[OFFSET3D(i + 1, j_p - 1, k_p, NX, NY)]) /
					(u[OFFSET3D(i + 1, j_p + 1, k_p, NX, NY)] + u[OFFSET3D(i, j_p + 1, k_p, NX, NY)] + u[OFFSET3D(i, j_p - 1, k_p, NX, NY)] + u[OFFSET3D(i + 1, j_p - 1, k_p, NX, NY)] + PSI);

	/* Compute derivative on z direction */
	double D_z =	(u[OFFSET3D(i + 1, j_p, k_p + 1, NX, NY)] + u[OFFSET3D(i, j_p, k_p + 1, NX, NY)] - u[OFFSET3D(i + 1, j_p, k_p - 1, NX, NY)] - u[OFFSET3D(i, j_p, k_p - 1, NX, NY)]) /
					(u[OFFSET3D(i + 1, j_p, k_p + 1, NX, NY)] + u[OFFSET3D(i, j_p, k_p + 1, NX, NY)] + u[OFFSET3D(i + 1, j_p, k_p - 1, NX, NY)] + u[OFFSET3D(i, j_p, k_p - 1, NX, NY)] + PSI);

	/* Compute new partial velocity */
	double V_new = (fabs(V_x) - V_x * V_x) * D_x - ((V_x * V_y) / 2.0) * D_y - ((V_x * V_z) / 2.0) * D_z;

	/* Store new velocities */
	pa_dest[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] = V_new;

}

__host__ __device__ void computePaVelocitiesYAt(	double* pa_dest, double* pa_src,
													double* pa_x, double* pa_z,
													DiscretizedValue3D* u,
													Dim3D dim_v_y, Dim3D dim_v_x, Dim3D dim_v_z,
													double dx, double dy, double dz, double dt,
													unsigned int i, unsigned int j, unsigned int k) {
	/* Compute Courant numbers */
	double V_y = pa_src[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)];

	/* Compute V_x */
	double V_x;
	if (j == 0) {
		V_x = 1 / 2.0 * (pa_x[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j, k, dim_v_x.NX, dim_v_x.NY)]);
	} else if (j == dim_v_y.NY - 1) {
		V_x = 1 / 2.0 * (pa_x[OFFSET3D(i, j - 1, k, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j - 1, k, dim_v_x.NX, dim_v_x.NY)]);
	} else {
		V_x = 1 / 4.0 * (	pa_x[OFFSET3D(i, j - 1, k, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j - 1, k, dim_v_x.NX, dim_v_x.NY)] +
							pa_x[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j, k, dim_v_x.NX, dim_v_x.NY)]);
	}

	/* Compute V_z */
	double V_z;
	if (j == 0) {
		V_z = 1 / 2.0 * (pa_z[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i, j, k + 1, dim_v_z.NX, dim_v_z.NY)]);
	} else if (j == dim_v_y.NY - 1) {
		V_z = 1 / 2.0 * (pa_z[OFFSET3D(i, j - 1, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i, j - 1, k + 1, dim_v_z.NX, dim_v_z.NY)]);
	} else {
		V_z = 1 / 4.0 * (	pa_z[OFFSET3D(i, j - 1, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i, j - 1, k + 1, dim_v_z.NX, dim_v_z.NY)] +
							pa_z[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] + pa_z[OFFSET3D(i, j, k + 1, dim_v_z.NX, dim_v_z.NY)]);
	}

	/* Compute partial velocity */
	/* Domain size */
	unsigned int NX = dim_v_z.NX + 2;
	unsigned int NY = dim_v_z.NY + 2;
	/* Adapted indices */
	unsigned int i_p = i + 1;
	unsigned int k_p = k + 1;

	/* Compute derivative on y direction */
	double D_y = (u[OFFSET3D(i_p, j + 1, k_p, NX, NY)] - u[OFFSET3D(i_p, j, k_p, NX, NY)]) / (u[OFFSET3D(i_p, j + 1, k_p, NX, NY)] + u[OFFSET3D(i_p, j, k_p, NX, NY)] + PSI);

	/* Compute derivative on x direction */
	double D_x = 	(u[OFFSET3D(i_p + 1, j + 1, k_p, NX, NY)] + u[OFFSET3D(i_p + 1, j, k_p, NX, NY)] - u[OFFSET3D(i_p - 1, j + 1, k_p, NX, NY)] - u[OFFSET3D(i_p - 1, j, k_p, NX, NY)]) /
					(u[OFFSET3D(i_p + 1, j + 1, k_p, NX, NY)] + u[OFFSET3D(i_p + 1, j, k_p, NX, NY)] + u[OFFSET3D(i_p - 1, j + 1, k_p, NX, NY)] + u[OFFSET3D(i_p - 1, j, k_p, NX, NY)] + PSI);

	/* Compute derivative on z direction */
	double D_z =	(u[OFFSET3D(i_p, j + 1, k_p + 1, NX, NY)] + u[OFFSET3D(i_p, j, k_p + 1, NX, NY)] - u[OFFSET3D(i_p, j + 1, k_p - 1, NX, NY)] - u[OFFSET3D(i_p, j, k_p - 1, NX, NY)]) /
					(u[OFFSET3D(i_p, j + 1, k_p + 1, NX, NY)] + u[OFFSET3D(i_p, j, k_p + 1, NX, NY)] + u[OFFSET3D(i_p, j + 1, k_p - 1, NX, NY)] + u[OFFSET3D(i_p, j, k_p - 1, NX, NY)] + PSI);

	/* Compute new partial velocity */
	double V_new = (fabs(V_y) - V_y * V_y) * D_y - ((V_y * V_x) / 2.0) * D_x - ((V_y * V_z) / 2.0) * D_z;

	/* Store new velocities */
	pa_dest[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] = V_new;

}

__host__ __device__ void computePaVelocitiesZAt(	double* pa_dest, double* pa_src,
													double* pa_x, double* pa_y,
													DiscretizedValue3D* u,
													Dim3D dim_v_z, Dim3D dim_v_x, Dim3D dim_v_y,
													double dx, double dy, double dz, double dt,
													unsigned int i, unsigned int j, unsigned int k) {
	/* Compute Courant numbers */
	double V_z = pa_src[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)];

	/* Compute V_x */
	double V_x;
	if (k == 0) {
		V_x = 1 / 2.0 * (pa_x[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j, k, dim_v_x.NX, dim_v_x.NY)]);
	} else if (k == dim_v_z.NZ - 1) {
		V_x = 1 / 2.0 * (pa_x[OFFSET3D(i, j, k - 1, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j, k - 1, dim_v_x.NX, dim_v_x.NY)]);
	} else {
		V_x = 1 / 4.0 * (	pa_x[OFFSET3D(i, j, k - 1, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j, k - 1, dim_v_x.NX, dim_v_x.NY)] +
							pa_x[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] + pa_x[OFFSET3D(i + 1, j, k, dim_v_x.NX, dim_v_x.NY)]);
	}

	/* Compute V_y */
	double V_y;
	if (k == 0) {
		V_y = 1 / 2.0 * (pa_y[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i, j + 1, k, dim_v_y.NX, dim_v_y.NY)]);
	} else if (k == dim_v_z.NZ - 1) {
		V_y = 1 / 2.0 * (pa_y[OFFSET3D(i, j, k - 1, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i, j + 1, k - 1, dim_v_y.NX, dim_v_y.NY)]);
	} else {
		V_y = 1 / 4.0 * (	pa_y[OFFSET3D(i, j, k - 1, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i, j + 1, k - 1, dim_v_y.NX, dim_v_y.NY)] +
							pa_y[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] + pa_y[OFFSET3D(i, j + 1, k, dim_v_y.NX, dim_v_y.NY)]);
	}

	/* Compute partial velocity */
	/* Domain size */
	unsigned int NX = dim_v_z.NX + 2;
	unsigned int NY = dim_v_z.NY + 2;
	/* Adapted indices */
	unsigned int i_p = i + 1;
	unsigned int j_p = j + 1;

	/* Compute derivative on z direction */
	double D_z = (u[OFFSET3D(i_p, j_p, k + 1, NX,  NY)] - u[OFFSET3D(i_p, j_p, k, NX,  NY)]) / (u[OFFSET3D(i_p, j_p, k + 1, NX,  NY)] + u[OFFSET3D(i_p, j_p, k, NX,  NY)] + PSI);

	/* Compute derivative on x  direction */
	double D_x =	(u[OFFSET3D(i_p + 1, j_p, k + 1, NX,  NY)] + u[OFFSET3D(i_p + 1, j_p, k, NX,  NY)] - u[OFFSET3D(i_p - 1, j_p, k + 1, NX,  NY)] - u[OFFSET3D(i_p - 1, j_p, k, NX,  NY)]) /
					(u[OFFSET3D(i_p + 1, j_p, k + 1, NX,  NY)] + u[OFFSET3D(i_p + 1, j_p, k, NX,  NY)] + u[OFFSET3D(i_p - 1, j_p, k + 1, NX,  NY)] + u[OFFSET3D(i_p - 1, j_p, k, NX,  NY)] + PSI);

	/* Compute derivative on y  direction */
	double D_y =	(u[OFFSET3D(i_p, j_p + 1, k + 1, NX,  NY)] + u[OFFSET3D(i_p, j_p + 1, k, NX,  NY)] - u[OFFSET3D(i_p, j_p - 1, k + 1, NX,  NY)] - u[OFFSET3D(i_p, j_p - 1, k, NX,  NY)]) /
					(u[OFFSET3D(i_p, j_p + 1, k + 1, NX,  NY)] + u[OFFSET3D(i_p, j_p + 1, k, NX,  NY)] + u[OFFSET3D(i_p, j_p - 1, k + 1, NX,  NY)] + u[OFFSET3D(i_p, j_p - 1, k, NX,  NY)] + PSI);

	/* Compute new partial velocity */
	double V_new = (fabs(V_z) - V_z * V_z) * D_z - ((V_z * V_x) / 2.0) * D_x - ((V_z * V_y) / 2.0) * D_y;

	/* Store new velocities */
	pa_dest[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] = V_new;
}

DiscretizedVelocities3DCells::DiscretizedVelocities3DCells(DiscretizedMesh3DCells& m)
	: mesh(m),
	  dim_v_x(mesh.getCellsX() - 1, mesh.getCellsY() - 2, mesh.getCellsZ() - 2),
	  dim_v_y(mesh.getCellsX() - 2, mesh.getCellsY() - 1, mesh.getCellsZ() - 2),
	  dim_v_z(mesh.getCellsX() - 2, mesh.getCellsY() - 2, mesh.getCellsZ() - 1),
	  size_x(dim_v_x.NX * dim_v_x.NY * dim_v_x.NZ),
	  size_y(dim_v_y.NX * dim_v_y.NY * dim_v_y.NZ),
	  size_z(dim_v_z.NX * dim_v_z.NY * dim_v_z.NZ),
	  size_x_t(size_x * sizeof(double)), size_y_t(size_y * sizeof(double)), size_z_t(size_z * sizeof(double)) {
	/* Allocate space on host */
	h_x_velocities = new double[size_x];
	h_y_velocities = new double[size_y];
	h_z_velocities = new double[size_z];

	h_x_pa_velocities = new double[size_x];
	h_y_pa_velocities = new double[size_y];
	h_z_pa_velocities = new double[size_z];

	/* Allocate space on device */
	CudaSafeCall(cudaMalloc((void**)&d_x_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_velocities, size_y_t));
	CudaSafeCall(cudaMalloc((void**)&d_z_velocities, size_z_t));

	CudaSafeCall(cudaMalloc((void**)&d_x_pa_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_pa_velocities, size_y_t));
	CudaSafeCall(cudaMalloc((void**)&d_z_pa_velocities, size_z_t));
}

DiscretizedVelocities3DCells::DiscretizedVelocities3DCells(const DiscretizedVelocities3DCells& d)
	: mesh(d.mesh),
	  dim_v_x(d.dim_v_x),
	  dim_v_y(d.dim_v_y),
	  dim_v_z(d.dim_v_z),
	  size_x(d.size_x), size_y(d.size_y), size_z(d.size_z),
	  size_x_t(d.size_x_t), size_y_t(d.size_y_t), size_z_t(d.size_z_t) {
	/* Free old allocated space */
	delete [] h_x_velocities;
	delete [] h_y_velocities;
	delete [] h_z_velocities;
	delete [] h_x_pa_velocities;
	delete [] h_y_pa_velocities;
	delete [] h_z_pa_velocities;

	CudaSafeCall(cudaFree(d_x_velocities));
	CudaSafeCall(cudaFree(d_y_velocities));
	CudaSafeCall(cudaFree(d_z_velocities));

	CudaSafeCall(cudaFree(d_x_pa_velocities));
	CudaSafeCall(cudaFree(d_y_pa_velocities));
	CudaSafeCall(cudaFree(d_z_pa_velocities));

	/* Allocate space on host */
	assert(h_x_velocities == NULL);
	h_x_velocities = new double[size_x];
	assert(h_y_velocities == NULL);
	h_y_velocities = new double[size_y];
	assert(h_z_velocities == NULL);
	h_z_velocities = new double[size_z];

	assert(h_x_pa_velocities == NULL);
	h_x_pa_velocities = new double[size_x];
	assert(h_y_pa_velocities == NULL);
	h_y_pa_velocities = new double[size_y];
	assert(h_z_pa_velocities == NULL);
	h_z_pa_velocities = new double[size_z];

	/* Allocate space on device */
	CudaSafeCall(cudaMalloc((void**)&d_x_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_velocities, size_y_t));
	CudaSafeCall(cudaMalloc((void**)&d_z_velocities, size_z_t));

	CudaSafeCall(cudaMalloc((void**)&d_x_pa_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_pa_velocities, size_y_t));
	CudaSafeCall(cudaMalloc((void**)&d_z_pa_velocities, size_z_t));

	/* Copy memory content */
	memcpy(h_x_velocities, d.h_x_velocities, size_x_t);
	memcpy(h_y_velocities, d.h_y_velocities, size_y_t);
	memcpy(h_z_velocities, d.h_z_velocities, size_z_t);

	memcpy(d_x_pa_velocities, d.h_x_pa_velocities, size_x_t);
	memcpy(d_y_pa_velocities, d.h_y_pa_velocities, size_y_t);
	memcpy(d_z_pa_velocities, d.h_z_pa_velocities, size_z_t);

	CudaSafeCall(cudaMemcpy(d_x_velocities, d.d_x_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_y_velocities, d.d_y_velocities, size_y_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_z_velocities, d.d_z_velocities, size_z_t, cudaMemcpyDeviceToDevice));

	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d.d_x_pa_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d.d_y_pa_velocities, size_y_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_z_pa_velocities, d.d_z_pa_velocities, size_z_t, cudaMemcpyDeviceToDevice));

}

DiscretizedVelocities3DCells& DiscretizedVelocities3DCells::operator =(const DiscretizedVelocities3DCells& d) {
	if (this == &d) {
		return *this;
	} else {
		/* Copy fields */
		size_x = d.size_x;
		size_y = d.size_y;
		size_z = d.size_z;
		size_x_t = d.size_x_t;
		size_y_t = d.size_y_t;
		size_z_t = d.size_z_t;

		dim_v_x = Dim3D(d.dim_v_x);
		dim_v_y = Dim3D(d.dim_v_y);
		dim_v_z = Dim3D(d.dim_v_z);

		/* Free old allocated space */
		delete [] h_x_velocities;
		delete [] h_y_velocities;
		delete [] h_z_velocities;
		delete [] h_x_pa_velocities;
		delete [] h_y_pa_velocities;
		delete [] h_z_pa_velocities;

		CudaSafeCall(cudaFree(d_x_velocities));
		CudaSafeCall(cudaFree(d_y_velocities));
		CudaSafeCall(cudaFree(d_z_velocities));

		CudaSafeCall(cudaFree(d_x_pa_velocities));
		CudaSafeCall(cudaFree(d_y_pa_velocities));
		CudaSafeCall(cudaFree(d_z_pa_velocities));

		/* Allocate space on host */
		assert(h_x_velocities == NULL);
		h_x_velocities = new double[size_x];
		assert(h_y_velocities == NULL);
		h_y_velocities = new double[size_y];
		assert(h_z_velocities == NULL);
		h_z_velocities = new double[size_z];

		assert(h_x_pa_velocities == NULL);
		h_x_pa_velocities = new double[size_x];
		assert(h_y_pa_velocities == NULL);
		h_y_pa_velocities = new double[size_y];
		assert(h_z_pa_velocities == NULL);
		h_z_pa_velocities = new double[size_z];

		/* Allocate space on device */
		CudaSafeCall(cudaMalloc((void**)&d_x_velocities, size_x_t));
		CudaSafeCall(cudaMalloc((void**)&d_y_velocities, size_y_t));
		CudaSafeCall(cudaMalloc((void**)&d_z_velocities, size_z_t));

		CudaSafeCall(cudaMalloc((void**)&d_x_pa_velocities, size_x_t));
		CudaSafeCall(cudaMalloc((void**)&d_y_pa_velocities, size_y_t));
		CudaSafeCall(cudaMalloc((void**)&d_z_pa_velocities, size_z_t));

		/* Copy memory content */
		memcpy(h_x_velocities, d.h_x_velocities, size_x_t);
		memcpy(h_y_velocities, d.h_y_velocities, size_y_t);
		memcpy(h_z_velocities, d.h_z_velocities, size_z_t);

		memcpy(d_x_pa_velocities, d.h_x_pa_velocities, size_x_t);
		memcpy(d_y_pa_velocities, d.h_y_pa_velocities, size_y_t);
		memcpy(d_z_pa_velocities, d.h_z_pa_velocities, size_z_t);

		CudaSafeCall(cudaMemcpy(d_x_velocities, d.d_x_velocities, size_x_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_y_velocities, d.d_y_velocities, size_y_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_z_velocities, d.d_z_velocities, size_z_t, cudaMemcpyDeviceToDevice));

		CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d.d_x_pa_velocities, size_x_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d.d_y_pa_velocities, size_y_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_z_pa_velocities, d.d_z_pa_velocities, size_z_t, cudaMemcpyDeviceToDevice));


		return *this;
	}
}

DiscretizedVelocities3DCells::~DiscretizedVelocities3DCells() {
	delete [] h_x_velocities;
	delete [] h_y_velocities;
	delete [] h_z_velocities;
	delete [] h_x_pa_velocities;
	delete [] h_y_pa_velocities;
	delete [] h_z_pa_velocities;

	CudaSafeCall(cudaFree(d_x_velocities));
	CudaSafeCall(cudaFree(d_y_velocities));
	CudaSafeCall(cudaFree(d_z_velocities));

	CudaSafeCall(cudaFree(d_x_pa_velocities));
	CudaSafeCall(cudaFree(d_y_pa_velocities));
	CudaSafeCall(cudaFree(d_z_pa_velocities));
}

void DiscretizedVelocities3DCells::initVelocitiesHost(const double time, const double dt) {
	/* Compute velocities on the host */
	initVelocitiesX(time, dt);
	initVelocitiesY(time, dt);
	initVelocitiesZ(time, dt);
}

void DiscretizedVelocities3DCells::initVelocitiesDevice(const double time, const double dt) {
	/* Compute block and threads sizes for x velocities */
	dim3 local = dim3(8, 8, 8);
	unsigned int num_blocks_x_x = ceil(dim_v_x.NX / 8.f);
	unsigned int num_blocks_x_y = ceil(dim_v_x.NY / 8.f);
	unsigned int num_blocks_x_z = ceil(dim_v_x.NZ / 8.f);
	dim3 block_x = dim3(num_blocks_x_x, num_blocks_x_y, num_blocks_x_z);

	initVelocitiesXDevice<<<block_x, local>>>(	d_x_velocities, d_x_pa_velocities, dim_v_x,
												mesh.getMinX(), mesh.getMinY(), mesh.getMinZ(),
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(),
												time, dt);
	CudaCheckError();

	/* Compute block and threads sizes for y velocities */
	unsigned int num_blocks_y_x= ceil(dim_v_y.NX / 8.f);
	unsigned int num_blocks_y_y = ceil(dim_v_y.NY / 8.f);
	unsigned int num_blocks_y_z = ceil(dim_v_y.NZ / 8.f);
	dim3 block_y = dim3(num_blocks_y_x, num_blocks_y_y, num_blocks_y_z);
	initVelocitiesYDevice<<<block_y, local>>>(	d_y_velocities, d_y_pa_velocities, dim_v_y,
												mesh.getMinX(), mesh.getMinY(), mesh.getMinZ(),
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(),
												time, dt);
	CudaCheckError();

	/* Compute block and threads sizes for z velocities */
	unsigned int num_blocks_z_x= ceil(dim_v_z.NX / 8.f);
	unsigned int num_blocks_z_y = ceil(dim_v_z.NY / 8.f);
	unsigned int num_blocks_z_z = ceil(dim_v_z.NZ / 8.f);
	dim3 block_z = dim3(num_blocks_z_x, num_blocks_z_y, num_blocks_z_z);
	initVelocitiesZDevice<<<block_z, local>>>(	d_z_velocities, d_z_pa_velocities, dim_v_z,
												mesh.getMinX(), mesh.getMinY(), mesh.getMinZ(),
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(),
												time, dt);
	CudaCheckError();
}

void DiscretizedVelocities3DCells::initVelocitiesX(double time, const double dt) {
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim_v_x.NZ; k++) {
		/* Loop over y axis */
		for (unsigned int j = 0; j < dim_v_x.NY; j++) {
			/* Loop over x axis */
			for (unsigned int i = 0; i < dim_v_x.NX; i++) {
				/* Compute position */
				vec3d position(mesh.getMinX() + i * mesh.getDeltaX(), mesh.getMinY() + (j + 1 / 2.0) * mesh.getDeltaY(), mesh.getMinZ() + (k + 1 / 2.0) * mesh.getDeltaZ());
				/* Store initial velocity */
				vec3d v = v_field3D(position, time);
				double v_courant = v.getX() * dt / mesh.getDeltaX();
				h_x_velocities[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] = v_courant;
				/* Store initial partial velocities as the initial one */
				h_x_pa_velocities[OFFSET3D(i, j, k, dim_v_x.NX, dim_v_x.NY)] = v_courant;
			}
		}
	}
}

void DiscretizedVelocities3DCells::initVelocitiesY(double time, const double dt) {
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim_v_y.NZ; k++) {
		/* Loop over y axis */
		for (unsigned int j = 0; j < dim_v_y.NY; j++) {
			/* Loop over x axis */
			for (unsigned int i = 0; i < dim_v_y.NX; i++) {
				/* Compute position */
				vec3d position(mesh.getMinX() + (i + 1 /2.0) * mesh.getDeltaX(), mesh.getMinY() + j * mesh.getDeltaY(), mesh.getMinZ() + (k + 1 / 2.0) * mesh.getDeltaZ());
				/* Store initial velocity */
				vec3d v = v_field3D(position, time);
				double v_courant = v.getY() * dt / mesh.getDeltaY();
				h_y_velocities[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] = v_courant;
				/* Store initial partial velocities as the initial one */
				h_y_pa_velocities[OFFSET3D(i, j, k, dim_v_y.NX, dim_v_y.NY)] = v_courant;
			}
		}
	}
}

void DiscretizedVelocities3DCells::initVelocitiesZ(double time, const double dt) {
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim_v_z.NZ; k++) {
		/* Loop over y axis */
		for (unsigned int j = 0; j < dim_v_z.NY; j++) {
			/* Loop over x axis */
			for (unsigned int i = 0; i < dim_v_z.NX; i++) {
				/* Compute position */
				vec3d position(mesh.getMinX() + (i + 1 /2.0) * mesh.getDeltaX(), mesh.getMinY() + (j + 1 / 2.0) * mesh.getDeltaY(), mesh.getMinZ() + k * mesh.getDeltaZ());
				/* Store initial velocity */
				vec3d v = v_field3D(position, time);
				double v_courant = v.getZ() * dt / mesh.getDeltaZ();
				h_z_velocities[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] = v_courant;
				/* Store initial partial velocities as the initial one */
				h_z_pa_velocities[OFFSET3D(i, j, k, dim_v_z.NX, dim_v_z.NY)] =  v_courant;
			}
		}
	}
}

double* DiscretizedVelocities3DCells::computePaVelocitiesX(const double dt) {
	/* Allocate space to compute new partial velocities */
	double* h_new_x_pa_velocities;
	h_new_x_pa_velocities = new double[size_x];
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim_v_x.NZ; k++) {
		/* Loop over y axis */
		for (unsigned int j = 0; j < dim_v_x.NY; j++) {
			/* Loop over x axis */
			for (unsigned int i = 0; i < dim_v_x.NX; i++) {
				/* Compute partial velocity */
				computePaVelocitiesXAt(	h_new_x_pa_velocities, h_x_pa_velocities,
										h_y_pa_velocities, h_z_pa_velocities,
										mesh.getHostDomain(),
										dim_v_x, dim_v_y, dim_v_z,
										mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(), dt,
										i, j, k);
			}
		}
	}

	return h_new_x_pa_velocities;
}


double* DiscretizedVelocities3DCells::computePaVelocitiesY(const double dt) {
	/* Allocate space to compute new partial velocities */
	double* h_new_y_pa_velocities;
	h_new_y_pa_velocities = new double[size_y];
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim_v_y.NZ; k++) {
		/* Loop over y axis */
		for (unsigned int j = 0; j < dim_v_y.NY; j++) {
			/* Loop over x axis */
			for (unsigned int i = 0; i < dim_v_y.NX; i++) {
				/* Compute partial velocity */
				computePaVelocitiesYAt(	h_new_y_pa_velocities, h_y_pa_velocities,
										h_x_pa_velocities, h_z_pa_velocities,
										mesh.getHostDomain(),
										dim_v_y, dim_v_x, dim_v_z,
										mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(), dt,
										i, j, k);
			}
		}
	}

	return h_new_y_pa_velocities;
}


double* DiscretizedVelocities3DCells::computePaVelocitiesZ(const double dt) {
	/* Allocate space to compute new partial velocities */
	double* h_new_z_pa_velocities;
	h_new_z_pa_velocities = new double[size_z];
	/* Loop over the z axis */
	for (unsigned int k = 0; k < dim_v_z.NZ; k++) {
		/* Loop over y axis */
		for (unsigned int j = 0; j < dim_v_z.NY; j++) {
			/* Loop over x axis */
			for (unsigned int i = 0; i < dim_v_z.NX; i++) {
				/* Compute partial velocity */
				computePaVelocitiesZAt(	h_new_z_pa_velocities, h_z_pa_velocities,
										h_x_pa_velocities, h_y_pa_velocities,
										mesh.getHostDomain(),
										dim_v_z, dim_v_x, dim_v_y,
										mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(), dt,
										i, j, k);
			}
		}
	}

	return h_new_z_pa_velocities;
}

void DiscretizedVelocities3DCells::computePaVelocitiesHost(const double dt) {
	double* h_new_x_pa_v;
	double* h_new_y_pa_v;
	double* h_new_z_pa_v;

	h_new_x_pa_v = computePaVelocitiesX(dt);
	h_new_y_pa_v = computePaVelocitiesY(dt);
	h_new_z_pa_v = computePaVelocitiesZ(dt);

	/* Update partial velocities */
	memcpy(h_x_pa_velocities, h_new_x_pa_v, size_x_t);
	memcpy(h_y_pa_velocities, h_new_y_pa_v, size_y_t);
	memcpy(h_z_pa_velocities, h_new_z_pa_v, size_z_t);

	delete [] h_new_x_pa_v;
	delete [] h_new_y_pa_v;
	delete [] h_new_z_pa_v;
}


void DiscretizedVelocities3DCells::computePaVelocitiesDevice(const double dt) {
	/* Compute block and threads sizes for x velocities */
	unsigned int num_blocks_x_x = ceil(dim_v_x.NX / 8.f);
	unsigned int num_blocks_x_y = ceil(dim_v_x.NY / 8.f);
	unsigned int num_blocks_x_z = ceil(dim_v_x.NZ / 8.f);
	dim3 block_x = dim3(num_blocks_x_x, num_blocks_x_y, num_blocks_x_z);
	dim3 local = dim3(8, 8, 8);

	/* Allocate space to compute new partial velocities */
	double* d_new_x_pa_velocities;
	CudaSafeCall(cudaMalloc((void**)&d_new_x_pa_velocities, size_x_t));
	/* Call device Kernel */
	paVelocitiesXDevice<<<block_x, local>>>(d_new_x_pa_velocities, d_x_pa_velocities,
											d_y_pa_velocities, d_z_pa_velocities,
											mesh.getDeviceDomain(),
											dim_v_x, dim_v_y, dim_v_z,
											mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(), dt);
	CudaCheckError();


	/* Compute block and threads sizes for y velocities */
	unsigned int num_blocks_y_x = ceil(dim_v_y.NX / 8.f);
	unsigned int num_blocks_y_y = ceil(dim_v_y.NY / 8.f);
	unsigned int num_blocks_y_z = ceil(dim_v_y.NZ / 8.f);
	dim3 block_y = dim3(num_blocks_y_x, num_blocks_y_y, num_blocks_y_z);

	/* Allocate space to compute new partial velocities */
	double* d_new_y_pa_velocities;
	CudaSafeCall(cudaMalloc((void**)&d_new_y_pa_velocities, size_y_t));
	/* Call device Kernel */
	paVelocitiesYDevice<<<block_y, local>>>(d_new_y_pa_velocities, d_y_pa_velocities,
											d_x_pa_velocities, d_z_pa_velocities,
											mesh.getDeviceDomain(),
											dim_v_y, dim_v_x, dim_v_z,
											mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(), dt);
	CudaCheckError();

	/* Compute block and threads sizes for z velocities */
	unsigned int num_blocks_z_x = ceil(dim_v_z.NX / 8.f);
	unsigned int num_blocks_z_y = ceil(dim_v_z.NY / 8.f);
	unsigned int num_blocks_z_z = ceil(dim_v_z.NZ / 8.f);
	dim3 block_z = dim3(num_blocks_z_x, num_blocks_z_y, num_blocks_z_z);

	/* Allocate space to compute new partial velocities */
	double* d_new_z_pa_velocities;
	CudaSafeCall(cudaMalloc((void**)&d_new_z_pa_velocities, size_z_t));
	/* Call device Kernel */
	paVelocitiesZDevice<<<block_z, local>>>(d_new_z_pa_velocities, d_z_pa_velocities,
											d_x_pa_velocities, d_y_pa_velocities,
											mesh.getDeviceDomain(),
											dim_v_z, dim_v_x, dim_v_y,
											mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(), dt);
	CudaCheckError();

	/* Update partial velocities on X */
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d_new_x_pa_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaFree(d_new_x_pa_velocities));

	/* Update partial velocities on Y */
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d_new_y_pa_velocities, size_y_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaFree(d_new_y_pa_velocities));

	/* Update partial velocities on Z */
	CudaSafeCall(cudaMemcpy(d_z_pa_velocities, d_new_z_pa_velocities, size_z_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaFree(d_new_z_pa_velocities));
}

void DiscretizedVelocities3DCells::resetPaVelocitiesHost() {
	/* Copy velocities to partial velocities array */
	memcpy(h_x_pa_velocities, h_x_velocities, size_x_t);
	memcpy(h_y_pa_velocities, h_y_velocities, size_y_t);
	memcpy(h_z_pa_velocities, h_z_velocities, size_z_t);
}

void DiscretizedVelocities3DCells::resetPaVelocitiesDevice() {
	/* Copy velocities to partial velocities array */
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d_x_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d_y_velocities, size_y_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_z_pa_velocities, d_z_velocities, size_z_t, cudaMemcpyDeviceToDevice));
}


void DiscretizedVelocities3DCells::updateInitVelocitiesHost() {
	updateInitVelocitiesXHost();
	updateInitVelocitiesYHost();
	updateInitVelocitiesZHost();
}

void DiscretizedVelocities3DCells::updateInitVelocitiesDevice() {
	updateInitVelocitiesXDevice();
	updateInitVelocitiesYDevice();
	updateInitVelocitiesZDevice();
}

/* Update initial velocities */
void DiscretizedVelocities3DCells::updateInitVelocitiesXHost() {
	CudaSafeCall(cudaMemcpy(h_x_velocities, d_x_velocities, size_x_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities3DCells::updateInitVelocitiesYHost() {
	CudaSafeCall(cudaMemcpy(h_y_velocities, d_y_velocities, size_y_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities3DCells::updateInitVelocitiesZHost() {
	CudaSafeCall(cudaMemcpy(h_z_velocities, d_z_velocities, size_z_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities3DCells::updateInitVelocitiesXDevice() {
	CudaSafeCall(cudaMemcpy(d_x_velocities, h_x_velocities, size_x_t, cudaMemcpyHostToDevice));
}

void DiscretizedVelocities3DCells::updateInitVelocitiesYDevice() {
	CudaSafeCall(cudaMemcpy(d_y_velocities, h_y_velocities, size_y_t, cudaMemcpyHostToDevice));
}

void DiscretizedVelocities3DCells::updateInitVelocitiesZDevice() {
	CudaSafeCall(cudaMemcpy(d_z_velocities, h_z_velocities, size_z_t, cudaMemcpyHostToDevice));
}

/* Update partial velocities */
void DiscretizedVelocities3DCells::updatePaVelocitiesXHost() {
	CudaSafeCall(cudaMemcpy(h_x_pa_velocities, d_x_pa_velocities, size_x_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities3DCells::updatePaVelocitiesYHost() {
	CudaSafeCall(cudaMemcpy(h_y_pa_velocities, d_y_pa_velocities, size_y_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities3DCells::updatePaVelocitiesZHost() {
	CudaSafeCall(cudaMemcpy(h_z_pa_velocities, d_z_pa_velocities, size_z_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities3DCells::updatePaVelocitiesXDevice() {
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, h_x_pa_velocities, size_x_t, cudaMemcpyHostToDevice));
}

void DiscretizedVelocities3DCells::updatePaVelocitiesYDevice() {
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, h_y_pa_velocities, size_y_t, cudaMemcpyHostToDevice));
}

void DiscretizedVelocities3DCells::updatePaVelocitiesZDevice() {
	CudaSafeCall(cudaMemcpy(d_z_pa_velocities, h_z_pa_velocities, size_z_t, cudaMemcpyHostToDevice));
}


/* Getters for initial velocities */
double*& DiscretizedVelocities3DCells::getHostInitVelocitiesX() { return h_x_velocities; }

double*& DiscretizedVelocities3DCells::getHostInitVelocitiesY() { return h_y_velocities; }

double*& DiscretizedVelocities3DCells::getHostInitVelocitiesZ() { return h_z_velocities; }

double*& DiscretizedVelocities3DCells::getDeviceInitVelocitiesX() { return d_x_velocities; }

double*& DiscretizedVelocities3DCells::getDeviceInitVelocitiesY() { return d_y_velocities; }

double*& DiscretizedVelocities3DCells::getDeviceInitVelocitiesZ() { return d_z_velocities; }

/* Getters for the partial velocities */
double*& DiscretizedVelocities3DCells::getHostPaVelocitiesX() { return h_x_pa_velocities; }

double*& DiscretizedVelocities3DCells::getHostPaVelocitiesY() { return h_y_pa_velocities; }

double*& DiscretizedVelocities3DCells::getHostPaVelocitiesZ() { return h_z_pa_velocities; }

double*& DiscretizedVelocities3DCells::getDevicePaVelocitiesX() { return d_x_pa_velocities; }

double*& DiscretizedVelocities3DCells::getDevicePaVelocitiesY() { return d_y_pa_velocities; }

double*& DiscretizedVelocities3DCells::getDevicePaVelocitiesZ() { return d_z_pa_velocities; }
