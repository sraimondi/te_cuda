/*
 * discretizedVelocities2D.cu
 *
 *  Created on: Apr 2, 2015
 *      Author: simon
 */


#include "discretization/velocities/discretizedVelocities2DCells.hpp"
#include "discretization/mesh/discretizedMesh2DCells.hpp"
#include "cuda_utils/utils.hpp"
#include "function/velocity.hpp"
#include "utils/macros.hpp"
#include <assert.h>

/* CUDA functions */
__global__ void initVelocitiesXDevice(	double* d_x_velocities, double* d_x_pa_velocities,
										Dim2D dim_v_x,
										double xmin, double ymin,
										double dx, double dy, double time, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim_v_x.NX && tid_y < dim_v_x.NY) {
		/* Compute position */
		vec2d position(xmin + tid_x * dx, ymin + (tid_y + 1 / 2.0) * dy);
		/* Store initial velocity */
		vec2d v = v_field2D(position, time);
		double v_courant = v.getX() * dt / dx;
		d_x_velocities[OFFSET2D(tid_x, tid_y, dim_v_x.NX)] = v_courant;
		/* Store initial partial velocities as the initial one */
		d_x_pa_velocities[OFFSET2D(tid_x, tid_y, dim_v_x.NX)] = v_courant;
	}
}

__global__ void initVelocitiesYDevice(	double* d_y_velocities, double* d_y_pa_velocities,
										Dim2D dim_v_y,
										double xmin, double ymin,
										double dx, double dy, double time, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim_v_y.NX && tid_y < dim_v_y.NY) {
		/* Compute position */
		vec2d position(xmin + (tid_x + 1 / 2.0) * dx, ymin + tid_y * dy);
		/* Store initial velocity */
		vec2d v = v_field2D(position, time);
		double v_courant = v.getY() * dt / dy;
		d_y_velocities[OFFSET2D(tid_x, tid_y, dim_v_y.NX)] = v_courant;
		/* Store initial partial velocities as the initial one */
		d_y_pa_velocities[OFFSET2D(tid_x, tid_y, dim_v_y.NX)] = v_courant;
	}
}

__global__ void paVelocitiesXDevice(double* pa_dest, double* pa_src, double* d_y_pa_velocities,
									DiscretizedValue2D* u,
									Dim2D dim_v_x, Dim2D dim_v_y,
									double dx, double dy, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim_v_x.NX && tid_y < dim_v_x.NY) {
		/* Compute new partial velocity */
		computePaVelocitiesXAt(pa_dest, pa_src, d_y_pa_velocities, u, dim_v_x, dim_v_y, dx, dy, dt, tid_x, tid_y);
	}
}

__global__ void paVelocitiesYDevice(double* pa_dest, double* pa_src, double* d_x_pa_velocities,
									DiscretizedValue2D* u,
									Dim2D dim_v_y, Dim2D dim_v_x,
									double dx, double dy, double dt) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim_v_y.NX && tid_y < dim_v_y.NY) {
		/* Compute new partial velocity */
		computePaVelocitiesYAt(pa_dest, pa_src, d_x_pa_velocities, u, dim_v_y, dim_v_x, dx, dy, dt, tid_x, tid_y);
	}
}

__host__ __device__ void computePaVelocitiesXAt(double* pa_dest, double* pa_src, double* pa_y,
												DiscretizedValue2D* u,
												Dim2D dim_v_x, Dim2D dim_v_y,
												double dx, double dy, double dt,
												unsigned int i, unsigned int j) {
	/* Compute Courant numbers */
	double V_x = pa_src[OFFSET2D(i, j, dim_v_x.NX)];

	/* Compute V_y */
	double V_y;
	if (i == 0) {
		V_y = 1 / 2.0 * (pa_y[OFFSET2D(i, j, dim_v_y.NX)] + pa_y[OFFSET2D(i, j + 1, dim_v_y.NX)]);
	} else if (i == dim_v_x.NX - 1) {
		V_y = 1 / 2.0 * (pa_y[OFFSET2D(i - 1, j, dim_v_y.NX)] + pa_y[OFFSET2D(i - 1, j + 1, dim_v_y.NX)]);
	} else {
		V_y = 1 / 4.0 * (pa_y[OFFSET2D(i - 1, j, dim_v_y.NX)] + pa_y[OFFSET2D(i - 1, j + 1, dim_v_y.NX)] + pa_y[OFFSET2D(i, j, dim_v_y.NX)] + pa_y[OFFSET2D(i, j + 1, dim_v_y.NX)]);
	}


	/* Compute partial velocity */
	unsigned int NX = dim_v_x.NX + 1;
	unsigned int j_p = j + 1;
	/* Compute derivative on x direction */
	double D_x = (u[OFFSET2D(i + 1, j_p, NX)] - u[OFFSET2D(i, j_p, NX)]) / (u[OFFSET2D(i, j_p, NX)] + u[OFFSET2D(i + 1, j_p, NX)] + PSI);

	/* Compute derivative on y direction */
	double D_y = 	(u[OFFSET2D(i + 1, j_p + 1, NX)] + u[OFFSET2D(i, j_p + 1, NX)] - u[OFFSET2D(i, j_p - 1, NX)] - u[OFFSET2D(i + 1, j_p - 1, NX)]) /
					(u[OFFSET2D(i + 1, j_p + 1, NX)] + u[OFFSET2D(i, j_p + 1, NX)] + u[OFFSET2D(i, j_p - 1, NX)] + u[OFFSET2D(i + 1, j_p - 1, NX)] + PSI);

	/* Compute new partial velocity */
	double V_new = (fabs(V_x) - V_x * V_x) * D_x - ((V_x * V_y) / 2.0) * D_y;

	/* Store new velocities */
	pa_dest[OFFSET2D(i, j, dim_v_x.NX)] = V_new;
}

__host__ __device__ void computePaVelocitiesYAt(double* pa_dest, double* pa_src, double* pa_x, DiscretizedValue2D* u,
												Dim2D dim_v_y, Dim2D dim_v_x,
												double dx, double dy, double dt,
												unsigned int i, unsigned int j) {
	/* Compute Courant numbers */
	double V_y = pa_src[OFFSET2D(i, j, dim_v_y.NX)];

	/* Compute V_x */
	double V_x;
	if (j == 0) {
		V_x = 1 / 2.0 * (pa_x[OFFSET2D(i, j, dim_v_x.NX)] + pa_x[OFFSET2D(i + 1, j, dim_v_x.NX)]);
	} else if (j == dim_v_y.NY - 1) {
		V_x = 1 / 2.0 * (pa_x[OFFSET2D(i, j - 1, dim_v_x.NX)] + pa_x[OFFSET2D(i + 1, j - 1, dim_v_x.NX)]);
	} else {
		V_x = 1 / 4.0 * (pa_x[OFFSET2D(i, j - 1, dim_v_x.NX)] + pa_x[OFFSET2D(i, j, dim_v_x.NX)] + pa_x[OFFSET2D(i + 1, j - 1, dim_v_x.NX)] + pa_x[OFFSET2D(i + 1, j, dim_v_x.NX)]);
	}


	/* Compute partial velocity */
	unsigned int NX = dim_v_x.NX + 1;
	unsigned int i_p = i + 1;

	/* Compute derivative on y direction */
	double D_y = (u[OFFSET2D(i_p, j + 1, NX)] - u[OFFSET2D(i_p, j, NX)]) /  (u[OFFSET2D(i_p, j + 1, NX)] + u[OFFSET2D(i_p, j, NX)] + PSI);

	/* Compute derivative on x direction */
	double D_x = 	(u[OFFSET2D(i_p + 1, j + 1, NX)] + u[OFFSET2D(i_p + 1, j, NX)] - u[OFFSET2D(i_p - 1, j + 1, NX)] - u[OFFSET2D(i_p - 1, j, NX)]) /
					(u[OFFSET2D(i_p + 1, j + 1, NX)] + u[OFFSET2D(i_p + 1, j, NX)] + u[OFFSET2D(i_p - 1, j + 1, NX)] + u[OFFSET2D(i_p - 1, j, NX)] + PSI);

	/* Compute new partial velocity */
	double V_new = (fabs(V_y) - V_y * V_y) * D_y - ((V_y * V_x) / 2.0) * D_x;

	/* Store new velocities */
	pa_dest[OFFSET2D(i, j, dim_v_y.NX)] = V_new;
}


DiscretizedVelocities2DCells::DiscretizedVelocities2DCells(const DiscretizedMesh2DCells& m)
	: mesh(m),
	  dim_v_x(mesh.getCellsX() - 1, mesh.getCellsY() - 2),
	  dim_v_y(mesh.getCellsX() - 2, mesh.getCellsY() - 1),
	  size_x(dim_v_x.NX * dim_v_x.NY),
	  size_y(dim_v_y.NX * dim_v_y.NY),
	  size_x_t(size_x * sizeof(double)), size_y_t(size_y * sizeof(double)) {
	/* Allocate space on host */
	h_x_velocities = new double[size_x];;
	h_y_velocities = new double[size_y];
	h_x_pa_velocities = new double[size_x];
	h_y_pa_velocities = new double[size_y];
	/* Allocate space on device */
	CudaSafeCall(cudaMalloc((void**)&d_x_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_velocities, size_y_t));
	CudaSafeCall(cudaMalloc((void**)&d_x_pa_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_pa_velocities, size_y_t));
}

DiscretizedVelocities2DCells::DiscretizedVelocities2DCells(const DiscretizedVelocities2DCells& d)
	: mesh(d.mesh),
	  dim_v_x(d.dim_v_x),
	  dim_v_y(d.dim_v_y),
	  size_x(d.size_x), size_y(d.size_y),
	  size_x_t(d.size_x_t), size_y_t(d.size_y_t) {
	/* Free old allocated space */
	delete [] h_x_velocities;
	delete [] h_y_velocities;
	delete [] h_x_pa_velocities;
	delete [] h_y_pa_velocities;

	CudaSafeCall(cudaFree(d_x_velocities));
	CudaSafeCall(cudaFree(d_y_velocities));
	CudaSafeCall(cudaFree(d_x_pa_velocities));
	CudaSafeCall(cudaFree(d_y_pa_velocities));

	/* Allocate space on host */
	assert(h_x_velocities == NULL);
	h_x_velocities = new double[size_x];
	assert(h_y_velocities == NULL);
	h_y_velocities = new double[size_y];
	assert(h_x_pa_velocities == NULL);
	h_x_pa_velocities = new double[size_x];
	assert(h_y_pa_velocities == NULL);
	h_y_pa_velocities = new double[size_y];


	/* Allocate space on device */
	CudaSafeCall(cudaMalloc((void**)&d_x_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_velocities, size_y_t));
	CudaSafeCall(cudaMalloc((void**)&d_x_pa_velocities, size_x_t));
	CudaSafeCall(cudaMalloc((void**)&d_y_pa_velocities, size_y_t));

	/* Copy memory content */
	memcpy(h_x_velocities, d.h_x_velocities, size_x_t);
	memcpy(h_y_velocities, d.h_y_velocities, size_y_t);
	memcpy(d_x_pa_velocities, d.h_x_pa_velocities, size_x_t);
	memcpy(d_y_pa_velocities, d.h_y_pa_velocities, size_y_t);

	CudaSafeCall(cudaMemcpy(d_x_velocities, d.d_x_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_y_velocities, d.d_y_velocities, size_y_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d.d_x_pa_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d.d_y_pa_velocities, size_y_t, cudaMemcpyDeviceToDevice));
}

DiscretizedVelocities2DCells& DiscretizedVelocities2DCells::operator =(const DiscretizedVelocities2DCells& d) {
	if (this == &d) {
		return *this;
	} else {
		/* Copy fields */
		size_x = d.size_x;
		size_y = d.size_y;
		size_x_t = d.size_x_t;
		size_y_t = d.size_y_t;

		dim_v_x = Dim2D(d.dim_v_x);
		dim_v_y = Dim2D(d.dim_v_y);

		/* Free old allocated space */
		delete [] h_x_velocities;
		delete [] h_y_velocities;
		delete [] h_x_pa_velocities;
		delete [] h_y_pa_velocities;

		CudaSafeCall(cudaFree(d_x_velocities));
		CudaSafeCall(cudaFree(d_y_velocities));
		CudaSafeCall(cudaFree(d_x_pa_velocities));
		CudaSafeCall(cudaFree(d_y_pa_velocities));

		/* Allocate space on host */
		assert(h_x_velocities == NULL);
		h_x_velocities = new double[size_x];
		assert(h_y_velocities == NULL);
		h_y_velocities = new double[size_y];
		assert(h_x_pa_velocities == NULL);
		h_x_pa_velocities = new double[size_x];
		assert(h_y_pa_velocities == NULL);
		h_y_pa_velocities = new double[size_y];

		/* Allocate space on device */
		CudaSafeCall(cudaMalloc((void**)&d_x_velocities, size_x_t));
		CudaSafeCall(cudaMalloc((void**)&d_y_velocities, size_y_t));
		CudaSafeCall(cudaMalloc((void**)&d_x_pa_velocities, size_x_t));
		CudaSafeCall(cudaMalloc((void**)&d_y_pa_velocities, size_y_t));

		/* Copy memory content */
		memcpy(h_x_velocities, d.h_x_velocities, size_x_t);
		memcpy(h_y_velocities, d.h_y_velocities, size_y_t);
		memcpy(d_x_pa_velocities, d.h_x_pa_velocities, size_x_t);
		memcpy(d_y_pa_velocities, d.h_y_pa_velocities, size_y_t);

		CudaSafeCall(cudaMemcpy(d_x_velocities, d.d_x_velocities, size_x_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_y_velocities, d.d_y_velocities, size_y_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d.d_x_pa_velocities, size_x_t, cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d.d_y_pa_velocities, size_y_t, cudaMemcpyDeviceToDevice));

		return *this;
	}
}


DiscretizedVelocities2DCells::~DiscretizedVelocities2DCells() {
	delete [] h_x_velocities;
	delete [] h_y_velocities;
	delete [] h_x_pa_velocities;
	delete [] h_y_pa_velocities;

	CudaSafeCall(cudaFree(d_x_velocities));
	CudaSafeCall(cudaFree(d_y_velocities));
	CudaSafeCall(cudaFree(d_x_pa_velocities));
	CudaSafeCall(cudaFree(d_y_pa_velocities));
}

void DiscretizedVelocities2DCells::initVelocitiesHost(const double time, const double dt) {
	/* Compute velocities on the host */
	initVelocitiesX(time, dt);
	initVelocitiesY(time, dt);
}

void DiscretizedVelocities2DCells::initVelocitiesDevice(const double time, const double dt) {
	/* Compute block and threads sizes for x velocities */
	unsigned int num_blocks_x_x = ceil(dim_v_x.NX / 32.f);
	unsigned int num_blocks_x_y = ceil(dim_v_x.NY / 4.f);
	dim3 block_x = dim3(num_blocks_x_x, num_blocks_x_y, 1);
	dim3 local = dim3(32, 4, 1);

	initVelocitiesXDevice<<<block_x, local>>>(d_x_velocities, d_x_pa_velocities, dim_v_x, mesh.getMinX(), mesh.getMinY(), mesh.getDeltaX(), mesh.getDeltaY(), time, dt);
	CudaCheckError();

	/* Compute block and threads sizes for y velocities */
	unsigned int num_blocks_y_x= ceil(dim_v_y.NX / 32.f);
	unsigned int num_blocks_y_y = ceil(dim_v_y.NY / 4.f);
	dim3 block_y = dim3(num_blocks_y_x, num_blocks_y_y, 1);
	initVelocitiesYDevice<<<block_y, local>>>(d_y_velocities, d_y_pa_velocities, dim_v_y, mesh.getMinX(), mesh.getMinY(), mesh.getDeltaX(), mesh.getDeltaY(), time, dt);
	CudaCheckError();
}

void DiscretizedVelocities2DCells::initVelocitiesX(double time, const double dt) {
	/* Loop over y axis */
	for (unsigned int j = 0; j < dim_v_x.NY; j++) {
		/* Loop over x axis */
		for (unsigned int i = 0; i < dim_v_x.NX; i++) {
			/* Compute position */
			vec2d position(mesh.getMinX() + i * mesh.getDeltaX(), mesh.getMinY() + (j + 1 / 2.0) * mesh.getDeltaY());
			/* Store initial velocity */
			vec2d v = v_field2D(position, time);
			double v_courant = v.getX() * dt / mesh.getDeltaX();
			h_x_velocities[OFFSET2D(i, j, dim_v_x.NX)] =  v_courant;
			/* Store initial partial velocities as the initial one */
			h_x_pa_velocities[OFFSET2D(i, j, dim_v_x.NX)] = v_courant;
		}
	}
}

void DiscretizedVelocities2DCells::initVelocitiesY(double time, const double dt) {
	/* Loop over y axis */
	for (unsigned int j = 0; j < dim_v_y.NY; j++) {
		/* Loop over x axis */
		for (unsigned int i = 0; i < dim_v_y.NX; i++) {
			/* Compute position */
			vec2d position(mesh.getMinX() + (i + 1 / 2.0) * mesh.getDeltaX(), mesh.getMinY() + j * mesh.getDeltaY());
			/* Store initial velocity */
			vec2d v = v_field2D(position, time);
			double v_courant = v.getY() * dt / mesh.getDeltaY();
			h_y_velocities[OFFSET2D(i, j, dim_v_y.NX)] = v_courant;
			/* Store initial partial velocities as the initial one */
			h_y_pa_velocities[OFFSET2D(i, j, dim_v_y.NX)] = v_courant;
		}
	}
}


double* DiscretizedVelocities2DCells::computePaVelocitiesX(const double dt) {
	/* Allocate space to compute new partial velocities */
	double* h_new_x_pa_velocities;
	h_new_x_pa_velocities = new double[size_x];
	/* Loop over y axis */
	for (unsigned int j = 0; j < dim_v_x.NY; j++) {
		/* Loop over x axis */
		for (unsigned int i = 0; i < dim_v_x.NX; i++) {
			/* Compute partial velocity */
			computePaVelocitiesXAt(h_new_x_pa_velocities, h_x_pa_velocities, h_y_pa_velocities, mesh.getHostDomain(), dim_v_x, dim_v_y, mesh.getDeltaX(), mesh.getDeltaY(), dt, i, j);
		}
	}

	return h_new_x_pa_velocities;
}

double* DiscretizedVelocities2DCells::computePaVelocitiesY(const double dt) {
	/* Allocate space to compute new partial velocities */
	double* h_new_y_pa_velocities;
	h_new_y_pa_velocities = new double[size_y];
	/* Loop over y axis */
	for (unsigned int j = 0; j < dim_v_y.NY; j++) {
		/* Loop over x axis */
		for (unsigned int i = 0; i < dim_v_y.NX; i++) {
			computePaVelocitiesYAt(h_new_y_pa_velocities, h_y_pa_velocities, h_x_pa_velocities, mesh.getHostDomain(), dim_v_y, dim_v_x, mesh.getDeltaX(), mesh.getDeltaY(), dt, i, j);
		}
	}

	return h_new_y_pa_velocities;
}

void DiscretizedVelocities2DCells::computePaVelocitiesHost(const double dt) {
	/* Pointers to store new partial velocities */
	double* h_new_x_pa_v;
	double* h_new_y_pa_v;
	h_new_x_pa_v = computePaVelocitiesX(dt);
	h_new_y_pa_v = computePaVelocitiesY(dt);

	/* Update partial velocities */
	memcpy(h_x_pa_velocities, h_new_x_pa_v, size_x_t);
	memcpy(h_y_pa_velocities, h_new_y_pa_v, size_y_t);

	delete [] h_new_y_pa_v;
	delete [] h_new_x_pa_v;
}

void DiscretizedVelocities2DCells::computePaVelocitiesDevice(const double dt) {
	/* Compute block and threads sizes for x velocities */
	unsigned int num_blocks_x_x = ceil(dim_v_x.NX / 32.f);
	unsigned int num_blocks_x_y = ceil(dim_v_x.NY / 4.f);
	dim3 block_x = dim3(num_blocks_x_x, num_blocks_x_y, 1);
	dim3 local = dim3(32, 4, 1);
	/* Allocate space to compute new partial velocities */
	double* d_new_x_pa_velocities;
	CudaSafeCall(cudaMalloc((void**)&d_new_x_pa_velocities, size_x_t));
	paVelocitiesXDevice<<<block_x, local>>>(d_new_x_pa_velocities, d_x_pa_velocities, d_y_pa_velocities, mesh.getDeviceDomain(), dim_v_x, dim_v_y, mesh.getDeltaX(), mesh.getDeltaY(), dt);
	CudaCheckError();

	/* Compute block and threads sizes for y velocities */
	unsigned int num_blocks_y_x= ceil(dim_v_y.NX / 32.f);
	unsigned int num_blocks_y_y = ceil(dim_v_y.NY / 4.f);
	dim3 block_y = dim3(num_blocks_y_x, num_blocks_y_y, 1);
	/* Allocate space to compute new partial velocities */
	double* d_new_y_pa_velocities;
	CudaSafeCall(cudaMalloc((void**)&d_new_y_pa_velocities, size_y_t));
	paVelocitiesYDevice<<<block_y, local>>>(d_new_y_pa_velocities, d_y_pa_velocities, d_x_pa_velocities, mesh.getDeviceDomain(), dim_v_y, dim_v_x, mesh.getDeltaX(), mesh.getDeltaY(), dt);
	CudaCheckError();

	/* Update partial velocities on X */
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d_new_x_pa_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaFree(d_new_x_pa_velocities));

	/* Update partial velocities on Y */
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d_new_y_pa_velocities, size_y_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaFree(d_new_y_pa_velocities));
}

void DiscretizedVelocities2DCells::resetPaVelocitiesHost() {
	/* Copy velocities to partial velocities array */
	memcpy(h_x_pa_velocities, h_x_velocities, size_x_t);
	memcpy(h_y_pa_velocities, h_y_velocities, size_y_t);
}

void DiscretizedVelocities2DCells::resetPaVelocitiesDevice() {
	/* Copy velocities to partial velocities array */
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, d_x_velocities, size_x_t, cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, d_y_velocities, size_y_t, cudaMemcpyDeviceToDevice));
}

void DiscretizedVelocities2DCells::updateInitVelocitiesHost() {
	updateInitVelocitiesXHost();
	updateInitVelocitiesYHost();
}

void DiscretizedVelocities2DCells::updateInitVelocitiesDevice() {
	updateInitVelocitiesXDevice();
	updateInitVelocitiesYDevice();
}

/* Update initial velocities */
void DiscretizedVelocities2DCells::updateInitVelocitiesXHost() {
	CudaSafeCall(cudaMemcpy(h_x_velocities, d_x_velocities, size_x_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities2DCells::updateInitVelocitiesYHost() {
	CudaSafeCall(cudaMemcpy(h_y_velocities, d_y_velocities, size_y_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities2DCells::updateInitVelocitiesXDevice() {
	CudaSafeCall(cudaMemcpy(d_x_velocities, h_x_velocities, size_x_t, cudaMemcpyHostToDevice));
}

void DiscretizedVelocities2DCells::updateInitVelocitiesYDevice() {
	CudaSafeCall(cudaMemcpy(d_y_velocities, h_y_velocities, size_y_t, cudaMemcpyHostToDevice));
}

/* Update partial velocities */
void DiscretizedVelocities2DCells::updatePaVelocitiesXHost() {
	CudaSafeCall(cudaMemcpy(h_x_pa_velocities, d_x_pa_velocities, size_x_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities2DCells::updatePaVelocitiesYHost() {
	CudaSafeCall(cudaMemcpy(h_y_pa_velocities, d_y_pa_velocities, size_y_t, cudaMemcpyDeviceToHost));
}

void DiscretizedVelocities2DCells::updatePaVelocitiesXDevice() {
	CudaSafeCall(cudaMemcpy(d_x_pa_velocities, h_x_pa_velocities, size_x_t, cudaMemcpyHostToDevice));
}

void DiscretizedVelocities2DCells::updatePaVelocitiesYDevice() {
	CudaSafeCall(cudaMemcpy(d_y_pa_velocities, h_y_pa_velocities, size_y_t, cudaMemcpyHostToDevice));
}

/* Getters for initial velocities */
double*& DiscretizedVelocities2DCells::getHostInitVelocitiesX() { return h_x_velocities; }

double*& DiscretizedVelocities2DCells::getHostInitVelocitiesY() { return h_y_velocities; }

double*& DiscretizedVelocities2DCells::getDeviceInitVelocitiesX() { return d_x_velocities; }

double*& DiscretizedVelocities2DCells::getDeviceInitVelocitiesY() { return d_y_velocities; }

/* Getters for the partial velocities */
double*& DiscretizedVelocities2DCells::getHostPaVelocitiesX() { return h_x_pa_velocities; }

double*& DiscretizedVelocities2DCells::getHostPaVelocitiesY() { return h_y_pa_velocities; }

double*& DiscretizedVelocities2DCells::getDevicePaVelocitiesX() { return d_x_pa_velocities; }

double*& DiscretizedVelocities2DCells::getDevicePaVelocitiesY() { return d_y_pa_velocities; }
