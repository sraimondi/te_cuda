/*
 * discretizedMesh3DCells.cu
 *
 *  Created on: Apr 14, 2015
 *      Author: simon
 */

#include "discretization/mesh/discretizedMesh3DCells.hpp"
#include "discretization/discretizedValue3D.hpp"
#include "cuda_utils/utils.hpp"
#include "utils/macros.hpp"
#include <assert.h>

DiscretizedMesh3DCells::DiscretizedMesh3DCells(const double xmin, const double xmax, const double ymin, const double ymax, const double zmin, const double zmax, const unsigned int nx, const unsigned int ny, const unsigned int nz)
	: xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax), zmin(zmin), zmax(zmax),
	  dim(nx + 2, ny + 2, nz + 2), dx((xmax - xmin) / nx), dy((ymax - ymin) / ny), dz((zmax - zmin) / nz),
	  domain_size(dim.NX * dim.NY * dim.NZ * sizeof(DiscretizedValue3D)), start_max(0.0), start_min(0.0) {
	/* Allocate space on the host */
	h_domain = new DiscretizedValue3D[dim.NX * dim.NY * dim.NZ];
	/* Allocate space on the device */
	CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
	/* Initialize domain */
	initializeDomain();
}

DiscretizedMesh3DCells::DiscretizedMesh3DCells(const DiscretizedMesh3DCells& d)
	: xmin(d.xmin), xmax(d.xmax), ymin(d.ymin), ymax(d.ymax), zmin(d.zmin), zmax(d.zmax),
	  dim(d.dim), dx(d.dx), dy(d.dy), dz(d.dz),
	  domain_size(d.domain_size), start_max(d.start_max), start_min(d.start_min) {
	/* Free old memory */
	delete [] h_domain;
	CudaSafeCall(cudaFree(d_domain));
	/* Allocate space on the host */
	assert(h_domain == NULL);
	h_domain = new DiscretizedValue3D[dim.NX * dim.NY * dim.NZ];
	/* Copy memory content */
	memcpy(h_domain, d.h_domain, domain_size);
	/* Allocate space on the device */
	CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
	/* Copy memory content */
	CudaSafeCall(cudaMemcpy(d_domain, d.d_domain, domain_size, cudaMemcpyDeviceToDevice));
}

DiscretizedMesh3DCells& DiscretizedMesh3DCells::operator =(const DiscretizedMesh3DCells& d) {
	if (this == &d) {
		return *this;
	} else {
		/* Copy filed values */
		xmin = d.xmin;
		xmax = d.xmax;
		ymin = d.ymin;
		ymax = d.ymin;
		zmin = d.zmin;
		zmax = d.zmax;
		dim = Dim3D(d.dim);
		domain_size = d.domain_size;
		start_min = d.start_min;
		start_max = d.start_max;
		/* Free old memory */
		delete [] h_domain;
		CudaSafeCall(cudaFree(d_domain));
		/* Allocate space on the host */
		assert(h_domain == NULL);
		h_domain = new DiscretizedValue3D[dim.NX * dim.NY * dim.NZ];
		/* Copy memory content */
		memcpy(h_domain, d.h_domain, domain_size);
		/* Allocate space on the device */
		CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
		/* Copy memory content */
		CudaSafeCall(cudaMemcpy(d_domain, d.d_domain, domain_size, cudaMemcpyDeviceToDevice));

		return *this;
	}
}

DiscretizedMesh3DCells::~DiscretizedMesh3DCells() {
	delete [] h_domain;
	CudaSafeCall(cudaFree(d_domain));
}

void DiscretizedMesh3DCells::initializeDomain() {
	/* Loop over the z axis */
	for (int k = 0; k < dim.NZ; k++) {
		/* Loop over the y axis */
		for (int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (int i = 0; i < dim.NX; ++i) {
				double x = xmin - dx / 2.0 + i * dx;
				double y = ymin - dy / 2.0 + j * dy;
				double z = zmin - dz / 2.0 + k * dz;
				/* Add element to domain */
				h_domain[OFFSET3D(i, j, k, dim.NX, dim.NY)] = DiscretizedValue3D(x, y, z);
			}
		}
	}
}

void DiscretizedMesh3DCells::updateDeviceDomain() {
	/* Copy all data to the device */
	CudaSafeCall(cudaMemcpy(d_domain, h_domain, domain_size, cudaMemcpyHostToDevice));
}

void DiscretizedMesh3DCells::updateHostDomain() {
	/* Copy data back from device to host */
	CudaSafeCall(cudaMemcpy(h_domain, d_domain, domain_size, cudaMemcpyDeviceToHost));
}

void DiscretizedMesh3DCells::initializeDomainFunction(double(*func)(const vec3d& p)) {
	/* Loop over the z axis */
	for (int k = 0; k < dim.NZ; k++) {
		/* Loop over the y axis */
		for (int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (int i = 0; i < dim.NX; ++i) {
				/* Compute function value */
				 h_domain[OFFSET3D(i, j, k, dim.NX, dim.NY)].computeFunctionValue(func);
			}
		}
	}

	/* Store staring max and min */
	start_max = computeMax();
	std::cout << "Initial max: " << start_max << std::endl;
	start_min = computeMin();
	std::cout << "Initial min: " << start_min << std::endl;

	/* Once the domain is initialized, copy it in initial state to the GPU */
	updateDeviceDomain();
}

double DiscretizedMesh3DCells::computeMax() const {
	double max = h_domain[0].getFunctionValue();
	/* Loop over the z axis */
	for (int k = 0; k < dim.NZ; k++) {
		/* Loop over the y axis */
		for (unsigned int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (unsigned int i = 0; i < dim.NX; ++i) {
				DiscretizedValue3D current_point = h_domain[OFFSET3D(i, j, k, dim.NX, dim.NY)];
				if (current_point.getFunctionValue() > max) {
					max = current_point.getFunctionValue();
				}
			}
		}
	}
	return max;
}

double DiscretizedMesh3DCells::computeMin() const {
	double min = h_domain[0].getFunctionValue();

	/* Loop over the z axis */
	for (int k = 0; k < dim.NZ; k++) {
		/* Loop over the y axis */
		for (unsigned int j = 0; j < dim.NY; ++j) {
			/* Loop over the x axis */
			for (unsigned int i = 0; i < dim.NX; ++i) {
				DiscretizedValue3D current_point = h_domain[OFFSET3D(i, j, k, dim.NX, dim.NY)];
				if (current_point.getFunctionValue() < min) {
					min = current_point.getFunctionValue();
				}
			}
		}
	}
	return min;
}


size_t DiscretizedMesh3DCells::getDomainSizeT() const {
	return domain_size;
}

unsigned int DiscretizedMesh3DCells::getDomainSize() const {
	return dim.NX * dim.NY * dim.NZ;
}

DiscretizedValue3D* DiscretizedMesh3DCells::getHostDomain() {
	return h_domain;
}

DiscretizedValue3D* DiscretizedMesh3DCells::getDeviceDomain() {
	return d_domain;
}

DiscretizedValue3D* DiscretizedMesh3DCells::getHostDomain() const {
	return h_domain;
}

DiscretizedValue3D* DiscretizedMesh3DCells::getDeviceDomain() const {
	return d_domain;
}

unsigned int DiscretizedMesh3DCells::getCellsX() const { return dim.NX; }

unsigned int DiscretizedMesh3DCells::getCellsY() const { return dim.NY; }

unsigned int DiscretizedMesh3DCells::getCellsZ() const { return dim.NZ; }

double DiscretizedMesh3DCells::getMinX() const { return xmin; }

double DiscretizedMesh3DCells::getMaxX() const { return xmax; }

double DiscretizedMesh3DCells::getMinY() const { return ymin; }

double DiscretizedMesh3DCells::getMaxY() const { return ymax; }

double DiscretizedMesh3DCells::getMinZ() const { return zmin; }

double DiscretizedMesh3DCells::getMaxZ() const { return zmax; }

double DiscretizedMesh3DCells::getDeltaX() const { return dx; }

double DiscretizedMesh3DCells::getDeltaY() const { return dy; }

double DiscretizedMesh3DCells::getDeltaZ() const { return dz; }

double DiscretizedMesh3DCells::getStartMax() const { return start_max; }

double DiscretizedMesh3DCells::getStartMin() const { return start_min; }
