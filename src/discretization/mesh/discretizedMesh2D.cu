/*
 * discretizedField2D.cu
 *
 *  Created on: Mar 6, 2015
 *      Author: simon
 */

#include "discretization/mesh/discretizedMesh2D.hpp"
#include "discretization/discretizedValue2D.hpp"
#include "cuda_utils/utils.hpp"
#include "utils/macros.hpp"

DiscretizedMesh2D::DiscretizedMesh2D()
	: xmin(0.0), xmax(0.0), ymin(0.0), ymax(0.0), dim(0, 0), dx(0.0), dy(0.0), h_domain(0), domain_size((dim.NX + 3) * (dim.NY + 3) * sizeof(DiscretizedValue2D)), start_max(0.0), start_min(0.0) {
	CudaSafeCall(cudaMalloc((void**)&d_domain, 0));
}

DiscretizedMesh2D::DiscretizedMesh2D(const double xmin, const double xmax, const double ymin, const double ymax, const unsigned int nx, const unsigned int ny)
	: xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax), dim(nx, ny), dx((xmax - xmin) / nx), dy((ymax - ymin) / ny), domain_size((dim.NX + 3) * (dim.NY + 3) * sizeof(DiscretizedValue2D)), start_max(0.0), start_min(0.0) {
	/* Allocate space on the host */
	h_domain = reinterpret_cast<DiscretizedValue2D*>(malloc(domain_size));
	/* Allocate space on the device */
	CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
	/* Initialize domain */
	initializeDomain();
}

DiscretizedMesh2D::~DiscretizedMesh2D() {
	free(h_domain);
	CudaSafeCall(cudaFree(d_domain));
}

void DiscretizedMesh2D::initializeDomain() {
	/* Loop over the y axis */
	for (int j = 0; j < dim.NY + 3; ++j) {
		/* Loop over the x axis */
		for (int i = 0; i < dim.NX + 3; ++i) {
			double x = xmin - dx + i * dx;
			double y = ymin - dy + j * dy;
			/* Add element to domain */
			h_domain[OFFSET2D(i, j, dim.NX + 3)] = DiscretizedValue2D(x, y);
		}
	}
}

void DiscretizedMesh2D::updateHostDomain() {
	/* Copy data back from device to host */
	CudaSafeCall(cudaMemcpy(h_domain, d_domain, domain_size, cudaMemcpyDeviceToHost));
}

void DiscretizedMesh2D::updateDeviceDomain() {
	/* Copy all data to the device */
	CudaSafeCall(cudaMemcpy(d_domain, h_domain, domain_size, cudaMemcpyHostToDevice));
}

void DiscretizedMesh2D::initializeDomainFunction(double(*func)(const vec2d& p)) {
	/* Loop over the y axis */
	for (int j = 0; j < dim.NY + 3; ++j) {
		/* Loop over the x axis */
		for (int i = 0; i < dim.NX + 3; ++i) {
			/* Compute function value */
			 h_domain[OFFSET2D(i, j, dim.NX + 3)].computeFunctionValue(func);
		};
	}

	/* Store staring max and min */
	start_max = computeMax();
	std::cout << "Initial max: " << start_max << std::endl;
	start_min = computeMin();
	std::cout << "Initial min: " << start_min << std::endl;

	/* Once the domain is initialized, copy it in initial state to the GPU */
	updateDeviceDomain();
}

double DiscretizedMesh2D::computeMax() const {
	double max = h_domain[0].getFunctionValue();\

	/* Loop over the y axis */
	for (unsigned int j = 0; j < dim.NY + 3; ++j) {
		/* Loop over the x axis */
		for (unsigned int i = 0; i < dim.NX + 3; ++i) {
			DiscretizedValue2D current_point = h_domain[OFFSET2D(i, j, dim.NX + 3)];
			if (current_point.getFunctionValue() > max) {
				max = current_point.getFunctionValue();
			}
		}
	}
	return max;
}

double DiscretizedMesh2D::computeMin() const {
	double min = h_domain[0].getFunctionValue();

	/* Loop over the y axis */
	for (unsigned int j = 0; j < dim.NY + 3; ++j) {
		/* Loop over the x axis */
		for (unsigned int i = 0; i < dim.NX + 3; ++i) {
			DiscretizedValue2D current_point = h_domain[OFFSET2D(i, j, dim.NX + 3)];
			if (current_point.getFunctionValue() < min) {
				min = current_point.getFunctionValue();
			}
		}
	}
	return min;
}

size_t DiscretizedMesh2D::getDomainSizeT() const {
	return domain_size;
}

unsigned int DiscretizedMesh2D::getDomainSize() const {
	return (dim.NX + 3) * (dim.NY + 3);
}

DiscretizedValue2D*& DiscretizedMesh2D::getHostDomain() {
	return h_domain;
}

DiscretizedValue2D*& DiscretizedMesh2D::getDeviceDomain() {
	return d_domain;
}

DiscretizedValue2D* const & DiscretizedMesh2D::getHostDomain() const {
	return h_domain;
}

DiscretizedValue2D* const & DiscretizedMesh2D::getDeviceDomain() const {
	return d_domain;
}

unsigned int DiscretizedMesh2D::getNodeX() const {
	return dim.NX;
}

unsigned int DiscretizedMesh2D::getNodeY() const {
	return dim.NY;
}

double DiscretizedMesh2D::getDeltaX() const {
	return dx;
}

double DiscretizedMesh2D::getDeltaY() const {
	return dy;
}

double DiscretizedMesh2D::getStartMax() const {
	return start_max;
}

double DiscretizedMesh2D::getStartMin() const {
	return start_min;
}
