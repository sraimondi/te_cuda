/*
 * discretizedField2DCells.cpp
 *
 *  Created on: Apr 2, 2015
 *      Author: simon
 */

#include "discretization/mesh/discretizedMesh2DCells.hpp"
#include "discretization/discretizedValue2D.hpp"
#include "cuda_utils/utils.hpp"
#include "utils/macros.hpp"
#include <assert.h>

DiscretizedMesh2DCells::DiscretizedMesh2DCells(const double xmin, const double xmax, const double ymin, const double ymax, const unsigned int nx, const unsigned int ny)
	: xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax), dim(nx + 2, ny + 2), dx((xmax - xmin) / nx), dy((ymax - ymin) / ny), domain_size(dim.NX * dim.NY * sizeof(DiscretizedValue2D)), start_max(0.0), start_min(0.0) {
	/* Allocate space on the host */
	h_domain = new DiscretizedValue2D[dim.NX * dim.NY];
	/* Allocate space on the device */
	CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
	/* Initialize domain */
	initializeDomain();
}

DiscretizedMesh2DCells::DiscretizedMesh2DCells(const DiscretizedMesh2DCells& d)
	: xmin(d.xmin), xmax(d.xmax), ymin(d.ymin), ymax(d.ymax), dim(d.dim), dx(d.dx), dy(d.dy), domain_size(d.domain_size), start_max(d.start_max), start_min(d.start_min) {
	/* Free old memory */
	delete [] h_domain;
	CudaSafeCall(cudaFree(d_domain));
	/* Allocate space on the host */
	assert(h_domain == NULL);
	h_domain = new DiscretizedValue2D[dim.NX * dim.NY];
	/* Copy memory content */
	memcpy(h_domain, d.h_domain, domain_size);
	/* Allocate space on the device */
	CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
	/* Copy memory content */
	CudaSafeCall(cudaMemcpy(d_domain, d.d_domain, domain_size, cudaMemcpyDeviceToDevice));
}

DiscretizedMesh2DCells& DiscretizedMesh2DCells::operator =(const DiscretizedMesh2DCells& d) {
	if (this == &d) {
		return *this;
	} else {
		/* Copy filed values */
		xmin = d.xmin;
		xmax = d.xmax;
		ymin = d.ymin;
		ymax = d.ymin;
		dim = Dim2D(d.dim);
		domain_size = d.domain_size;
		start_min = d.start_min;
		start_max = d.start_max;
		/* Free old memory */
		delete [] h_domain;
		CudaSafeCall(cudaFree(d_domain));
		/* Allocate space on the host */
		assert(h_domain == NULL);
		h_domain = new DiscretizedValue2D[dim.NX * dim.NY];
		/* Copy memory content */
		memcpy(h_domain, d.h_domain, domain_size);
		/* Allocate space on the device */
		CudaSafeCall(cudaMalloc((void**)&d_domain, domain_size));
		/* Copy memory content */
		CudaSafeCall(cudaMemcpy(d_domain, d.d_domain, domain_size, cudaMemcpyDeviceToDevice));

		return *this;
	}
}

DiscretizedMesh2DCells::~DiscretizedMesh2DCells() {
	delete [] h_domain;
	CudaSafeCall(cudaFree(d_domain));
}

void DiscretizedMesh2DCells::initializeDomain() {
	/* Loop over the y axis */
	for (int j = 0; j < dim.NY; ++j) {
		/* Loop over the x axis */
		for (int i = 0; i < dim.NX; ++i) {
			double x = xmin - dx / 2.0 + i * dx;
			double y = ymin - dy / 2.0 + j * dy;
			/* Add element to domain */
			h_domain[OFFSET2D(i, j, dim.NX)] = DiscretizedValue2D(x, y);
		}
	}
}

void DiscretizedMesh2DCells::updateDeviceDomain() {
	/* Copy all data to the device */
	CudaSafeCall(cudaMemcpy(d_domain, h_domain, domain_size, cudaMemcpyHostToDevice));
}

void DiscretizedMesh2DCells::updateHostDomain() {
	/* Copy data back from device to host */
	CudaSafeCall(cudaMemcpy(h_domain, d_domain, domain_size, cudaMemcpyDeviceToHost));
}

void DiscretizedMesh2DCells::initializeDomainFunction(double(*func)(const vec2d& p)) {
	/* Loop over the y axis */
	for (int j = 0; j < dim.NY; ++j) {
		/* Loop over the x axis */
		for (int i = 0; i < dim.NX; ++i) {
			/* Compute function value */
			 h_domain[OFFSET2D(i, j, dim.NX)].computeFunctionValue(func);
		}
	}

	/* Store staring max and min */
	start_max = computeMax();
	std::cout << "Initial max: " << start_max << std::endl;
	start_min = computeMin();
	std::cout << "Initial min: " << start_min << std::endl;

	/* Once the domain is initialized, copy it in initial state to the GPU */
	updateDeviceDomain();
}

double DiscretizedMesh2DCells::computeMax() const {
	double max = h_domain[0].getFunctionValue();\

	/* Loop over the y axis */
	for (unsigned int j = 0; j < dim.NY; ++j) {
		/* Loop over the x axis */
		for (unsigned int i = 0; i < dim.NX; ++i) {
			DiscretizedValue2D current_point = h_domain[OFFSET2D(i, j, dim.NX)];
			if (current_point.getFunctionValue() > max) {
				max = current_point.getFunctionValue();
			}
		}
	}
	return max;
}

double DiscretizedMesh2DCells::computeMin() const {
	double min = h_domain[0].getFunctionValue();

	/* Loop over the y axis */
	for (unsigned int j = 0; j < dim.NY; ++j) {
		/* Loop over the x axis */
		for (unsigned int i = 0; i < dim.NX; ++i) {
			DiscretizedValue2D current_point = h_domain[OFFSET2D(i, j, dim.NX)];
			if (current_point.getFunctionValue() < min) {
				min = current_point.getFunctionValue();
			}
		}
	}
	return min;
}


size_t DiscretizedMesh2DCells::getDomainSizeT() const {
	return domain_size;
}

unsigned int DiscretizedMesh2DCells::getDomainSize() const {
	return dim.NX * dim.NY;
}

DiscretizedValue2D* DiscretizedMesh2DCells::getHostDomain() {
	return h_domain;
}

DiscretizedValue2D* DiscretizedMesh2DCells::getDeviceDomain() {
	return d_domain;
}

DiscretizedValue2D* DiscretizedMesh2DCells::getHostDomain() const {
	return h_domain;
}

DiscretizedValue2D* DiscretizedMesh2DCells::getDeviceDomain() const {
	return d_domain;
}

unsigned int DiscretizedMesh2DCells::getCellsX() const {
	return dim.NX;
}

unsigned int DiscretizedMesh2DCells::getCellsY() const {
	return dim.NY;
}

double DiscretizedMesh2DCells::getMinX() const {
	return xmin;
}

double DiscretizedMesh2DCells::getMaxX() const {
	return xmax;
}

double DiscretizedMesh2DCells::getMinY() const {
	return ymin;
}

double DiscretizedMesh2DCells::getMaxY() const {
	return ymax;
}

double DiscretizedMesh2DCells::getDeltaX() const {
	return dx;
}

double DiscretizedMesh2DCells::getDeltaY() const {
	return dy;
}

double DiscretizedMesh2DCells::getStartMax() const {
	return start_max;
}

double DiscretizedMesh2DCells::getStartMin() const {
	return start_min;
}
