#include "function/velocity.hpp"

__host__ __device__ vec2d v_field2D(const vec2d& v, const double t) {
	return v_rot2D(v, t);
}

__host__ __device__ vec3d v_field3D(const vec3d& v, const double t) {
	return v_rot3DXZ(v, t);
}

__host__ __device__ vec2d v_rot2D(const vec2d& v, const double t) {
	vec2d c(1, 1);
	vec2d c_v = v - c;
	vec2d s(c_v.getY(), -c_v.getX());

	return s;
}

__host__ __device__ vec3d v_rot3DXY(const vec3d& v, const double t) {
	vec3d c(1, 1, 1);
	vec3d c_v = v - c;
	vec3d s(c_v.getY(), -c_v.getX(), 0.0);

	return s;
}

__host__ __device__ vec3d v_rot3DXZ(const vec3d& v, const double t) {
	vec3d c(1, 1, 1);
	vec3d c_v = v - c;
	vec3d s(c_v.getZ(), 0.0, -c_v.getX());

	return s;
}

__host__ __device__ vec3d v_rot3DYZ(const vec3d& v, const double t) {
	vec3d c(1, 1, 1);
	vec3d c_v = v - c;
	vec3d s(0.0, c_v.getZ(), -c_v.getY());

	return s;
}
