/*
 * solver.cpp
 *
 *  Created on: Mar 8, 2015
 *      Author: simon
 */

#include "solver/solver2D.hpp"
#include "graphic/graphic.hpp"

__device__ void updateDeviceBufferColor2D(DiscretizedValue2D& point, double* d_vertex_color_buffer, unsigned int index, double max, double min) {
	float r, g, b;
	/* Compute position color */
	computePointColor(point, max, min, &r, &g, &b);
	/* Store color */
	d_vertex_color_buffer[index * 6 + 3] = r;
	d_vertex_color_buffer[index * 6 + 4] = g;
	d_vertex_color_buffer[index * 6 + 5] = b;
}

Solver2D::Solver2D()
	: dt(0), time(0), dim(0, 0) {}

Solver2D::Solver2D(const double t, const double dt, const unsigned int Nx, const unsigned int Ny)
	: dt(dt), time(t), dim(Nx, Ny) {}

Solver2D::Solver2D(const Solver2D& s)
	: dt(s.dt), time(s.time), dim(s.dim) {}

Solver2D& Solver2D::operator =(const Solver2D& s) {
	if(this == &s) {
		return *this;
	} else {
		dim = Dim2D(s.dim);
		dt = s.dt;
		time = s.time;

		return *this;
	}
}

Solver2D::~Solver2D() {}
