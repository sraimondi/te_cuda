/*
 * solver3D.cu
 *
 *  Created on: Apr 15, 2015
 *      Author: simon
 */

#include "solver/solver3D.hpp"
#include "graphic/graphic.hpp"

__device__ void updateDeviceBufferColor3D(DiscretizedValue3D& point, double* d_vertex_color_buffer, unsigned int index, double max, double min) {
	float r, g, b, alpha;
	/* Compute position color */
	computePointColor(point, max, min, &r, &g, &b, &alpha);
	/* Store color */
	d_vertex_color_buffer[index * 7 + 3] = r;
	d_vertex_color_buffer[index * 7 + 4] = g;
	d_vertex_color_buffer[index * 7 + 5] = b;
	d_vertex_color_buffer[index * 7 + 6] = alpha;
}

Solver3D::Solver3D()
	: dt(0), time(0), dim(0, 0, 0) {}

Solver3D::Solver3D(const double t, const double dt, const unsigned int Nx, const unsigned int Ny, const unsigned int Nz)
	: dt(dt), time(t), dim(Nx, Ny, Nz) {}

Solver3D::Solver3D(const Solver3D& s)
	: dt(s.dt), time(s.time), dim(s.dim) {}

Solver3D& Solver3D::operator =(const Solver3D& s) {
	if (this == & s) {
		return *this;
	} else {
		dt = s.dt;
		time = s.time;
		dim = Dim3D(s.dim);

		return *this;
	}
}

Solver3D::~Solver3D() {}


