/*
 * solverFOUSchemeSplit.cpp
 *
 *  Created on: Mar 9, 2015
 *      Author: simon
 */

#include "solver/upwind_scheme/solverFOUSchemeSplit.hpp"
#include "discretization/mesh/discretizedMesh2D.hpp"
#include "function/velocity.hpp"
#include "function/boundary.hpp"
#include "cuda_utils/utils.hpp"
#include "utils/macros.hpp"
#include <iostream>
#include "stdlib.h"
#include <math.h>

/* CUDA functions */
/* __global__ function to call to compute a step on the device in X direction*/
__global__ void computeStepDeviceFOUSSplitX(DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
											double time, double dt, Dim2D dim, double dx, double dy,
											double field_max, double field_min) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim.NX && tid_y < dim.NY) {
		/* Compute new value of the domain at position i, j in X direction */
		/* Check if we are at the boundaries */
		if (tid_y == 0 || tid_y == dim.NY - 1 || tid_x == 0 || tid_x == dim.NX - 1) {
			computeBoundaryFOUSSplit(dest, src, d_vertex_color_buffer, tid_x, tid_y, time, dt, dim, dx, dy, field_max, field_min);
		} else {
			computeStepAtPositionFOUSSplitX(dest, src, d_vertex_color_buffer, tid_x, tid_y, time, dt, dim, dx, field_max, field_min);
		}
	}
}

/* __global__ function to call to compute a step on the device in Y direction*/
__global__ void computeStepDeviceFOUSSplitY(DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
											double time, double dt, Dim2D dim, double dx, double dy,
											double field_max, double field_min) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim.NX && tid_y < dim.NY) {
		/* Compute new value of the domain at position i, j in Y direction */
		/* Check if we are at the boundaries */
		if (tid_y == 0 || tid_y == dim.NY - 1 || tid_x == 0 || tid_x == dim.NX - 1) {
			computeBoundaryFOUSSplit(dest, src, d_vertex_color_buffer, tid_x, tid_y, time, dt, dim, dx, dy, field_max, field_min);
		} else {
			computeStepAtPositionFOUSSplitY(dest, src, d_vertex_color_buffer, tid_x, tid_y, time, dt, dim, dy, field_max, field_min);
		}
	}
}

/* Compute new value at a certain position in X direction */
__host__ __device__ void computeStepAtPositionFOUSSplitX(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
															unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dx,
															double field_max, double field_min) {
	/* Compute forward difference on x axis */
	double forward_diff_x = (src[OFFSET2D(i + 1, j, dim.NX)].getFunctionValue() - src[OFFSET2D(i, j, dim.NX)].getFunctionValue()) / dx;
	/* Compute backward difference on x axis */
	double backward_diff_x = (src[OFFSET2D(i, j, dim.NX)].getFunctionValue() - src[OFFSET2D(i - 1, j, dim.NX)].getFunctionValue()) / dx;

	/* Compute speed value */
	vec2d speed = v_field2D(src[OFFSET2D(i, j, dim.NX)].getPosition(), time);
	/* Create speeds value in x direction */
	double speed_x_plus = fmax(speed.getX(), 0.0);
	double speed_x_minus = fmin(speed.getX(), 0.0);

	/* Compute new value of the field, intermediate */
	dest[OFFSET2D(i, j, dim.NX)] = src[OFFSET2D(i, j, dim.NX)] - dt * (speed_x_plus * backward_diff_x + speed_x_minus * forward_diff_x);
}

/* Compute new value at a certain position in Y direction */
__host__ __device__ void computeStepAtPositionFOUSSplitY(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
															unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dy,
															double field_max, double field_min) {
	/* Compute forward difference on y axis */
	double forward_diff_y = (src[OFFSET2D(i, j + 1, dim.NX)].getFunctionValue() - src[OFFSET2D(i, j, dim.NX)].getFunctionValue()) / dy;
	/* Compute backward difference on y axis */
	double backward_diff_y = (src[OFFSET2D(i, j, dim.NX)].getFunctionValue() - src[OFFSET2D(i, j - 1, dim.NX)].getFunctionValue()) / dy;

	/* Compute speed value */
	vec2d speed = v_field2D(src[OFFSET2D(i, j, dim.NX)].getPosition(), time);
	/* Create speeds value in y direction */
	double speed_y_plus = fmax(speed.getY(), 0.0);
	double speed_y_minus = fmin(speed.getY(), 0.0);

	/* Compute new value of the field */
	dest[OFFSET2D(i, j, dim.NX)] = src[OFFSET2D(i, j, dim.NX)] - dt * (speed_y_plus * backward_diff_y + speed_y_minus * forward_diff_y);

#ifdef __CUDACC__
	/* Update color in the device buffer */
	if (d_vertex_color_buffer != NULL) {
		updateDeviceBufferColor2D(dest[OFFSET2D(i, j, dim.NX)], d_vertex_color_buffer, OFFSET2D(i, j, dim.NX), field_max, field_min);
	}
#endif
}

/* Compute boundary conditions */
__host__ __device__ void computeBoundaryFOUSSplit(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
													unsigned int i, unsigned int j, double time, double dt, Dim2D dim, double dx, double dy,
													double field_max, double field_min) {
	/* Simply put zero for the moment */
	dest[OFFSET2D(i, j, dim.NX)].setFValue(boundary2D(dest[OFFSET2D(i, j, dim.NX)].getPosition(), time));
	/* Update color in the device buffer */
#ifdef __CUDACC__
	if (d_vertex_color_buffer != NULL) {
		updateDeviceBufferColor2D(dest[OFFSET2D(i, j, dim.NX)], d_vertex_color_buffer, OFFSET2D(i, j, dim.NX), field_max, field_min);
	}
#endif
}


SolverFOUSchemeSplit::SolverFOUSchemeSplit()
	: Solver2D(), mesh(NULL) {
	CudaSafeCall(cudaMalloc((void**)&d_domain_input, 0));
	CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, 0));
}

SolverFOUSchemeSplit::SolverFOUSchemeSplit(DiscretizedMesh2D* m, const double t, const double dt)
	: Solver2D(t, dt, m->getNodeX() + 3, m->getNodeY() + 3), mesh(m) {
	/* Allocate space for the input domain and intermediate on the device */
	CudaSafeCall(cudaMalloc((void**)&d_domain_input, mesh->getDomainSizeT()));
	CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh->getDomainSizeT()));
	/* Copy initial field to input buffer and intermediate */
	CudaSafeCall(cudaMemcpy(d_domain_input, mesh->getDeviceDomain(), mesh->getDomainSizeT(), cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh->getDeviceDomain(), mesh->getDomainSizeT(), cudaMemcpyDeviceToDevice));
#ifdef CHECK_STABILITY
	computeDeltaT();
#endif
}

SolverFOUSchemeSplit::~SolverFOUSchemeSplit() {
	/* Free CUDA resources of the solver */
	CudaSafeCall(cudaFree(d_domain_input));
	CudaSafeCall(cudaFree(d_domain_intermediate));
}

void SolverFOUSchemeSplit::computeDeltaT() {
	/* Compute stability for delta_t according to Courant–Friedrichs–Lewy condition */
	std::cout << "Evaluating delta time for stability of Upwind Method" << std::endl;
	/* First guess for delta_t is 0.001 seconds */
	bool stable = true;
	double d_t = dt;
	unsigned int iteration = 0;
	/* Get domain to evaluate velocity field */
	DiscretizedValue2D* domain = mesh->getHostDomain();
	double eval_t;
	unsigned int i;
	unsigned int j;
	do {
		if (!stable) {
			d_t -= d_t / 2.0;
		}
		stable = true;
		/* Print current delta t tested */
		std::cout << "Trying time step: " << d_t << std::endl;
		eval_t = time;

		/* Loop over time and check if condition is satisfied. Otherwise decrease delta_t */
		do {
			j = 1;
			/* Loop over the y axis */
			do {
				i = 1;
				/* Loop over the x axis */
				do {
					/* Compute velocity at that point*/
					vec2d field_velocity = v_field2D(domain[OFFSET2D(i, j, dim.NX)].getPosition(), eval_t);
					/* Check for stability */
					double cfl = d_t * (field_velocity.getX() / mesh->getDeltaX() + field_velocity.getY() / mesh->getDeltaY());
					if (cfl > 1.0) {
						stable = false;
					}
					i++;
				} while (i < dim.NX - 1 && stable);
				j++;
			} while (j < dim.NY - 1 && stable);

			eval_t += d_t;
		} while (eval_t < time + TIME_STABILITY && stable);

		iteration++;
		/* Check for interrupt conditions */
		if (iteration > 100) {
			std::cout << "Impossible to find stability. Stopping execution!" << std::endl;
			exit(EXIT_FAILURE);
		}
	} while (!stable);

	/* Assign final value to delta_t */
	dt = d_t;
	std::cout << "Final delta time value: " << dt << std::endl;
}

void SolverFOUSchemeSplit::computeStepCPU() {
	/* Compute boundary conditions */
	computeBoundary();
	/* Get domain to compute solution */
	DiscretizedValue2D* domain = mesh->getHostDomain();
	/* Store space for new domain */
	DiscretizedValue2D* new_domain = reinterpret_cast<DiscretizedValue2D*>(malloc(mesh->getDomainSizeT()));
	/* Store space for new domain */
	DiscretizedValue2D* intermediate_domain = reinterpret_cast<DiscretizedValue2D*>(malloc(mesh->getDomainSizeT()));
	/* Update values in new_domain and intermediate_domain */
	memcpy(new_domain, domain, mesh->getDomainSizeT());
	memcpy(intermediate_domain, domain, mesh->getDomainSizeT());

	/* Compute FOU on x direction */
	for (unsigned int j = 1; j < dim.NY - 1; j++) {
		for (unsigned int i = 1; i < dim.NX - 1; i++) {
			/* Compute approximation at position i, j in X direction */
			computeStepAtPositionFOUSSplitX(intermediate_domain, domain, NULL, i, j, time, dt, dim, mesh->getDeltaX(), mesh->getStartMax(), mesh->getStartMin());
		}
	}

	/* Compute FOU on y direction */
	for (unsigned int j = 1; j < dim.NY - 1; j++) {
		for (unsigned int i = 1; i < dim.NX - 1; i++) {
			/* Compute approximation at position i, j in Y direction */
			computeStepAtPositionFOUSSplitY(new_domain, intermediate_domain, NULL, i, j, time, dt, dim, mesh->getDeltaY(), mesh->getStartMax(), mesh->getStartMin());
		}
	}

	/* Free intermediate domain */
	free(intermediate_domain);
	/* Update values in domain */
	memcpy(mesh->getHostDomain(), new_domain, mesh->getDomainSizeT());
	/* Free new mesh domain */
	free(new_domain);
	/* Update time */
	time += dt;
}

void SolverFOUSchemeSplit::computeStepGPU(double* d_vertex_color) {
	/* Launch computation */
	unsigned int num_blocks_x = ceil(dim.NX / 32.f);
	unsigned int num_blocks_y = ceil(dim.NY / 32.f);
	dim3 block = dim3(num_blocks_x, num_blocks_y, 1);
	dim3 local = dim3(32, 32, 1);

	/* Compute one step on the device on the X direction */
	computeStepDeviceFOUSSplitX<<<block, local>>>(d_domain_intermediate, d_domain_input, NULL, time, dt, dim, mesh->getDeltaX(), mesh->getDeltaY(), mesh->getStartMax(), mesh->getStartMin());
	CudaCheckError();

	/* Wait for first kernel to end */
	CudaSafeCall(cudaDeviceSynchronize());

	/* Compute one step on the device on the Y direction */
	computeStepDeviceFOUSSplitY<<<block, local>>>(mesh->getDeviceDomain(), d_domain_intermediate, d_vertex_color, time, dt, dim, mesh->getDeltaX(), mesh->getDeltaY(), mesh->getStartMax(), mesh->getStartMin());
	CudaCheckError();

	/* Update copy for next step */
	CudaSafeCall(cudaMemcpy(d_domain_input, mesh->getDeviceDomain(), mesh->getDomainSizeT(), cudaMemcpyDeviceToDevice));

	time += dt;

}

void SolverFOUSchemeSplit::computeBoundary() {
	/* Get domain to compute boundary conditions */
	DiscretizedValue2D* domain = mesh->getHostDomain();
	/* Set first row and last row */
	for (unsigned int i = 0; i < dim.NX; i++) {
		domain[i].setFValue(boundary2D(domain[i].getPosition(), time));
		domain[OFFSET2D(i, dim.NY, dim.NX)].setFValue(boundary2D(domain[OFFSET2D(i, dim.NY, dim.NX)].getPosition(), time));
	}

	/* Set first and last column */
	for (unsigned int j = 0; j < dim.NY; j++) {
		domain[OFFSET2D(0, j, dim.NX)].setFValue(boundary2D(domain[OFFSET2D(0, j, dim.NX)].getPosition(), time));
		domain[OFFSET2D(dim.NX, j, dim.NX)].setFValue(boundary2D(domain[OFFSET2D(dim.NX, j, dim.NX)].getPosition(), time));
	}
}
