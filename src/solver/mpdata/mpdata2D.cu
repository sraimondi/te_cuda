/*
 * mpdata2D.cu
 *
 *  Created on: Apr 6, 2015
 *      Author: simon
 */


#include "solver/mpdata/mpdata2D.hpp"
#include "cuda_utils/utils.hpp"
#include "function/velocity.hpp"
#include "function/boundary.hpp"
#include <math.h>
#include "utils/macros.hpp"

/* CUDA functions */
__global__ void computeMPDATA2D(DiscretizedValue2D* dest, DiscretizedValue2D* src, double* x_pa_velocities, double* y_pa_velocities, double* d_vertex_color_buffer,
								double time, double dt,
								Dim2D dim,
								double dx, double dy,
								double field_max, double field_min) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (tid_x < dim.NX && tid_y < dim.NY) {
		/* Compute new value of the domain at position i, j */
		/* Check if we are at the boundaries */
		if (tid_y == 0 || tid_y == dim.NY - 1 || tid_x == 0 || tid_x == dim.NX - 1) {
			computeBoundaryMPDATA2D(dest, src, d_vertex_color_buffer, tid_x, tid_y, time, dt, dim, dx, dy, field_max, field_min);
		} else {
			computeMPDATA2Dat(dest, src, x_pa_velocities, y_pa_velocities, d_vertex_color_buffer, tid_x, tid_y, dim.NX, field_max, field_min);
		}
	}
}

__host__ __device__ void computeMPDATA2Dat(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* x_pa_velocities, double* y_pa_velocities, double* d_vertex_color_buffer,
											unsigned int i, unsigned int j,
											unsigned int Nx,
											double field_max, double field_min) {
	/* Compute courant number */
	double U_i_plus_j = x_pa_velocities[OFFSET2D(i, j - 1, Nx - 1)];
	double U_i_minus_j = x_pa_velocities[OFFSET2D(i - 1, j - 1, Nx - 1)];

	double V_i_j_plus = y_pa_velocities[OFFSET2D(i - 1, j, Nx - 2)];
	double V_i_j_minus = y_pa_velocities[OFFSET2D(i - 1, j - 1, Nx - 2)];

	/* Compute flux and update function value */
	dest[OFFSET2D(i, j, Nx)] = src[OFFSET2D(i, j, Nx)] - (flux(src[OFFSET2D(i, j, Nx)], src[OFFSET2D(i + 1, j, Nx)], U_i_plus_j) - flux(src[OFFSET2D(i - 1, j, Nx)], src[OFFSET2D(i, j, Nx)], U_i_minus_j))
														- (flux(src[OFFSET2D(i, j, Nx)], src[OFFSET2D(i, j + 1, Nx)], V_i_j_plus) - flux(src[OFFSET2D(i, j - 1, Nx)], src[OFFSET2D(i, j, Nx)], V_i_j_minus));
#ifdef __CUDACC__
	/* Update color in the device buffer */
	if (d_vertex_color_buffer != NULL) {
		updateDeviceBufferColor2D(dest[OFFSET2D(i, j, Nx)], d_vertex_color_buffer, OFFSET2D(i, j, Nx), field_max, field_min);
	}
#endif
}

__host__ __device__ void computeBoundaryMPDATA2D(	DiscretizedValue2D* dest, DiscretizedValue2D* src, double* d_vertex_color_buffer,
													unsigned int i, unsigned int j,
													double time, double dt,
													Dim2D dim,
													double dx, double dy,
													double field_max, double field_min) {
	/* Simply put zero for the moment */
	dest[OFFSET2D(i, j, dim.NX)] = boundary2D(dest[OFFSET2D(i, j, dim.NX)].getPosition(), time);
	/* Update color in the device buffer */
#ifdef __CUDACC__
	if (d_vertex_color_buffer != NULL) {
		updateDeviceBufferColor2D(dest[OFFSET2D(i, j, dim.NX)], d_vertex_color_buffer, OFFSET2D(i, j, dim.NX), field_max, field_min);
	}
#endif
}

__host__ __device__ double flux(DiscretizedValue2D& left, DiscretizedValue2D& right, double courant_number) {
	double courant_nn = 0.5 * (courant_number + fabs(courant_number));
	double courant_np = 0.5 * (courant_number - fabs(courant_number));

	return courant_nn * left.getFunctionValue() + courant_np * right.getFunctionValue();
}

SolverMPDATA2D::SolverMPDATA2D(DiscretizedMesh2DCells& m, const double time, const double dt, unsigned int passes, const bool const_v)
	: Solver2D(time, dt, m.getCellsX(), m.getCellsY()), mesh(m), mesh_velocities(DiscretizedVelocities2DCells(mesh)),
	  passes(passes), const_v(const_v) {
	CudaSafeCall(cudaMalloc((void**)&d_domain_new, mesh.getDomainSizeT()));
	CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh.getDomainSizeT()));
	/* Copy initial field to input buffer and intermediate */
	CudaSafeCall(cudaMemcpy(d_domain_new, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
#ifdef CHECK_STABILITY
	computeDeltaT();
#endif
	mesh_velocities.initVelocitiesHost(time, dt);
	mesh_velocities.initVelocitiesDevice(time, dt);
	CudaSafeCall(cudaDeviceSynchronize());
}

SolverMPDATA2D::SolverMPDATA2D(const SolverMPDATA2D& s)
	: Solver2D(s), mesh(s.mesh), mesh_velocities(s.mesh_velocities), passes(s.passes), const_v(s.const_v) {
	CudaSafeCall(cudaMalloc((void**)&d_domain_new, mesh.getDomainSizeT()));
	CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh.getDomainSizeT()));
	/* Copy initial field to input buffer and intermediate */
	CudaSafeCall(cudaMemcpy(d_domain_new, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
}

SolverMPDATA2D& SolverMPDATA2D::operator=(const SolverMPDATA2D& s) {
	if (this == &s) {
		return *this;
	} else {
		/* Call assignment operator of super class */
		Solver2D::operator =(s);
		/* Copy attributes */
		passes = s.passes;

		 /* Copy of the internal classes */
		mesh = s.mesh;
		mesh_velocities = s.mesh_velocities;

		/* Allocate space for device pointers */
		CudaSafeCall(cudaMalloc((void**)&d_domain_new, mesh.getDomainSizeT()));
		CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh.getDomainSizeT()));

		/* Copy initial field to input buffer and intermediate */
		CudaSafeCall(cudaMemcpy(d_domain_new, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));

		return *this;
	}
}

SolverMPDATA2D::~SolverMPDATA2D() {
	CudaSafeCall(cudaFree(d_domain_new));
	CudaSafeCall(cudaFree(d_domain_intermediate));
}


void SolverMPDATA2D::computeDeltaT() {
	/* Compute stability for delta_t according to Courant–Friedrichs–Lewy condition */
	std::cout << "Evaluating delta time for stability of MPDATA Method" << std::endl;
	/* First guess for delta_t is 0.001 seconds */
	bool stable = true;
	double d_t = dt;
	unsigned int iteration = 0;
	/* Get domain to evaluate velocity field */
	DiscretizedValue2D* domain = mesh.getHostDomain();
	double eval_t;
	unsigned int i;
	unsigned int j;
	do {
		if (!stable) {
			d_t -= d_t / 2.0;
		}
		stable = true;
		/* Print current delta t tested */
		std::cout << "Trying time step: " << d_t << std::endl;
		eval_t = time;

		/* Loop over time and check if condition is satisfied. Otherwise decrease delta_t */
		do {
			j = 1;
			/* Loop over the y axis */
			do {
				i = 1;
				/* Loop over the x axis */
				do {
					/* Compute velocity at that point*/
					vec2d field_velocity = v_field2D(domain[OFFSET2D(i, j, mesh.getCellsX())].getPosition(), eval_t);
					/* Check for stability */
					double cfl = d_t * (field_velocity.getX() / mesh.getDeltaX() + field_velocity.getY() / mesh.getDeltaY());
					if (cfl > 1.0) {
						stable = false;
					}
					i++;
				} while (i < dim.NX - 1 && stable);
				j++;
			} while (j < dim.NY - 1 && stable);

			eval_t += d_t;
		} while (eval_t < time + TIME_STABILITY && stable);

		iteration++;
		/* Check for interrupt conditions */
		if (iteration > 100) {
			std::cout << "Impossible to find stability. Stopping execution!" << std::endl;
			exit(EXIT_FAILURE);
		}
	} while (!stable);

	/* Assign final value to delta_t */
	dt = d_t;
	std::cout << "Final delta time value: " << dt << std::endl;
}

void SolverMPDATA2D::computeBoundary() {
	/* Get domain to compute boundary conditions */
	DiscretizedValue2D* domain = mesh.getHostDomain();
	/* Set first row and last row */
	for (unsigned int i = 0; i < dim.NX; i++) {
		domain[i] = boundary2D(domain[i].getPosition(), time);
		domain[OFFSET2D(i, dim.NY - 1, dim.NX)] = boundary2D(domain[OFFSET2D(i, dim.NY - 1, dim.NX)].getPosition(), time);
	}

	/* Set first and last column */
	for (unsigned int j = 0; j < dim.NY; j++) {
		domain[OFFSET2D(0, j, dim.NX)] = boundary2D(domain[OFFSET2D(0, j, dim.NX)].getPosition(), time);
		domain[OFFSET2D(dim.NX - 1, j, dim.NX)] = boundary2D(domain[OFFSET2D(dim.NX - 1, j, dim.NX)].getPosition(), time);
	}
}

void SolverMPDATA2D::computeStepCPU() {
	/* Compute boundaries */
	computeBoundary();
	/* Get domain to compute solution */
	DiscretizedValue2D* u = mesh.getHostDomain();
	/* Store space for new domain */
	DiscretizedValue2D* new_u = reinterpret_cast<DiscretizedValue2D*>(malloc(mesh.getDomainSizeT()));
	/* Store space for new domain */
	DiscretizedValue2D* intermediate_u = reinterpret_cast<DiscretizedValue2D*>(malloc(mesh.getDomainSizeT()));
	/* Update values in new_domain and intermediate_domain */
	memcpy(new_u, u, mesh.getDomainSizeT());
	memcpy(intermediate_u, u, mesh.getDomainSizeT());

	/* Update partial velocities */
	if (!const_v) {
		mesh_velocities.initVelocitiesHost(time, dt);
	} else {
		mesh_velocities.resetPaVelocitiesHost();
	}
	 /* Loop over the domain for each passes of the solver */
	for (unsigned int pass = 0; pass < passes; pass++) {
		if (pass > 0) {
			mesh_velocities.computePaVelocitiesHost(dt);
		}

		/* Check if we are at the first pass or at a higher order */
		if (pass == 0) {
			/* Loop over all cells and compute upstream equation */
			/* Loop over y axis */
			for (unsigned int j = 1; j < dim.NY - 1; j++) {
				/* Loop over x axis */
				for (unsigned int i = 1; i < dim.NX - 1; i++) {
					computeMPDATA2Dat(new_u, u, mesh_velocities.getHostInitVelocitiesX(), mesh_velocities.getHostInitVelocitiesY(), NULL, i, j, dim.NX, mesh.getStartMax(), mesh.getStartMin());
				}
			}
		} else {
			/* Loop over all cells and compute higher order approximation */
			/* Loop over y axis */
			for (unsigned int j = 1; j < dim.NY - 1; j++) {
				/* Loop over x axis */
				for (unsigned int i = 1; i < dim.NX - 1; i++) {
					computeMPDATA2Dat(intermediate_u, new_u, mesh_velocities.getHostPaVelocitiesX(), mesh_velocities.getHostPaVelocitiesY(), NULL, i, j, dim.NX, mesh.getStartMax(), mesh.getStartMin());
				}
			}
			/* Update new_u */
			memcpy(new_u, intermediate_u, mesh.getDomainSizeT());
		}
	}

	/* Free intermediate domain */
	free(intermediate_u);
	/* Update values in host domain */
	memcpy(mesh.getHostDomain(), new_u, mesh.getDomainSizeT());
	/* Free new mesh domain */
	free(new_u);
	/* Update time */
	time += dt;

}

void SolverMPDATA2D::computeStepGPU(double* d_vertex_color) {
	/* Compute dimensions computation */
	unsigned int num_blocks_x = ceil(dim.NX / 32.f);
	unsigned int num_blocks_y = ceil(dim.NY / 4.f);
	dim3 block = dim3(num_blocks_x, num_blocks_y, 1);
	dim3 local = dim3(32, 4, 1);

	/* Update partial velocities */
	if (!const_v) {
		mesh_velocities.initVelocitiesDevice(time, dt);
		CudaSafeCall(cudaDeviceSynchronize());
	} else {
		mesh_velocities.resetPaVelocitiesDevice();
	}

	for (unsigned int pass = 0; pass < passes; pass++) {
		if (pass > 0) {
			mesh_velocities.computePaVelocitiesDevice(dt);
			CudaSafeCall(cudaDeviceSynchronize());
		}

		/* Check if we are at the first pass or at a higher order */
		if (pass == 0) {
			computeMPDATA2D<<<block, local>>>(	d_domain_new, mesh.getDeviceDomain(),
												mesh_velocities.getDeviceInitVelocitiesX(), mesh_velocities.getDeviceInitVelocitiesY(),
												d_vertex_color, time, dt, dim,
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getStartMax(), mesh.getStartMin());
			CudaCheckError();
		} else {
			computeMPDATA2D<<<block, local>>>(	d_domain_intermediate, d_domain_new,
												mesh_velocities.getDevicePaVelocitiesX(), mesh_velocities.getDevicePaVelocitiesY(),
												d_vertex_color, time, dt, dim,
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getStartMax(), mesh.getStartMin());
			CudaCheckError();
			/* Update d_domain_new */
			CudaSafeCall(cudaMemcpy(d_domain_new, d_domain_intermediate, mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
		}

	}

	/* Update device domain */
	CudaSafeCall(cudaMemcpy(mesh.getDeviceDomain(), d_domain_new, mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));

	time += dt;

}
