/*
 * mpdata3D.cu
 *
 *  Created on: Apr 15, 2015
 *      Author: simon
 */

#include "solver/mpdata/mpdata3D.hpp"
#include "cuda_utils/utils.hpp"
#include "function/velocity.hpp"
#include "function/boundary.hpp"
#include <math.h>
#include "utils/macros.hpp"

__global__ void computeMPDATA3D(DiscretizedValue3D* dest, DiscretizedValue3D* src,
								double* x_pa_velocities, double* y_pa_velocities, double* z_pa_velocities,
								double* d_vertex_color_buffer, double time, double dt,
								Dim3D dim,
								double dx, double dy, double dz,
								double field_max, double field_min) {
	/* Compute index for the thread */
	unsigned int tid_x = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int tid_y = blockDim.y * blockIdx.y + threadIdx.y;
	unsigned int tid_z = blockDim.z * blockIdx.z + threadIdx.z;

	if (tid_x < dim.NX && tid_y < dim.NY && tid_z < dim.NZ) {
		/* Compute new value of the domain at position i, j, k */
		/* Check if we are at the boundaries */
		if (tid_y == 0 || tid_y == dim.NY - 1 || tid_x == 0 || tid_x == dim.NX - 1 || tid_z == 0 || tid_z == dim.NZ - 1) {
			computeBoundaryMPDATA3D(dest, src, d_vertex_color_buffer, tid_x, tid_y, tid_z, time, dt, dim, dx, dy, dz, field_max, field_min);
		} else {
			computeMPDATA3Dat(dest, src, x_pa_velocities, y_pa_velocities, z_pa_velocities, d_vertex_color_buffer, tid_x, tid_y, tid_z, dim.NX, dim.NY, field_max, field_min);
		}
	}
}

__host__ __device__ void computeMPDATA3Dat(	DiscretizedValue3D* dest, DiscretizedValue3D* src,
											double* x_pa_velocities, double* y_pa_velocities, double* z_pa_velocities,
											double* d_vertex_color_buffer,
											unsigned int i, unsigned int j, unsigned int k,
											unsigned int Nx, unsigned int Ny,
											double field_max, double field_min) {
	/* Compute courant number */
	double U_i_plus_j_k = x_pa_velocities[OFFSET3D(i, j - 1, k - 1, Nx - 1, Ny - 2)];
	double U_i_minus_j_k = x_pa_velocities[OFFSET3D(i - 1, j - 1, k - 1, Nx - 1, Ny - 2)];

	double V_i_j_plus_k = y_pa_velocities[OFFSET3D(i - 1, j, k - 1, Nx - 2, Ny - 1)];
	double V_i_j_minus_k = y_pa_velocities[OFFSET3D(i - 1, j - 1, k - 1, Nx - 2, Ny - 1)];

	double W_i_j_k_plus = z_pa_velocities[OFFSET3D(i - 1, j - 1, k, Nx - 2, Ny - 2)];
	double W_i_j_k_minus = z_pa_velocities[OFFSET3D(i - 1, j - 1, k - 1, Nx - 2, Ny - 2)];


	/* Compute flux and update function value */
	dest[OFFSET3D(i, j, k, Nx, Ny)] = src[OFFSET3D(i, j, k, Nx, Ny)]
	                                      - (flux(src[OFFSET3D(i, j, k, Nx, Ny)], src[OFFSET3D(i + 1, j, k, Nx, Ny)], U_i_plus_j_k) - flux(src[OFFSET3D(i - 1, j, k, Nx, Ny)], src[OFFSET3D(i, j, k, Nx, Ny)], U_i_minus_j_k))
	                                      - (flux(src[OFFSET3D(i, j, k, Nx, Ny)], src[OFFSET3D(i, j + 1, k, Nx, Ny)], V_i_j_plus_k) - flux(src[OFFSET3D(i, j - 1, k, Nx, Ny)], src[OFFSET3D(i, j, k, Nx, Ny)], V_i_j_minus_k))
	                                      - (flux(src[OFFSET3D(i, j, k, Nx, Ny)], src[OFFSET3D(i, j, k + 1, Nx, Ny)], W_i_j_k_plus) - flux(src[OFFSET3D(i, j, k - 1, Nx, Ny)], src[OFFSET3D(i, j, k, Nx, Ny)], W_i_j_k_minus));
#ifdef __CUDACC__
	/* Update color in the device buffer */
	if (d_vertex_color_buffer != NULL) {
		updateDeviceBufferColor3D(dest[OFFSET3D(i, j, k, Nx, Ny)], d_vertex_color_buffer, OFFSET3D(i, j, k, Nx, Ny), field_max, field_min);
	}
#endif
}


__host__ __device__ void computeBoundaryMPDATA3D(	DiscretizedValue3D* dest, DiscretizedValue3D* src,
													double* d_vertex_color_buffer,
													unsigned int i, unsigned int j, unsigned int k,
													double time, double dt,
													Dim3D dim,
													double dx, double dy, double dz,
													double field_max, double field_min) {
	/* Simply put zero for the moment */
	dest[OFFSET3D(i, j, k, dim.NX, dim.NY)] = boundary3D(dest[OFFSET3D(i, j, k, dim.NX, dim.NY)].getPosition(), time);
	/* Update color in the device buffer */
#ifdef __CUDACC__
	if (d_vertex_color_buffer != NULL) {
		updateDeviceBufferColor3D(dest[OFFSET3D(i, j, k, dim.NX, dim.NY)], d_vertex_color_buffer, OFFSET3D(i, j, k, dim.NX, dim.NY), field_max, field_min);
	}
#endif
}


__host__ __device__ double flux(DiscretizedValue3D& left, DiscretizedValue3D& right, double courant_number) {
	double courant_nn = 0.5 * (courant_number + fabs(courant_number));
	double courant_np = 0.5 * (courant_number - fabs(courant_number));

	return courant_nn * left.getFunctionValue() + courant_np * right.getFunctionValue();
}

SolverMPDATA3D::SolverMPDATA3D(DiscretizedMesh3DCells& m, const double time, const double dt, unsigned int passes, const bool const_v)
	: Solver3D(time, dt, m.getCellsX(), m.getCellsY(), m.getCellsZ()),  mesh(m),
	  mesh_velocities(DiscretizedVelocities3DCells(mesh)), passes(passes), const_v(const_v) {
	CudaSafeCall(cudaMalloc((void**)&d_domain_new, mesh.getDomainSizeT()));
	CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh.getDomainSizeT()));

	/* Copy initial field to input buffer and intermediate */
	CudaSafeCall(cudaMemcpy(d_domain_new, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
#ifdef CHECK_STABILITY
	computeDeltaT();
#endif
	/* Initialize velocities */
	mesh_velocities.initVelocitiesHost(time, dt);
	mesh_velocities.initVelocitiesDevice(time, dt);
	CudaSafeCall(cudaDeviceSynchronize());
}

SolverMPDATA3D::SolverMPDATA3D(const SolverMPDATA3D& s)
	: Solver3D(s), mesh(s.mesh), mesh_velocities(s.mesh_velocities), passes(s.passes), const_v(s.const_v) {
	CudaSafeCall(cudaMalloc((void**)&d_domain_new, mesh.getDomainSizeT()));
	CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh.getDomainSizeT()));
	/* Copy initial field to input buffer and intermediate */
	CudaSafeCall(cudaMemcpy(d_domain_new, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
	CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
}

SolverMPDATA3D& SolverMPDATA3D::operator =(const SolverMPDATA3D& s) {
	if (this == &s) {
		return *this;
	} else {
		/* Call assignment operator of super class */
		Solver3D::operator =(s);
		/* Copy attributes */
		passes = s.passes;

		 /* Copy of the internal classes */
		mesh = s.mesh;
		mesh_velocities = s.mesh_velocities;

		/* Allocate space for device pointers */
		CudaSafeCall(cudaMalloc((void**)&d_domain_new, mesh.getDomainSizeT()));
		CudaSafeCall(cudaMalloc((void**)&d_domain_intermediate, mesh.getDomainSizeT()));

		/* Copy initial field to input buffer and intermediate */
		CudaSafeCall(cudaMemcpy(d_domain_new, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
		CudaSafeCall(cudaMemcpy(d_domain_intermediate, mesh.getDeviceDomain(), mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));

		return *this;
	}
}

SolverMPDATA3D::~SolverMPDATA3D() {
	CudaSafeCall(cudaFree(d_domain_new));
	CudaSafeCall(cudaFree(d_domain_intermediate));
}

void SolverMPDATA3D::computeDeltaT() {
	/* Compute stability for delta_t according to Courant–Friedrichs–Lewy condition */
	std::cout << "Evaluating delta time for stability of MPDATA Method" << std::endl;
	/* First guess for delta_t is 0.001 seconds */
	bool stable = true;
	double d_t = dt;
	unsigned int iteration = 0;
	/* Get domain to evaluate velocity field */
	DiscretizedValue3D* domain = mesh.getHostDomain();
	double eval_t;
	unsigned int i;
	unsigned int j;
	do {
		if (!stable) {
			d_t -= d_t / 2.0;
		}
		stable = true;
		/* Print current delta t tested */
		std::cout << "Trying time step: " << d_t << std::endl;
		eval_t = time;

		/* Loop over time and check if condition is satisfied. Otherwise decrease delta_t */
		do {
			j = 1;
			/* Loop over the y axis */
			do {
				i = 1;
				/* Loop over the x axis */
				do {
					/* Compute velocity at that point*/
//					vec2d field_velocity = v_field2D(domain[OFFSET2D(i, j, mesh.getCellsX() + 2)].getPosition(), eval_t);
					/* Check for stability */
//					double cfl = d_t * (field_velocity.getX() / mesh.getDeltaX() + field_velocity.getY() / mesh.getDeltaY());
//					if (cfl > 1.0) {
//						stable = false;
//					}
					i++;
				} while (i < dim.NX - 1 && stable);
				j++;
			} while (j < dim.NY - 1 && stable);

			eval_t += d_t;
		} while (eval_t < time + TIME_STABILITY && stable);

		iteration++;
		/* Check for interrupt conditions */
		if (iteration > 100) {
			std::cout << "Impossible to find stability. Stopping execution!" << std::endl;
			exit(EXIT_FAILURE);
		}
	} while (!stable);

	/* Assign final value to delta_t */
	dt = d_t;
	std::cout << "Final delta time value: " << dt << std::endl;
}

void SolverMPDATA3D::computeBoundary() {
	/* Get domain to compute boundary conditions */
	DiscretizedValue3D* domain = mesh.getHostDomain();
	/* Set front face and back face */
	for (unsigned int k = 0; k < dim.NZ; k++) {
		for (unsigned int i = 0; i < dim.NX; i++) {
			domain[OFFSET3D(i, 0, k, dim.NX, dim.NY)].setFValue(boundary3D(domain[OFFSET3D(i, 0, k, dim.NX, dim.NY)].getPosition(), time));
			domain[OFFSET3D(i, dim.NY - 1, k, dim.NX, dim.NY)].setFValue(boundary3D(domain[OFFSET3D(i, dim.NY - 1, k, dim.NX, dim.NY)].getPosition(), time));
		}
	}

	/* Set right and left faces */
	for (unsigned int k = 0; k < dim.NZ; k++) {
		for (unsigned int j = 0; j < dim.NY; j++) {
			domain[OFFSET3D(0, j, k, dim.NX, dim.NY)].setFValue(boundary3D(domain[OFFSET3D(0, j, k, dim.NX, dim.NY)].getPosition(), time));
			domain[OFFSET3D(dim.NX - 1, j, k, dim.NX, dim.NY)].setFValue(boundary3D(domain[OFFSET3D(dim.NX - 1, j, k, dim.NX, dim.NY)].getPosition(), time));
		}
	}

	/* Set top and bottom faces */
	for (unsigned int j = 0; j < dim.NY; j++) {
		for (unsigned int i = 0; i < dim.NX; i++) {
			domain[OFFSET3D(i, j, 0, dim.NX, dim.NY)].setFValue(boundary3D(domain[OFFSET3D(i, j, 0, dim.NX, dim.NY)].getPosition(), time));
			domain[OFFSET3D(i, j, dim.NZ - 1, dim.NX, dim.NY)].setFValue(boundary3D(domain[OFFSET3D(i, j, dim.NZ - 1, dim.NX, dim.NY)].getPosition(), time));
		}
	}
}

void SolverMPDATA3D::computeStepCPU() {
	/* Compute boundaries */
	computeBoundary();
	/* Get domain to compute solution */
	DiscretizedValue3D* u = mesh.getHostDomain();
	/* Store space for new domain */
	DiscretizedValue3D* new_u = new DiscretizedValue3D[mesh.getDomainSize()];
	/* Store space for new domain */
	DiscretizedValue3D* intermediate_u = new DiscretizedValue3D[mesh.getDomainSize()];
	/* Update values in new_domain and intermediate_domain */
	memcpy(new_u, u, mesh.getDomainSizeT());
	memcpy(intermediate_u, u, mesh.getDomainSizeT());

	/* Update partial velocities */
	if (!const_v) {
		mesh_velocities.initVelocitiesHost(time, dt);
	} else {
		mesh_velocities.resetPaVelocitiesHost();
	}

	 /* Loop over the domain for each passes of the solver */
	for (unsigned int pass = 0; pass < passes; pass++) {
		if (pass > 0) {
			mesh_velocities.computePaVelocitiesHost(dt);
		}

		/* Check if we are at the first pass or at a higher order */
		if (pass == 0) {
			/* Loop over all cells and compute upstream equation */
			/* Loop over z axis */
			for (unsigned int k = 1; k < dim.NZ - 1; k++) {
				/* Loop over y axis */
				for (unsigned int j = 1; j < dim.NY - 1; j++) {
					/* Loop over x axis */
					for (unsigned int i = 1; i < dim.NX - 1; i++) {
						computeMPDATA3Dat(	new_u, u,
											mesh_velocities.getHostInitVelocitiesX(), mesh_velocities.getHostInitVelocitiesY(), mesh_velocities.getHostInitVelocitiesZ(),
											NULL, i, j, k, dim.NX, dim.NY, mesh.getStartMax(), mesh.getStartMin());
					}
				}
			}
		} else {
			/* Loop over all cells and compute higher order approximation */
			/* Loop over z axis */
			for (unsigned int k = 1; k < dim.NZ - 1; k++) {
				/* Loop over y axis */
				for (unsigned int j = 1; j < dim.NY - 1; j++) {
					/* Loop over x axis */
					for (unsigned int i = 1; i < dim.NX - 1; i++) {
						computeMPDATA3Dat(	intermediate_u, new_u,
											mesh_velocities.getHostPaVelocitiesX(), mesh_velocities.getHostPaVelocitiesY(), mesh_velocities.getHostPaVelocitiesZ(),
											NULL, i, j, k, dim.NX, dim.NY, mesh.getStartMax(), mesh.getStartMin());
					}
				}
			}

			/* Update new_u */
			memcpy(new_u, intermediate_u, mesh.getDomainSizeT());
		}
	}

	/* Free intermediate domain */
	delete [] intermediate_u;
	/* Update values in host domain */
	memcpy(mesh.getHostDomain(), new_u, mesh.getDomainSizeT());
	/* Free new mesh domain */
	delete [] new_u;
	/* Update time */
	time += dt;
}

void SolverMPDATA3D::computeStepGPU(double* d_vertex_color) {
	/* Compute dimensions computation */
	unsigned int num_blocks_x = ceil(dim.NX / 8.f);
	unsigned int num_blocks_y = ceil(dim.NY / 8.f);
	unsigned int num_blocks_z = ceil(dim.NZ / 8.f);
	dim3 block = dim3(num_blocks_x, num_blocks_y, num_blocks_z);
	dim3 local = dim3(8, 8, 8);

	/* Update partial velocities */
	if (!const_v) {
		mesh_velocities.initVelocitiesDevice(time, dt);
		CudaSafeCall(cudaDeviceSynchronize());
	} else {
		mesh_velocities.resetPaVelocitiesDevice();
	}

	for (unsigned int pass = 0; pass < passes; pass++) {
		if (pass > 0) {
			mesh_velocities.computePaVelocitiesDevice(dt);
			CudaSafeCall(cudaDeviceSynchronize());
		}

		/* Check if we are at the first pass or at a higher order */
		if (pass == 0) {
			computeMPDATA3D<<<block, local>>>(	d_domain_new, mesh.getDeviceDomain(),
												mesh_velocities.getDeviceInitVelocitiesX(), mesh_velocities.getDeviceInitVelocitiesY(), mesh_velocities.getDeviceInitVelocitiesZ(),
												d_vertex_color, time, dt, dim,
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(),
												mesh.getStartMax(), mesh.getStartMin());
			CudaCheckError();
		} else {
			computeMPDATA3D<<<block, local>>>(	d_domain_intermediate, d_domain_new,
												mesh_velocities.getDevicePaVelocitiesX(), mesh_velocities.getDevicePaVelocitiesY(), mesh_velocities.getDevicePaVelocitiesZ(),
												d_vertex_color, time, dt, dim,
												mesh.getDeltaX(), mesh.getDeltaY(), mesh.getDeltaZ(),
												mesh.getStartMax(), mesh.getStartMin());
			CudaCheckError();
			/* Update d_domain_new */
			CudaSafeCall(cudaMemcpy(d_domain_new, d_domain_intermediate, mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));
		}

	}

	/* Update device domain */
	CudaSafeCall(cudaMemcpy(mesh.getDeviceDomain(), d_domain_new, mesh.getDomainSizeT(), cudaMemcpyDeviceToDevice));

	time += dt;

}
