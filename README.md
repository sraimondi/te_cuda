# README #

This code implements the [MPDATA algorithm](https://www.rsmas.miami.edu/users/miskandarani/Courses/MPO662/Smolarkiewicz/MPDATAreview.JCP140.pdf) using the CUDA library.

You can find my Bachelor Thesis [here](https://www.dropbox.com/s/12mvwsl6m16pv7e/SimoneRaimondi-MPDATA.pdf?dl=0).

### Building ###
You need to have a CUDA capable GPU to build and run the code.
Under Ubuntu 14.10 (tested operating system), download the [CUDA Toolkit](https://developer.nvidia.com/cuda-downloads) and install it with the NVIDIA drivers.
You need also to install glfw and glew on your system. You can do it easily using Synaptic package manager or apt-get.
The best way to build the project is to open Nsight, the IDE provided with the NVIDA Toolkit and import the project there. 
Once you have imported it, you just need to build it inside the editor.

### Demo ###
The program comes with two demos of the solver in two and three dimensions. You can run them by going into the Debug folder and type

./te_cuda -gpu 300 300

for the two-dimensional demo and

./te_cuda -gpu 30 30 30

for the three-dimensional demo.

The numbers are examples of grid sizes to discretize the domain.